﻿using System;

namespace System.IO.Ports
{
    public static class extSerialPort
    {
        public static string GetPort(this SerialPort serial_port)
        {
            string result;

            result = Microsoft.Win32.Registry.GetValue(@"HKEY_LOCAL_MACHINE\HARDWARE\DEVICEMAP\SERIALCOMM",
                                                       @"\Device\HhpcdcVirtualPort0",
                                                       "Empty").ToString();

            if (result == null || result == "Empty")
            {
                result = Microsoft.Win32.Registry.GetValue(@"HKEY_LOCAL_MACHINE\HARDWARE\DEVICEMAP\SERIALCOMM",
                                                           @"\Device\honeywell_cdc_AcmSerial0",
                                                           "Empty").ToString();
            }

            return ((result == null || result == "Empty") ? "NULL" : result);
        }
    }
}
