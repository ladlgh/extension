﻿using System.Collections.Generic;

namespace System.IO
{
    public static class extPath
    {
        public static List<string> AllFilePaths(this DirectoryInfo directory,
                                                string extension)
        {
            List<string> list = new List<string>();

            // get all files of suborders
            FileInfo[] files = directory.GetFiles("*" + extension, SearchOption.AllDirectories);

            foreach (FileInfo file in files)
            {
                list.Add(file.FullName);
            }

            return list;
        }

        private static void FileSearcher(List<string> list,
                                         DirectoryInfo directory,
                                         string extension)
        {
            try
            {
                // Alle Verzeichnisse rekursiv durchlaufen
                foreach (DirectoryInfo subdir in directory.GetDirectories())
                {
                    FileSearcher(list, subdir, extension);
                }

                // Alle Dateien durchlaufen
                foreach (FileInfo file in directory.GetFiles("*" + extension, SearchOption.TopDirectoryOnly))
                {
                    list.Add(file.FullName);
                }
            }
            catch
            {
                // do nothing
            }
        }
    }
}
