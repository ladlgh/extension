﻿using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;

namespace System.Collections.Generic
{
    public static class extDictionary
    {
        // Load File To Dictionary
        public static Dictionary<TKey, TValue> DeSerialize<TKey, TValue>(this Dictionary<TKey, TValue> dictionary,
                                                                         string file_path,
                                                                         bool crypto = false)
        {
            byte[] storage;

            if (crypto)
            {
                using (CryptoCore cryptor = new CryptoCore())
                {
                    storage = cryptor.DecryptBuffer(File.ReadAllBytes(file_path));
                }
            }
            else
            {
                storage = File.ReadAllBytes(file_path);
            }

            using (MemoryStream ms = new MemoryStream(storage))
            {
                BinaryFormatter bf = new BinaryFormatter();

                dictionary = (Dictionary<TKey, TValue>)bf.Deserialize(ms);

                return dictionary;
            }
        }

        // Write Dictionary To File
        public static void Serialize<TKey, TValue>(this Dictionary<TKey, TValue> dictionary,
                                                   string file_path,
                                                   bool crypto = false)
        {
            byte[] storage;

            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, dictionary);
                storage = ms.ToArray();
            }

            if (crypto)
            {
                using (CryptoCore cryptor = new CryptoCore())
                {
                    File.WriteAllBytes(file_path, cryptor.EncryptBuffer(storage));
                }
            }
            else
            {
                File.WriteAllBytes(file_path, storage);
            }
        }

        // get index of key
        public static int IndexOfKey<TKey, TValue>(this Dictionary<TKey, TValue> dictionary,
                                                   TKey key)
        {
            int index = -1;

            index = dictionary.Keys.ToList().IndexOf(key);

            return index;
        }

        // get index of value
        public static int IndexOfValue<TKey, TValue>(this Dictionary<TKey, TValue> dictionary,
                                                     TValue value)
        {
            int index = -1;

            index = dictionary.Values.ToList().IndexOf(value);

            return index;
        }
    }
}
