﻿using System.Collections.Generic;

namespace System.Windows.Forms
{
    public static class extControl
    {
        public static void Invoke(this Control control,
                                  Action action)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            if (control.InvokeRequired)
            {
                control.Invoke(action);
            }
            else
            {
                action();
            }
        }

        public static IEnumerable<Control> All(this Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                foreach (Control grandChild in control.Controls.All())
                {
                    yield return grandChild;
                }

                yield return control;
            }
        }
    }
}
