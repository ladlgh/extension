﻿using System.Drawing.Imaging;
using System.IO;

namespace System.Drawing
{
    public static class extBitmap
    {
        public static Image ToImage(this Bitmap bmp)
        {
            Image image;

            using (Stream s = new MemoryStream())
            {
                bmp.Save(s, ImageFormat.Bmp);
                image = Image.FromStream(s);
            }

            return image;
        }
    }
}
