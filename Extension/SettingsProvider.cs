﻿using System.IO;
using System.Collections.Specialized;
using System.Xml;
using System.Collections;
using System.Windows.Forms;

namespace System.Configuration
{
    public abstract class PortableSettingsProvider : SettingsProvider, IApplicationSettingsProvider
    {
        public abstract string ClassName            { get; }
        public abstract string RootSubPath          { get; }
        public abstract bool   RootSubPathIsHidden  { get; }
        public abstract string FileName             { get; }
        public abstract string XmlRootNodeName      { get; }

        private string XmlLocalNodeName  = "LOCAL";
        private string XmlGlobalNodeName = "CLOBAL";
        
        public PortableSettingsProvider() { }

        private XmlDocument xmldocument;
        
        private string SettingsFilePath
        {
            get
            {
                string appPath      = Path.GetDirectoryName(Application.ExecutablePath);
                string settingsPath = appPath;

                if (!string.IsNullOrEmpty(RootSubPath))
                {
                    settingsPath = Path.Combine(settingsPath, RootSubPath);
                }

                if (settingsPath != appPath)
                {
                    if (!Directory.Exists(settingsPath))
                    {
                        try
                        {
                            DirectoryInfo di = Directory.CreateDirectory(settingsPath);

                            if (RootSubPathIsHidden)
                            {
                                di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
                            }
                        }
                        catch (Exception)
                        {
                            settingsPath = appPath;
                        }
                    }
                    else if (RootSubPathIsHidden)
                    {
                        DirectoryInfo di = new DirectoryInfo(settingsPath);

                        if ((di.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
                        {
                            di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
                        }
                    }
                }

                return Path.Combine(settingsPath, FileName);
            }
        }

        private XmlNode XmlLocalNode
        {
            get
            {
                XmlNode settingsNode = GetRootNode(XmlLocalNodeName);
                XmlNode userNode     = settingsNode.SelectSingleNode(Environment.UserName.ToLowerInvariant());

                if (userNode == null)
                {
                    userNode = SettingsDocument.CreateElement(Environment.UserName.ToLowerInvariant());
                    settingsNode.AppendChild(userNode);
                }

                return userNode;
            }
        }

        private XmlNode XmlGlobalNode
        {
            get
            {
                return GetRootNode(XmlGlobalNodeName);
            }
        }

        private XmlNode XmlRootNode
        {
            get
            {
                return SettingsDocument.SelectSingleNode(XmlRootNodeName);
            }
        }

        private XmlDocument SettingsDocument
        {
            get
            {
                if (xmldocument == null)
                {
                    try
                    {
                        xmldocument = new XmlDocument();
                        xmldocument.Load(SettingsFilePath);
                    }
                    catch (Exception)
                    {

                    }

                    if (xmldocument.SelectSingleNode(XmlRootNodeName) != null)
                    {
                        return xmldocument;
                    }

                    xmldocument = GetBlankXmlDocument();
                }

                return xmldocument;
            }
        }

        public override string ApplicationName
        {
            get
            {
                return Path.GetFileNameWithoutExtension(Application.ExecutablePath);
            }
            set { }
        }

        public override string Name
        {
            get
            {
                return ClassName;
            }
        }

        public override void Initialize(string name, NameValueCollection config)
        {
            base.Initialize(Name, config);
        }

        public override void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection collection)
        {
            foreach (SettingsPropertyValue propertyValue in collection)
            {
                SetValue(propertyValue);
            }

            try
            {
                SettingsDocument.Save(SettingsFilePath);
            }
            catch (Exception)
            {
                /* 
                 * If this is a portable application and the device has been 
                 * removed then this will fail, so don't do anything. It's 
                 * probably better for the application to stop saving settings 
                 * rather than just crashing outright. Probably.
                 */
            }
        }

        public override SettingsPropertyValueCollection GetPropertyValues(SettingsContext context, SettingsPropertyCollection collection)
        {
            SettingsPropertyValueCollection values = new SettingsPropertyValueCollection();

            foreach (SettingsProperty property in collection)
            {
                values.Add(new SettingsPropertyValue(property)
                {
                    SerializedValue = GetValue(property)
                });
            }

            return values;
        }
        
        private void SetValue(SettingsPropertyValue propertyValue)
        {
            XmlNode  targetNode = IsGlobal(propertyValue.Property) ? XmlGlobalNode : XmlLocalNode;
            XmlNode settingNode = targetNode.SelectSingleNode(string.Format("setting[@name='{0}']", propertyValue.Name));

            if (settingNode != null)
            {
                settingNode.InnerText = propertyValue.SerializedValue.ToString();
            }
            else
            {
                settingNode = SettingsDocument.CreateElement("setting");

                XmlAttribute nameAttribute = SettingsDocument.CreateAttribute("name");
                nameAttribute.Value        = propertyValue.Name;

                settingNode.Attributes.Append(nameAttribute);
                settingNode.InnerText = propertyValue.SerializedValue.ToString();

                targetNode.AppendChild(settingNode);
            }
        }

        private string GetValue(SettingsProperty property)
        {
            XmlNode  targetNode = IsGlobal(property) ? XmlGlobalNode : XmlLocalNode;
            XmlNode settingNode = targetNode.SelectSingleNode(string.Format("setting[@name='{0}']", property.Name));

            if (settingNode == null)
            {
                return (property.DefaultValue != null) ? property.DefaultValue.ToString() : string.Empty;
            }

            return settingNode.InnerText;
        }

        private bool IsGlobal(SettingsProperty property)
        {
            foreach (DictionaryEntry attribute in property.Attributes)
            {
                if ((Attribute)attribute.Value is SettingsManageabilityAttribute)
                {
                    return true;
                }
            }

            return false;
        }

        private XmlNode GetRootNode(string name)
        {
            XmlNode RootNode = XmlRootNode.SelectSingleNode(name);

            if (RootNode == null)
            {
                RootNode = SettingsDocument.CreateElement(name);
                XmlRootNode.AppendChild(RootNode);
            }

            return RootNode;
        }

        private XmlDocument GetBlankXmlDocument()
        {
            XmlDocument blankXmlDocument = new XmlDocument();
            blankXmlDocument.AppendChild(blankXmlDocument.CreateXmlDeclaration("1.0", "utf-8", string.Empty));
            blankXmlDocument.AppendChild(blankXmlDocument.CreateElement(XmlRootNodeName));

            return blankXmlDocument;
        }

        public void Reset(SettingsContext context)
        {
            XmlLocalNode.RemoveAll();
            XmlGlobalNode.RemoveAll();

            xmldocument.Save(SettingsFilePath);
        }

        public SettingsPropertyValue GetPreviousVersion(SettingsContext context, SettingsProperty property)
        {
            return new SettingsPropertyValue(property);
        }

        public void Upgrade(SettingsContext context, SettingsPropertyCollection properties)
        {

        }
    }
}