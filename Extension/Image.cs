﻿using System.Drawing.Imaging;
using System.IO;

namespace System.Drawing
{
    public static class extImage
    {
        public static Image FromBitmap(this Image image, Bitmap bitmap)
        {
            using (Stream s = new MemoryStream())
            {
                bitmap.Save(s, ImageFormat.Bmp);
                image = Image.FromStream(s);
            }

            return image;
        }
    }
}
