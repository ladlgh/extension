﻿using System;

namespace System.IO
{
    public static class extFile
    {
        public static int LinesCount(this string file_path)
        {
            int counter = -1;

            if (File.Exists(file_path))
            {
                counter = 0;

                using (StreamReader sr = new StreamReader(file_path))
                {
                    string line;

                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.StartsWith("[") && !line.StartsWith("[G")) counter++;
                    }
                }
            }

            return counter;
        }
    }
}
