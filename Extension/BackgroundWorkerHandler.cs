﻿using System.Collections.Generic;
using System.Linq;

namespace System.ComponentModel
{
    public class BackgroundWorkerHandler
    {
        public int Total
        {
            get
            {
                return (actions == null) ? 0 : actions.Count();
            }
        }

        private IEnumerable<Action> actions;
        private BackgroundWorker worker;

        public void AddActions(IEnumerable<Action> action_list)
        {
            actions = action_list;
        }

        public BackgroundWorkerHandler(BackgroundWorker aWorker)
        {
            worker = aWorker;
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;

            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
        }

        public BackgroundWorkerHandler(IEnumerable<Action> action_list, BackgroundWorker aWorker) : this(aWorker)
        {
            actions = action_list;
        }
        
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (actions == null)
            {
                throw new InvalidOperationException("You must provide actions to execute");
            }

            int total   = actions.Count();
            int current = 0;

            foreach (var next in actions)
            {
                next();
                current++;

                if (worker.CancellationPending == true)
                {
                    return;
                }
            }
        }
    }
}
