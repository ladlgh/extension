﻿using System.Data;

namespace System.Windows.Forms
{
    public static class extDataGridView
    {
        public static void DataSourceRefresh(this DataGridView datagridview,
                                             DataView dataview,
                                             string defaultsort = "")
        {
            try
            {
                // notice first showen and selected rows
                int firstRowIdx      = (datagridview.Rows.Count > 0) ? datagridview.FirstDisplayedCell.RowIndex : -1;
                int selectedRowCount = datagridview.Rows.GetRowCount(DataGridViewElementStates.Selected);
                int[] selectedRows   = new int[selectedRowCount];

                for (int i = 0; i < selectedRowCount; i++)
                {
                    selectedRows[i] = datagridview.SelectedRows[i].Index;
                }

                // refresh datagridview
                dataview.Sort = defaultsort;
                datagridview.DataSource = dataview;
                datagridview.ClearSelection();

                // refresh datagridview
                datagridview.Refresh();
                datagridview.Update();
            }
            catch (Exception)
            {
                MessageBox.Show("Something is wrong, can`t set datasource!",
                                "Error...",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        public static int Get_RowCount(this DataGridView datagridview,
                                       bool IsGrouped = false,
                                       bool IsGroupRow = false)
        {
            return datagridview.Rows.Count;
        }

        public static void ReSelectCell(this DataGridView datagridview,
                                        int[] last_selected_row,
                                        bool IsGrouped = false,
                                        bool IsGroupRow = false)
        {
            int last_row_id = datagridview.Rows.Count - 1;
            datagridview.FirstDisplayedScrollingRowIndex = Math.Min(last_selected_row[0], last_row_id);
            int selected_row_id = Math.Min(last_selected_row[1], last_row_id);

            if (IsGrouped
                && IsGroupRow)
            {
                selected_row_id = (selected_row_id + 1 > last_row_id) ? selected_row_id - 1 : selected_row_id + 1;
            }

            datagridview.Rows[selected_row_id].Selected = true;
        }

        public static void AdjustSelectedCell(this DataGridView datagridview,
                                              int[] last_selected_row)
        {
            if (datagridview.SelectedRows.Count == 0)
            {
                return;
            }

            if (datagridview.Rows.Count > last_selected_row[1])
            {
                datagridview.InvalidateRow(last_selected_row[1]);
            }

            last_selected_row[1] = (datagridview.SelectedRows[0] != null) ? datagridview.SelectedRows[0].Index : 0;
        }
    }
}
