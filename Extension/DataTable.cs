﻿using System.Collections.Generic;
using System.Linq;

namespace System.Data
{
    public static class extDataTable
    {
        // Merge Dictionary of DataTables by Primary Key
        public static DataTable MergeByPrimaryKey(this DataTable datatable,
                                                  Dictionary<DataTable, string[]> datatables)
        {
            if (!datatables.Any())
            {
                throw new ArgumentException("Tables must not be empty!", "tables");
            }

            string tableName = datatable.TableName;
            string[] columnNames = new string[datatable.Columns.Count];

            for (int x = 0; x < datatable.Columns.Count; x++)
            {
                columnNames[x] = datatable.Columns[x].ColumnName;
            }

            foreach (KeyValuePair<DataTable, string[]> tables in datatables)
            {
                datatable.PrimaryKey = new DataColumn[] { datatable.Columns[tables.Value[0]] };
                datatable.DefaultView.RowFilter = string.Empty;
                tables.Key.PrimaryKey = new DataColumn[] { tables.Key.Columns[tables.Value[0]] };
                tables.Key.DefaultView.RowFilter = string.Empty;

                datatable.Merge(tables.Key, false);
            }

            return datatable.DefaultView.ToTable(tableName, false, columnNames);
        }

        // Merge Dictionary of DataTables by Row Column Check
        public static DataTable MergeByRowColumn(this DataTable datatable,
                                                 Dictionary<DataTable, string[]> datatables)
        {
            string tableName = datatable.TableName;

            foreach (KeyValuePair<DataTable, string[]> tables in datatables)
            {
                foreach (DataRow rowParent in datatable.Rows)
                {
                    foreach (DataRow rowChild in tables.Key.Rows)
                    {
                        if (rowParent[tables.Value[0]].Equals(rowChild[tables.Value[0]]))
                        {
                            for (int i = 1; i < tables.Value.Count(); i++)
                            {
                                rowParent[tables.Value[i]] = rowChild[tables.Value[i]];
                            }

                            break;
                        }
                    }
                }
            }

            return datatable.DefaultView.ToTable(tableName);
        }

        public static DataTable LoadDummyDataTable(this DataTable datatable)
        {
            datatable.Columns.Add("AString");
            datatable.Columns.Add("ADate");
            datatable.Columns.Add("AnInt");

            Random random = new Random();
            DateTime datetime = DateTime.Today;

            for (int i = 0; i < 100; i++)
            {
                datatable.Rows.Add(new object[] {
                                                    new string((char)('A' + random.Next(0, 25)), 8),
                                                    datetime.AddDays(random.Next(-100, 100)),
                                                    random.Next(0, 50)
                                                });
            };

            return datatable;
        }
    }
}
