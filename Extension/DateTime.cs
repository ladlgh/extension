﻿using System.Collections.Generic;
using System.Linq;

namespace System
{
    public static class extDateTime
    {
        public static DateTime GetTimeToFinishDate(this DateTime start_date,
                                                   DateTime estimated_date,
                                                   List<DateTime> locked_days,
                                                   double process_quota,
                                                   bool include_saturday = false,
                                                   bool include_sunday = false)
        {
            DateTime result;

            TimeSpan diff = estimated_date.Subtract(start_date);
            result = start_date.AddSeconds(diff.TotalSeconds * (process_quota / 100.0));

            result = result.AddDays(WeekendDaysCount(start_date.Date, result.Date, include_saturday, include_sunday));
            result = result.AddDays(locked_days.Count(i => (i.Date >= start_date.Date && i.Date < result.Date)));

            while (locked_days.Count(i => i.Date == result.Date) > 0
                    || (!include_saturday && result.DayOfWeek == DayOfWeek.Saturday)
                    || (!include_sunday && result.DayOfWeek == DayOfWeek.Sunday))
            {
                result = result.AddDays(1);
            }

            return result;
        }

        public static DateTime GetNewEstimatedDate(this DateTime estimated_date,
                                                   DateTime finish_date,
                                                   DateTime target_date,
                                                   List<DateTime> locked_days,
                                                   bool includeSaturday = false,
                                                   bool includeSunday = false)
        {
            DateTime result;

            TimeSpan diff = finish_date.Subtract(DateTime.Now);

            if (diff.TotalSeconds >= 0)
            {
                result = (diff.Hours >= 12) ? estimated_date.AddDays(-diff.Days).AddDays(-1).Date
                                            : estimated_date.AddDays(-diff.Days).Date;
            }
            else
            {
                result = (diff.Hours <= -12) ? estimated_date.AddDays(-diff.Days).AddDays(1).Date
                                             : estimated_date.AddDays(-diff.Days).Date;
            }

            if (result > estimated_date)
            {
                result = result.AddDays(WeekendDaysCount(estimated_date.Date, result.Date, includeSaturday, includeSunday));
                result = result.AddDays(locked_days.Count(i => (i.Date >= estimated_date.Date && i.Date <= result.Date)));
            }
            else if (estimated_date > result)
            {
                result = result.AddDays(WeekendDaysCount(result.Date, estimated_date.Date, includeSaturday, includeSunday));
                result = result.AddDays(locked_days.Count(i => (i.Date >= result.Date && i.Date <= estimated_date.Date)));
            }

            while (locked_days.Count(i => i.Date == result.Date) > 0
                    || (!includeSaturday && result.DayOfWeek == DayOfWeek.Saturday)
                    || (!includeSunday && result.DayOfWeek == DayOfWeek.Sunday))
            {
                result = result.AddDays(1);
            }

            return (target_date.Date > result.Date) ? target_date.Date : result.Date;
        }

        public static int WeekendDaysCount(this DateTime start_date,
                                           DateTime end_date,
                                           bool include_saturday = false,
                                           bool include_sunday = false)
        {
            int result = 0;

            for (DateTime dt = start_date; dt < end_date; dt = dt.AddDays(1.0))
            {
                if (!include_saturday
                    && dt.DayOfWeek == DayOfWeek.Saturday)
                {
                    result++;
                }

                if (!include_sunday
                    && dt.DayOfWeek == DayOfWeek.Sunday)
                {
                    result++;
                }
            }

            return result;
        }
    }
}
