﻿using System.IO;
using System.Text;

namespace System.Security.Cryptography
{
    public sealed class CryptoCore : IDisposable
    {
        private static readonly byte[] Key = {
                                                26, 159, 48, 42,  53,  86, 123,  56,
                                                89,  74, 54, 87,  21,  96,  36,  61,
                                                94,  86, 49, 41,  31,  64,  97,  82,
                                                71,  93, 94, 67, 240, 231, 143, 176
                                             };

        private static readonly byte[] IV =  {
                                                164, 183,  59,  94,  72, 81, 83,  64,
                                                123, 222, 234, 137, 129, 15,  3, 134
                                             };
        /// <summary>
        /// Abstract object
        /// </summary>
        private SymmetricAlgorithm algorithm;

        /// <summary>
        /// Default constructor
        /// </summary>
        public CryptoCore()
        {
            // init crypto
            this.algorithm = new RijndaelManaged();
            this.algorithm.Mode = CipherMode.CBC;
            this.algorithm.Key = Key;
            this.algorithm.IV = IV;
        }

        /// <summary>
        /// Release all resources used by the SymmetricAlgorithm class
        /// </summary>
        public void Dispose()
        {
            this.algorithm.Clear();
        }

        /// <summary>
        /// Set Binary Keys
        /// </summary>
        public void SetBinaryKeys(byte[] Key, byte[] IV)
        {
            this.algorithm.Key = Key;
            this.algorithm.IV = IV;
        }

        /// <summary>
        /// Extract Binary Keys
        /// </summary>
        public void ExtractBinaryKeys(out byte[] Key, out byte[] IV)
        {
            Key = this.algorithm.Key;
            IV = this.algorithm.IV;
        }

        /// <summary>
        /// Process the data with CryptoStream
        /// </summary>
        private byte[] Process(byte[] data, int startIndex, int count, ICryptoTransform cryptor)
        {
            //
            // the memory stream granularity must match the block size
            // of the current cryptographic operation
            //
            int capacity = count;
            int mod = count % algorithm.BlockSize;

            if (mod > 0) capacity += (algorithm.BlockSize - mod);

            MemoryStream memoryStream = new MemoryStream(capacity);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptor, CryptoStreamMode.Write);

            cryptoStream.Write(data, startIndex, count);
            cryptoStream.FlushFinalBlock();

            cryptoStream.Close();
            cryptoStream = null;

            cryptor.Dispose();
            cryptor = null;

            return memoryStream.ToArray();
        }

        /// <summary>
        ///  Byte array encryption function
        /// </summary>
        /// <param name="cleanBuffer">input byte array</param>
        /// <returns>output encrypted byte array</returns>
        public byte[] EncryptBuffer(byte[] cleanBuffer)
        {
            byte[] output;

            // Encryptor object
            ICryptoTransform cryptoTransform = this.algorithm.CreateEncryptor();

            // Get the result
            output = this.Process(cleanBuffer, 0, cleanBuffer.Length, cryptoTransform);

            //clean
            cryptoTransform.Dispose();

            return output;
        }

        /// <summary>
        ///  Byte array decryption function
        /// </summary>
        /// <param name="cryptoBuffer">input chiper byte array</param>
        /// <returns>output decrypted byte array</returns>
        public byte[] DecryptBuffer(byte[] cryptoBuffer)
        {
            byte[] output;

            // Decryptor object
            ICryptoTransform cryptoTransform = this.algorithm.CreateDecryptor();

            // Get the result 	
            output = this.Process(cryptoBuffer, 0, cryptoBuffer.Length, cryptoTransform);

            //clean
            cryptoTransform.Dispose();

            return output;
        }

        /// <summary>
        /// string encryption function
        /// </summary>
        /// <param name="plainText">clean text</param>
        /// <returns>base64 encrypted string</returns>
        public string EncryptString(string plainText)
        {
            return Convert.ToBase64String(EncryptBuffer(Encoding.UTF8.GetBytes(plainText)));
        }

        /// <summary>
        /// string decryption function
        /// </summary>
        /// <param name="encyptedText">base64 encrypted string</param>
        /// <returns>decrypted text</returns>
        public string DecryptString(string encyptedText)
        {
            return Encoding.UTF8.GetString(DecryptBuffer(Convert.FromBase64String(encyptedText)));
        }
    }
}
