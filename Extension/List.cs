﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;

namespace System.Collections.Generic
{
    public static class extList
    {
        // load file to list
        public static List<T> DeSerialize<T>(this List<T> list,
                                             string file_path,
                                             bool crypto = false)
        {
            byte[] storage;

            if (crypto)
            {
                using (CryptoCore cryptor = new CryptoCore())
                {
                    storage = cryptor.DecryptBuffer(File.ReadAllBytes(file_path));
                }
            }
            else
            {
                storage = File.ReadAllBytes(file_path);
            }

            using (MemoryStream ms = new MemoryStream(storage))
            {
                BinaryFormatter bf = new BinaryFormatter();

                list = (List<T>)bf.Deserialize(ms);

                return list;
            }
        }

        // write list to file
        public static void Serialize<T>(this List<T> list,
                                        string file_path,
                                        bool crypto = false)
        {
            byte[] storage;

            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, list);
                storage = ms.ToArray();
            }

            if (crypto)
            {
                using (CryptoCore cryptor = new CryptoCore())
                {
                    File.WriteAllBytes(file_path, cryptor.EncryptBuffer(storage));
                }
            }
            else
            {
                File.WriteAllBytes(file_path, storage);
            }
        }
    }
}
