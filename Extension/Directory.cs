﻿using System.Windows.Forms;

namespace System.IO
{
    public static class extDirectory
    {
        public static void CreateHiddenDirectory(this string directory_path)
        {
            if (!Directory.Exists(directory_path))
            {
                try
                {
                    DirectoryInfo di = Directory.CreateDirectory(directory_path);
                    di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Could not create path:\n"
                                    + "'" + directory_path + "'!\n"
                                    + "\n"
                                    + "Error:\n"
                                    + ex.Message,
                                    "Error...",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }
            }
            else
            {
                DirectoryInfo di = new DirectoryInfo(directory_path);

                if ((di.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
                {
                    di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
                }
            }
        }
    }
}
