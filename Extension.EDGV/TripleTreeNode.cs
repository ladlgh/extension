﻿using System;
using System.Windows.Forms;

namespace Extension.EDGV
{
    /// <summary>
    /// 
    /// </summary>
    public enum TripleTreeNodeType : byte
    {
        Default,
        AllsNode,
        EmptysNode,
        MSecDateTimeNode,
        SecDateTimeNode,
        MinDateTimeNode,
        HourDateTimeNode,
        DayDateTimeNode,
        MonthDateTimeNode,
        YearDateTimeNode
    }

    /// <summary>
    /// 
    /// </summary>
    public class TripleTreeNode : TreeNode
    {
        private CheckState checkState = CheckState.Unchecked;
        private TripleTreeNode parent;

        /// <summary>
        /// 
        /// </summary>
        public TripleTreeNodeType NodeType { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public object Value { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        new public TripleTreeNode Parent
        {
            get
            {
                if (this.parent is TripleTreeNode)
                    return this.parent;
                else
                    return null;
            }
            set
            {
                this.parent = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        new public bool Checked
        {
            get
            {
                return this.checkState == CheckState.Checked;
            }

            set
            {
                this.CheckState = (value == true ? CheckState.Checked : CheckState.Unchecked);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public CheckState CheckState
        {
            get
            {
                return this.checkState;
            }
            set
            {
                this.checkState = value;
                SetCheckImage();
            }
        }

        private TripleTreeNode(string Text, object Value, CheckState State, TripleTreeNodeType NodeType)
            : base(Text)
        {
            this.CheckState = State;
            this.NodeType = NodeType;
            this.Value = Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="Value"></param>
        /// <param name="State"></param>
        /// <returns></returns>
        public static TripleTreeNode CreateNode(string Text, object Value, CheckState State = CheckState.Checked)
        {
            return new TripleTreeNode(Text, Value, State, TripleTreeNodeType.Default);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="Value"></param>
        /// <param name="State"></param>
        /// <returns></returns>
        public static TripleTreeNode CreateYearNode(string Text, object Value, CheckState State = CheckState.Checked)
        {
            return new TripleTreeNode(Text, Value, State, TripleTreeNodeType.YearDateTimeNode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="Value"></param>
        /// <param name="State"></param>
        /// <returns></returns>
        public static TripleTreeNode CreateMonthNode(string Text, object Value, CheckState State = CheckState.Checked)
        {
            return new TripleTreeNode(Text, Value, State, TripleTreeNodeType.MonthDateTimeNode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="Value"></param>
        /// <param name="State"></param>
        /// <returns></returns>
        public static TripleTreeNode CreateDayNode(string Text, object Value, CheckState State = CheckState.Checked)
        {
            return new TripleTreeNode(Text, Value, State, TripleTreeNodeType.DayDateTimeNode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="Value"></param>
        /// <param name="State"></param>
        /// <returns></returns>
        public static TripleTreeNode CreateHourNode(string Text, object Value, CheckState State = CheckState.Checked)
        {
            return new TripleTreeNode(Text, Value, State, TripleTreeNodeType.HourDateTimeNode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="Value"></param>
        /// <param name="State"></param>
        /// <returns></returns>
        public static TripleTreeNode CreateMinNode(string Text, object Value, CheckState State = CheckState.Checked)
        {
            return new TripleTreeNode(Text, Value, State, TripleTreeNodeType.MinDateTimeNode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="Value"></param>
        /// <param name="State"></param>
        /// <returns></returns>
        public static TripleTreeNode CreateSecNode(string Text, object Value, CheckState State = CheckState.Checked)
        {
            return new TripleTreeNode(Text, Value, State, TripleTreeNodeType.SecDateTimeNode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="Value"></param>
        /// <param name="State"></param>
        /// <returns></returns>
        public static TripleTreeNode CreateMSecNode(string Text, object Value, CheckState State = CheckState.Checked)
        {
            return new TripleTreeNode(Text, Value, State, TripleTreeNodeType.MSecDateTimeNode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="State"></param>
        /// <returns></returns>
        public static TripleTreeNode CreateEmptysNode(string Text, CheckState State = CheckState.Checked)
        {
            return new TripleTreeNode(Text, null, State, TripleTreeNodeType.EmptysNode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="State"></param>
        /// <returns></returns>
        public static TripleTreeNode CreateAllsNode(string Text, CheckState State = CheckState.Checked)
        {
            return new TripleTreeNode(Text, null, State, TripleTreeNodeType.AllsNode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="Value"></param>
        /// <param name="State"></param>
        /// <returns></returns>
        public TripleTreeNode CreateChildNode(string Text, object Value, CheckState State = CheckState.Checked)
        {
            TripleTreeNode n = null;
            switch (this.NodeType)
            {
                case TripleTreeNodeType.YearDateTimeNode:
                    n = TripleTreeNode.CreateMonthNode(Text, Value, State);
                    break;

                case TripleTreeNodeType.MonthDateTimeNode:
                    n = TripleTreeNode.CreateDayNode(Text, Value, State);
                    break;

                case TripleTreeNodeType.DayDateTimeNode:
                    n = TripleTreeNode.CreateHourNode(Text, Value, State);
                    break;

                case TripleTreeNodeType.HourDateTimeNode:
                    n = TripleTreeNode.CreateMinNode(Text, Value, State);
                    break;

                case TripleTreeNodeType.MinDateTimeNode:
                    n = TripleTreeNode.CreateSecNode(Text, Value, State);
                    break;

                case TripleTreeNodeType.SecDateTimeNode:
                    n = TripleTreeNode.CreateMSecNode(Text, Value, State);
                    break;

                default:
                    n = null;
                    break;
            }

            if (n != null)
                this.AddChild(n);

            return n;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="Value"></param>
        /// <returns></returns>
        public TripleTreeNode CreateChildNode(string Text, object Value)
        {
            return this.CreateChildNode(Text, Value, this.checkState);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public new TripleTreeNode Clone()
        {
            TripleTreeNode n = null;
            switch (this.NodeType)
            {
                case TripleTreeNodeType.YearDateTimeNode:
                    n = TripleTreeNode.CreateYearNode(this.Text, this.Value, this.checkState);
                    break;

                case TripleTreeNodeType.MonthDateTimeNode:
                    n = TripleTreeNode.CreateMonthNode(this.Text, this.Value, this.checkState);
                    break;

                case TripleTreeNodeType.DayDateTimeNode:
                    n = TripleTreeNode.CreateDayNode(this.Text, this.Value, this.checkState);
                    break;

                case TripleTreeNodeType.HourDateTimeNode:
                    n = TripleTreeNode.CreateHourNode(this.Text, this.Value, this.checkState);
                    break;

                case TripleTreeNodeType.MinDateTimeNode:
                    n = TripleTreeNode.CreateMinNode(this.Text, this.Value, this.checkState);
                    break;

                case TripleTreeNodeType.SecDateTimeNode:
                    n = TripleTreeNode.CreateSecNode(this.Text, this.Value, this.checkState);
                    break;

                case TripleTreeNodeType.MSecDateTimeNode:
                    n = TripleTreeNode.CreateMSecNode(this.Text, this.Value, this.checkState);
                    break;

                case TripleTreeNodeType.EmptysNode:
                    n = TripleTreeNode.CreateEmptysNode(this.Text, this.checkState);
                    break;

                case TripleTreeNodeType.AllsNode:
                    n = TripleTreeNode.CreateAllsNode(this.Text, this.checkState);
                    break;

                default:
                    n = TripleTreeNode.CreateNode(this.Text, this.Value, this.checkState);
                    break;
            }

            n.NodeFont = this.NodeFont;

            if (this.GetNodeCount(false) > 0)
            {
                foreach (TripleTreeNode child in this.Nodes)
                    n.AddChild(child.Clone());
            }

            return n;
        }

        private void SetCheckImage()
        {
            switch (this.checkState)
            {
                case CheckState.Checked:
                    this.StateImageIndex = 1;
                    break;

                case CheckState.Indeterminate:
                    this.StateImageIndex = 2;
                    break;

                default:
                    this.StateImageIndex = 0;
                    break;
            }
        }

        private void AddChild(TripleTreeNode child)
        {
            child.Parent = this;
            this.Nodes.Add(child);
        }
    }
}