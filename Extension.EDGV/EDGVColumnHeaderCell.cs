﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Extension.EDGV
{
    /// <summary>
    /// 
    /// </summary>
    public class EDGVColumnHeaderCell : DataGridViewColumnHeaderCell
    {
        private Image filterImage = Resources.AddFilter;
        private Size filterButtonImageSize = new Size(16, 16);
        private bool filterButtonPressed = false;
        private bool filterButtonOver = false;
        private Rectangle filterButtonOffsetBounds = Rectangle.Empty;
        private Rectangle filterButtonImageBounds = Rectangle.Empty;
        private Padding filterButtonMargin = new Padding(1, 1, 1, 1);
        private bool filterEnabled = false;

        /// <summary>
        /// 
        /// </summary>
        public EDGVFilterMenu FilterMenu { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public event EDGVFilterEventHandler FilterPopup;

        /// <summary>
        /// 
        /// </summary>
        public event EDGVFilterEventHandler SortChanged;

        /// <summary>
        /// 
        /// </summary>
        public event EDGVFilterEventHandler FilterChanged;

        /// <summary>
        /// 
        /// </summary>
        public Size MinimumSize
        {
            get
            {
                return new Size(this.filterButtonImageSize.Width + this.filterButtonMargin.Left + this.filterButtonMargin.Right,
                    this.filterButtonImageSize.Height + this.filterButtonMargin.Bottom + this.filterButtonMargin.Top);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public EDGVFilterMenuSortType ActiveSortType
        {
            get
            {
                if (this.FilterMenu != null && this.FilterEnabled)
                    return this.FilterMenu.ActiveSortType;
                else
                    return EDGVFilterMenuSortType.NONE;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public EDGVFilterMenuFilterType ActiveFilterType
        {
            get
            {
                if (this.FilterMenu != null && this.FilterEnabled)
                    return this.FilterMenu.ActiveFilterType;
                else
                    return EDGVFilterMenuFilterType.NONE;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string SortString
        {
            get
            {
                if (this.FilterMenu != null && this.FilterEnabled)
                    return this.FilterMenu.SortString;
                else
                    return "";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string FilterString
        {
            get
            {
                if (this.FilterMenu != null && this.FilterEnabled)
                    return this.FilterMenu.FilterString;
                else
                    return "";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool FilterEnabled
        {
            get
            {
                return this.filterEnabled;
            }
            set
            {
                if (!value)
                {
                    this.filterButtonPressed = false;
                    this.filterButtonOver = false;
                }

                if (value != this.filterEnabled)
                {
                    this.filterEnabled = value;
                    bool refreshed = false;
                    if (this.FilterMenu.FilterString.Length > 0)
                    {
                        FilterMenu_FilterChanged(this, new EventArgs());
                        refreshed = true;
                    }
                    if (this.FilterMenu.SortString.Length > 0)
                    {
                        FilterMenu_SortChanged(this, new EventArgs());
                        refreshed = true;
                    }
                    if (!refreshed)
                        this.RepaintCell();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool DateWithTime
        {
            get
            {
                return this.FilterMenu.DateWithTime;
            }
            set
            {
                this.FilterMenu.DateWithTime = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool TimeFilter
        {
            get
            {
                return this.FilterMenu.TimeFilter;
            }
            set
            {
                this.FilterMenu.TimeFilter = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        ~EDGVColumnHeaderCell()
        {
            if (this.FilterMenu != null)
            {
                this.FilterMenu.FilterChanged -= FilterMenu_FilterChanged;
                this.FilterMenu.SortChanged -= FilterMenu_SortChanged;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldCell"></param>
        /// <param name="FilterEnabled"></param>
        public EDGVColumnHeaderCell(DataGridViewColumnHeaderCell oldCell, bool FilterEnabled = false)
        {
            this.Tag = oldCell.Tag;
            this.ErrorText = oldCell.ErrorText;
            this.ToolTipText = oldCell.ToolTipText;
            this.Value = oldCell.Value;
            this.ValueType = oldCell.ValueType;
            this.ContextMenuStrip = oldCell.ContextMenuStrip;
            this.Style = oldCell.Style;
            this.filterEnabled = FilterEnabled;

            EDGVColumnHeaderCell oldEDGVCell = oldCell as EDGVColumnHeaderCell;

            if (oldEDGVCell != null && oldEDGVCell.FilterMenu != null)
            {
                this.FilterMenu = oldEDGVCell.FilterMenu;
                this.filterImage = oldEDGVCell.filterImage;
                this.filterButtonPressed = oldEDGVCell.filterButtonPressed;
                this.filterButtonOver = oldEDGVCell.filterButtonOver;
                this.filterButtonOffsetBounds = oldEDGVCell.filterButtonOffsetBounds;
                this.filterButtonImageBounds = oldEDGVCell.filterButtonImageBounds;

                this.FilterMenu.FilterChanged += new EventHandler(FilterMenu_FilterChanged);
                this.FilterMenu.SortChanged += new EventHandler(FilterMenu_SortChanged);
            }
            else
            {
                this.FilterMenu = new EDGVFilterMenu(oldCell.OwningColumn.ValueType);
                this.FilterMenu.FilterChanged += new EventHandler(FilterMenu_FilterChanged);
                this.FilterMenu.SortChanged += new EventHandler(FilterMenu_SortChanged);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new EDGVColumnHeaderCell(this, this.FilterEnabled);
        }

        private void RepaintCell()
        {
            if (this.Displayed && this.DataGridView != null)
                this.DataGridView.InvalidateCell(this);
        }

        internal void ClearSorting()
        {
            if (this.FilterMenu != null && this.FilterEnabled)
            {
                this.FilterMenu.ClearSorting();
                this.RefreshImage();
                this.RepaintCell();
            }
        }

        internal void ClearFilter()
        {
            if (this.FilterMenu != null && this.FilterEnabled)
            {
                this.FilterMenu.ClearFilter();
                this.RefreshImage();
                this.RepaintCell();
            }
        }

        private void RefreshImage()
        {
            if (this.ActiveFilterType == EDGVFilterMenuFilterType.Loaded)
            {
                this.filterImage = Resources.SavedFilter;
            }
            else
                if (this.ActiveFilterType == EDGVFilterMenuFilterType.NONE)
                {
                    if (this.ActiveSortType == EDGVFilterMenuSortType.NONE)
                        this.filterImage = Resources.AddFilter;
                    else if (this.ActiveSortType == EDGVFilterMenuSortType.ASC)
                        this.filterImage = Resources.ASC;
                    else
                        this.filterImage = Resources.DESC;
                }
                else
                {
                    if (this.ActiveSortType == EDGVFilterMenuSortType.NONE)
                        this.filterImage = Resources.Filter;
                    else if (this.ActiveSortType == EDGVFilterMenuSortType.ASC)
                        this.filterImage = Resources.FilterASC;
                    else
                        this.filterImage = Resources.FilterDESC;
                }
        }

        private void FilterMenu_FilterChanged(object sender, EventArgs e)
        {
            RefreshImage();
            this.RepaintCell();
            if (this.FilterEnabled && this.FilterChanged != null)
                this.FilterChanged(this, new EDGVFilterEventArgs(this.FilterMenu, this.OwningColumn));
        }

        private void FilterMenu_SortChanged(object sender, EventArgs e)
        {
            RefreshImage();
            this.RepaintCell();
            if (this.FilterEnabled && this.SortChanged != null)
                this.SortChanged(this, new EDGVFilterEventArgs(this.FilterMenu, this.OwningColumn));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="graphics"></param>
        /// <param name="clipBounds"></param>
        /// <param name="cellBounds"></param>
        /// <param name="rowIndex"></param>
        /// <param name="cellState"></param>
        /// <param name="value"></param>
        /// <param name="formattedValue"></param>
        /// <param name="errorText"></param>
        /// <param name="cellStyle"></param>
        /// <param name="advancedBorderStyle"></param>
        /// <param name="paintParts"></param>
        protected override void Paint(Graphics graphics,
                                      Rectangle clipBounds,
                                      Rectangle cellBounds,
                                      int rowIndex,
                                      DataGridViewElementStates cellState,
                                      object value,
                                      object formattedValue,
                                      string errorText,
                                      DataGridViewCellStyle cellStyle,
                                      DataGridViewAdvancedBorderStyle advancedBorderStyle,
                                      DataGridViewPaintParts paintParts)
        {
            if (this.FilterEnabled
                && this.SortGlyphDirection != SortOrder.None)
            {
                this.SortGlyphDirection = SortOrder.None;
            }

            base.Paint(graphics,
                       clipBounds,
                       cellBounds,
                       rowIndex,
                       cellState,
                       value,
                       formattedValue,
                       errorText,
                       cellStyle,
                       advancedBorderStyle,
                       paintParts);

            if (this.FilterEnabled
                && paintParts.HasFlag(DataGridViewPaintParts.ContentBackground))
            {
                Rectangle[] rects = GetFilterBounds();

                this.filterButtonOffsetBounds = rects[0];
                Rectangle buttonBounds        = this.filterButtonOffsetBounds;
                buttonBounds.Inflate(-1, -1);

                this.filterButtonImageBounds = rects[2];
                Rectangle imageBounds        = rects[1];
                imageBounds.Inflate(-1, -1);

                if (buttonBounds != null
                    && clipBounds.IntersectsWith(buttonBounds))
                {
                    int d_d  = (-15),
                        d_r  = Math.Max(Math.Min(cellStyle.BackColor.R + d_d, 254), 0),
                        d_g  = Math.Max(Math.Min(cellStyle.BackColor.G + d_d, 254), 0),
                        d_b  = Math.Max(Math.Min(cellStyle.BackColor.B + d_d, 254), 0),
                        mo_d = (-30),
                        mo_r = Math.Max(Math.Min(cellStyle.BackColor.R + mo_d, 254), 0),
                        mo_g = Math.Max(Math.Min(cellStyle.BackColor.G + mo_d, 254), 0),
                        mo_b = Math.Max(Math.Min(cellStyle.BackColor.B + mo_d, 254), 0);

                    Color clr_default   = Color.FromArgb(d_r, d_g, d_b),
                          clr_mouseover = Color.FromArgb(mo_r, mo_g, mo_b);

                    using (Brush b = new SolidBrush(this.filterButtonOver ? clr_mouseover : clr_default))
                    {
                        graphics.FillRectangle(b, buttonBounds);
                    }

                    graphics.DrawImage(this.filterImage, imageBounds);
                }
            }
        }

        private Rectangle[] GetFilterBounds()
        {
            Rectangle cell = this.DataGridView.GetCellDisplayRectangle(this.ColumnIndex, -1, false);

            // get button rectangle
            Size s_btn = new Size(this.filterButtonImageSize.Width, cell.Height - (2 * this.filterButtonMargin.Top));
            Point p_btn = new Point(cell.Right - this.filterButtonImageSize.Width - this.filterButtonMargin.Right,
                                    cell.Top + this.filterButtonMargin.Top);

            // get image rectangle
            Size s_img = this.filterButtonImageSize;
            Point p_img = new Point(cell.Right - this.filterButtonImageSize.Width - this.filterButtonMargin.Right,
                                    cell.Top + this.filterButtonMargin.Top + (cell.Height - this.filterButtonImageSize.Height) / 2);

            // get image rectangle
            Size s_cms = s_btn;
            Point p_cms = new Point(this.OwningColumn.Width - this.filterButtonImageSize.Width - this.filterButtonMargin.Right,
                                    this.filterButtonMargin.Top + (cell.Height - this.filterButtonImageSize.Height) / 2);

            return new Rectangle[] { new Rectangle(p_btn, s_btn), new Rectangle(p_img, s_img), new Rectangle(p_cms, s_cms) };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseMove(DataGridViewCellMouseEventArgs e)
        {
            if (this.FilterEnabled)
            {
                if (this.filterButtonImageBounds.Contains(e.X, e.Y) && !this.filterButtonOver)
                {
                    this.filterButtonOver = true;
                    this.RepaintCell();
                }
                else if (!this.filterButtonImageBounds.Contains(e.X, e.Y) && this.filterButtonOver)
                {
                    this.filterButtonOver = false;
                    this.RepaintCell();
                }
            }
            base.OnMouseMove(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseDown(DataGridViewCellMouseEventArgs e)
        {
            if (this.FilterEnabled)
            {
                if (this.filterButtonImageBounds.Contains(e.X, e.Y) && e.Button == MouseButtons.Left && !this.filterButtonPressed)
                {
                    this.filterButtonPressed = true;
                    this.filterButtonOver = true;
                    this.RepaintCell();
                }
            }
            else
                base.OnMouseDown(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseUp(DataGridViewCellMouseEventArgs e)
        {
            if (this.FilterEnabled)
            {
                if (e.Button == MouseButtons.Left && this.filterButtonPressed)
                {
                    this.filterButtonPressed = false;
                    this.filterButtonOver = false;
                    this.RepaintCell();
                    if (this.filterButtonImageBounds.Contains(e.X, e.Y) && this.FilterPopup != null)
                    {
                        this.FilterPopup(this, new EDGVFilterEventArgs(this.FilterMenu, this.OwningColumn));
                    }
                }
            }
            else
                base.OnMouseUp(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rowIndex"></param>
        protected override void OnMouseLeave(int rowIndex)
        {
            if (this.FilterEnabled && this.filterButtonOver)
            {
                this.filterButtonOver = false;
                this.RepaintCell();
            }

            base.OnMouseLeave(rowIndex);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Enabled"></param>
        public void SetLoadedFilterMode(bool Enabled)
        {
            this.FilterMenu.SetLoadedFilterMode(Enabled);
            this.RefreshImage();
            this.RepaintCell();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void EDGVFilterEventHandler(object sender, EDGVFilterEventArgs e);

    /// <summary>
    /// 
    /// </summary>
    public class EDGVFilterEventArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        public EDGVFilterMenu FilterMenu { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public DataGridViewColumn Column { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterMenu"></param>
        /// <param name="column"></param>
        public EDGVFilterEventArgs(EDGVFilterMenu filterMenu, DataGridViewColumn column)
        {
            this.FilterMenu = filterMenu;
            this.Column = column;
        }
    }
}