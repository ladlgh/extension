﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Extension.EDGV
{
    public static class DrawingControl
    {
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd,
                                             int wMsg,
                                             bool wParam,
                                             int lParam);

        private const int WM_SETREDRAW = 11;

        public static void DoubleBuffered(this Control control, bool status)
        {
            control.DoubleBuffering(status);
            control.SuspendDrawing(status);
            control.ResumeDrawing(status);
        }

        /// <summary>
        /// Some controls, such as the DataGridView, do not allow setting the DoubleBuffered property.
        /// It is set as a protected property. This method is a work-around to allow setting it.
        /// Call this in the constructor just after InitializeComponent().
        /// </summary>
        /// <param name="control">The Control on which to set DoubleBuffered to true.</param>
        private static void DoubleBuffering(this Control control, bool status)
        {
            // if not remote desktop session then enable double-buffering optimization
            if (!SystemInformation.TerminalServerSession)
            {
                // set instance non-public property with name "DoubleBuffered" to true
                typeof(Control).InvokeMember("DoubleBuffered",
                                             BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                                             null,
                                             control,
                                             new object[] { status });
            }
        }

        /// <summary>
        /// Suspend drawing updates for the specified control. After the control has been updated
        /// call DrawingControl.ResumeDrawing(Control control).
        /// </summary>
        /// <param name="control">The control to suspend draw updates on.</param>
        private static void SuspendDrawing(this Control control, bool status)
        {
            SendMessage(control.Handle, WM_SETREDRAW, !status, 0);
        }

        /// <summary>
        /// Resume drawing updates for the specified control.
        /// </summary>
        /// <param name="control">The control to resume draw updates on.</param>
        private static void ResumeDrawing(this Control control, bool status)
        {
            SendMessage(control.Handle, WM_SETREDRAW, status, 0);
            control.Refresh();
        }
    }
}
