﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ExtendedDataGridView")]
[assembly: AssemblyDescription("Extended DataGridView")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Lex Application Development Lab")]
[assembly: AssemblyProduct("Extension.EDGV")]
[assembly: AssemblyCopyright("Copyright © LADL 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("d257509c-f016-497e-abac-2bc58da14a84")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("01.2018.18.01")]
[assembly: AssemblyFileVersion("01.2018.18.01")]
