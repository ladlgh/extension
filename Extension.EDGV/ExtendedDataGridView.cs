﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Extension.EDGV
{
    /// <summary>
    /// 
    /// </summary>
    public class ExtendedDataGridView : DataGridView
    {
        private List<string> sortOrder = new List<string>();
        private List<string> filterOrder = new List<string>();
        private List<string> readyToShowFilters = new List<string>();

        private string sortString = null;
        private string filterString = null;
        private bool atoGenerateContextFilters = true;
        private bool dateWithTime = false;
        private bool timeFilter = false;
        private bool loadedFilter = false;

        /// <summary>
        /// 
        /// </summary>
        public bool AutoGenerateContextFilters
        {
            get
            {
                return this.atoGenerateContextFilters;
            }
            set
            {
                this.atoGenerateContextFilters = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool DateWithTime
        {
            get
            {
                return this.dateWithTime;
            }
            set
            {
                this.dateWithTime = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool TimeFilter
        {
            get
            {
                return this.timeFilter;
            }
            set
            {
                this.timeFilter = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler SortStringChanged;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler FilterStringChanged;

        private IEnumerable<EDGVColumnHeaderCell> filterCells
        {
            get
            {
                return from DataGridViewColumn c in this.Columns
                       where c.HeaderCell != null && c.HeaderCell is EDGVColumnHeaderCell
                       select (c.HeaderCell as EDGVColumnHeaderCell);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string SortString
        {
            get
            {
                return this.sortString == null ? "" : this.sortString;
            }
            private set
            {
                string old = value;

                if (old != this.sortString)
                {
                    this.sortString = value;

                    if (this.SortedColumn != null)
                    {
                        this.SortedColumn.HeaderCell.SortGlyphDirection = System.Windows.Forms.SortOrder.None;
                    }

                    if (this.SortStringChanged != null)
                        SortStringChanged(this, new EventArgs());
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string FilterString
        {
            get
            {
                return this.filterString == null ? "" : this.filterString;
            }
            private set
            {
                string old = value;

                if (old != this.filterString)
                {
                    this.filterString = value;

                    if (this.FilterStringChanged != null)
                        FilterStringChanged(this, new EventArgs());
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ExtendedDataGridView()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnColumnAdded(DataGridViewColumnEventArgs e)
        {
            e.Column.SortMode = DataGridViewColumnSortMode.Programmatic;
            EDGVColumnHeaderCell cell = new EDGVColumnHeaderCell(e.Column.HeaderCell, this.AutoGenerateContextFilters);
            cell.DateWithTime = this.DateWithTime;
            cell.TimeFilter = this.TimeFilter;
            cell.SortChanged += new EDGVFilterEventHandler(eSortChanged);
            cell.FilterChanged += new EDGVFilterEventHandler(eFilterChanged);
            cell.FilterPopup += new EDGVFilterEventHandler(eFilterPopup);
            e.Column.MinimumWidth = cell.MinimumSize.Width;
            if (this.ColumnHeadersHeight < cell.MinimumSize.Height)
                this.ColumnHeadersHeight = cell.MinimumSize.Height;
            e.Column.HeaderCell = cell;

            base.OnColumnAdded(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnColumnRemoved(DataGridViewColumnEventArgs e)
        {
            this.readyToShowFilters.Remove(e.Column.Name);
            this.filterOrder.Remove(e.Column.Name);
            this.sortOrder.Remove(e.Column.Name);

            EDGVColumnHeaderCell cell = e.Column.HeaderCell as EDGVColumnHeaderCell;
            if (cell != null)
            {
                cell.SortChanged -= eSortChanged;
                cell.FilterChanged -= eFilterChanged;
                cell.FilterPopup -= eFilterPopup;
            }
            base.OnColumnRemoved(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRowsAdded(DataGridViewRowsAddedEventArgs e)
        {
            this.readyToShowFilters.Clear();
            base.OnRowsAdded(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRowsRemoved(DataGridViewRowsRemovedEventArgs e)
        {
            this.readyToShowFilters.Clear();
            base.OnRowsRemoved(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnCellValueChanged(DataGridViewCellEventArgs e)
        {
            this.readyToShowFilters.Remove(this.Columns[e.ColumnIndex].Name);
            base.OnCellValueChanged(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void eFilterPopup(object sender, EDGVFilterEventArgs e)
        {
            if (this.Columns.Contains(e.Column))
            {
                EDGVFilterMenu FilterMenu = e.FilterMenu;
                DataGridViewColumn Column = e.Column;

                System.Drawing.Rectangle rect = this.GetCellDisplayRectangle(Column.Index, -1, true);

                if (this.readyToShowFilters.Contains(Column.Name))
                    FilterMenu.Show(this, rect.Left, rect.Bottom, false);
                else
                {
                    this.readyToShowFilters.Add(Column.Name);

                    if (filterOrder.Count() > 0 && filterOrder.Last() == Column.Name)
                        FilterMenu.Show(this, rect.Left, rect.Bottom, true);
                    else
                        FilterMenu.Show(this, rect.Left, rect.Bottom, EDGVFilterMenu.GetValuesForFilter(this, Column.Name));
                }
            }
        }

        private void eFilterChanged(object sender, EDGVFilterEventArgs e)
        {
            if (this.Columns.Contains(e.Column))
            {
                EDGVFilterMenu FilterMenu = e.FilterMenu;
                DataGridViewColumn Column = e.Column;

                this.filterOrder.Remove(Column.Name);
                if (FilterMenu.ActiveFilterType != EDGVFilterMenuFilterType.NONE)
                    this.filterOrder.Add(Column.Name);

                this.FilterString = CreateFilterString();

                if (this.loadedFilter)
                {
                    this.loadedFilter = false;
                    foreach (EDGVColumnHeaderCell c in this.filterCells.Where(f => f.FilterMenu != FilterMenu))
                        c.SetLoadedFilterMode(false);
                }
            }
        }

        private void eSortChanged(object sender, EDGVFilterEventArgs e)
        {
            if (this.Columns.Contains(e.Column))
            {
                EDGVFilterMenu FilterMenu = e.FilterMenu;
                DataGridViewColumn Column = e.Column;

                this.sortOrder.Remove(Column.Name);
                if (FilterMenu.ActiveSortType != EDGVFilterMenuSortType.NONE)
                    this.sortOrder.Add(Column.Name);
                this.SortString = CreateSortString();
            }
        }

        private string CreateFilterString()
        {
            StringBuilder sb = new StringBuilder("");
            string appx = "";

            foreach (string name in this.filterOrder)
            {
                DataGridViewColumn Column = this.Columns[name];

                if (Column != null)
                {
                    EDGVColumnHeaderCell cell = Column.HeaderCell as EDGVColumnHeaderCell;

                    if (cell != null)
                    {
                        if (cell.FilterEnabled
                            && cell.ActiveFilterType != EDGVFilterMenuFilterType.NONE)
                        {
                            sb.AppendFormat(appx + cell.FilterString, Column.DataPropertyName);
                            appx = " AND ";
                        }
                    }
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string CreateSortString()
        {
            StringBuilder sb = new StringBuilder("");
            string appx = "";

            foreach (string name in this.sortOrder)
            {
                DataGridViewColumn Column = this.Columns[name];

                if (Column != null)
                {
                    EDGVColumnHeaderCell cell = Column.HeaderCell as EDGVColumnHeaderCell;
                    if (cell != null)
                    {
                        if (cell.FilterEnabled && cell.ActiveSortType != EDGVFilterMenuSortType.NONE)
                        {
                            sb.AppendFormat(appx + cell.SortString, Column.DataPropertyName);
                            appx = ", ";
                        }
                    }
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Column"></param>
        public void EnableFilter(DataGridViewColumn Column)
        {
            if (this.Columns.Contains(Column))
            {
                EDGVColumnHeaderCell c = Column.HeaderCell as EDGVColumnHeaderCell;
                if (c != null)
                    this.EnableFilter(Column, c.DateWithTime, c.TimeFilter);
                else
                    this.EnableFilter(Column, this.DateWithTime, this.TimeFilter);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Column"></param>
        /// <param name="DateWithTime"></param>
        /// <param name="TimeFilter"></param>
        public void EnableFilter(DataGridViewColumn Column, bool DateWithTime, bool TimeFilter)
        {
            if (this.Columns.Contains(Column))
            {
                EDGVColumnHeaderCell cell = Column.HeaderCell as EDGVColumnHeaderCell;
                if (cell != null)
                {
                    if (cell.DateWithTime != DateWithTime || cell.TimeFilter != TimeFilter || (!cell.FilterEnabled && (cell.FilterString.Length > 0 || cell.SortString.Length > 0)))
                        this.ClearFilter(true);

                    cell.DateWithTime = DateWithTime;
                    cell.TimeFilter = TimeFilter;
                    cell.FilterEnabled = true;
                    this.readyToShowFilters.Remove(Column.Name);
                }
                else
                {
                    Column.SortMode = DataGridViewColumnSortMode.Programmatic;
                    cell = new EDGVColumnHeaderCell(Column.HeaderCell, true);
                    cell.DateWithTime = this.DateWithTime;
                    cell.TimeFilter = this.TimeFilter;
                    cell.SortChanged += new EDGVFilterEventHandler(eSortChanged);
                    cell.FilterChanged += new EDGVFilterEventHandler(eFilterChanged);
                    cell.FilterPopup += new EDGVFilterEventHandler(eFilterPopup);
                    Column.MinimumWidth = cell.MinimumSize.Width;
                    if (this.ColumnHeadersHeight < cell.MinimumSize.Height)
                        this.ColumnHeadersHeight = cell.MinimumSize.Height;
                    Column.HeaderCell = cell;
                }
                Column.SortMode = DataGridViewColumnSortMode.Programmatic;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnSorted(EventArgs e)
        {
            this.ClearSort();
            base.OnSorted(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Column"></param>
        public void DisableFilter(DataGridViewColumn Column)
        {
            if (this.Columns.Contains(Column))
            {
                EDGVColumnHeaderCell cell = Column.HeaderCell as EDGVColumnHeaderCell;
                if (cell != null)
                {
                    if (cell.FilterEnabled == true && (cell.SortString.Length > 0 || cell.FilterString.Length > 0))
                    {
                        this.ClearFilter(true);
                        cell.FilterEnabled = false;
                    }
                    else
                        cell.FilterEnabled = false;
                    this.filterOrder.Remove(Column.Name);
                    this.sortOrder.Remove(Column.Name);
                    this.readyToShowFilters.Remove(Column.Name);
                }
                Column.SortMode = DataGridViewColumnSortMode.Automatic;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Filter"></param>
        /// <param name="Sorting"></param>
        public void LoadFilter(string Filter, string Sorting = null)
        {
            foreach (EDGVColumnHeaderCell c in this.filterCells)
                c.SetLoadedFilterMode(true);

            this.filterOrder.Clear();
            this.sortOrder.Clear();
            this.readyToShowFilters.Clear();

            if (Filter != null)
                this.FilterString = Filter;
            if (Sorting != null)
                this.SortString = Sorting;

            this.loadedFilter = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="FireEvent"></param>
        public void ClearSort(bool FireEvent = false)
        {
            foreach (EDGVColumnHeaderCell c in this.filterCells)
                c.ClearSorting();
            this.sortOrder.Clear();

            if (FireEvent)
                this.SortString = null;
            else
                this.sortString = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="FireEvent"></param>
        public void ClearFilter(bool FireEvent = false)
        {
            foreach (EDGVColumnHeaderCell c in this.filterCells)
            {
                c.ClearFilter();
            }
            this.filterOrder.Clear();

            if (FireEvent)
                this.FilterString = null;
            else
                this.filterString = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ValueToFind"></param>
        /// <param name="ColumnName"></param>
        /// <param name="RowIndex"></param>
        /// <param name="ColumnIndex"></param>
        /// <param name="isWholeWordSearch"></param>
        /// <param name="isCaseSensitive"></param>
        /// <returns></returns>
        public DataGridViewCell FindCell(string ValueToFind,
                                         string ColumnName = null,
                                         int RowIndex = 0,
                                         int ColumnIndex = 0,
                                         bool isWholeWordSearch = true,
                                         bool isCaseSensitive = false)
        {
            if (ValueToFind != null
                && this.RowCount > 0
                && this.ColumnCount > 0
                && (ColumnName == null || (this.Columns.Contains(ColumnName)
                && this.Columns[ColumnName].Visible)))
            {
                RowIndex = Math.Max(0, RowIndex);

                if (!isCaseSensitive)
                    ValueToFind = ValueToFind.ToLower();

                if (ColumnName != null)
                {
                    int c = this.Columns[ColumnName].Index;
                    if (ColumnIndex > c)
                        RowIndex++;
                    for (int r = RowIndex; r < this.RowCount; r++)
                    {
                        string value = this.Rows[r].Cells[c].FormattedValue.ToString();
                        if (!isCaseSensitive)
                            value = value.ToLower();

                        if ((!isWholeWordSearch && value.Contains(ValueToFind)) || value.Equals(ValueToFind))
                            return this.Rows[r].Cells[c];
                    }
                }
                else
                {
                    ColumnIndex = Math.Max(0, ColumnIndex);

                    for (int r = RowIndex; r < this.RowCount; r++)
                    {
                        for (int c = ColumnIndex; c < this.ColumnCount; c++)
                        {
                            string value = this.Rows[r].Cells[c].FormattedValue.ToString();
                            if (!isCaseSensitive)
                                value = value.ToLower();

                            if ((!isWholeWordSearch && value.Contains(ValueToFind)) || value.Equals(ValueToFind))
                                return this.Rows[r].Cells[c];
                        }

                        ColumnIndex = 0;
                    }
                }
            }

            return null;
        }
    }
}