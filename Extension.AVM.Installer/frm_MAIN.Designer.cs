﻿namespace Extension.AVM.Installer
{
    partial class frm_MAIN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_MAIN));
            this.pb_Icon = new System.Windows.Forms.PictureBox();
            this.lbl_Info = new System.Windows.Forms.Label();
            this.pb_Progress = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Icon)).BeginInit();
            this.SuspendLayout();
            // 
            // pb_Icon
            // 
            this.pb_Icon.Image = ((System.Drawing.Image)(resources.GetObject("pb_Icon.Image")));
            this.pb_Icon.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pb_Icon.Location = new System.Drawing.Point(10, 10);
            this.pb_Icon.Name = "pb_Icon";
            this.pb_Icon.Size = new System.Drawing.Size(50, 50);
            this.pb_Icon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pb_Icon.TabIndex = 1;
            this.pb_Icon.TabStop = false;
            // 
            // lbl_Info
            // 
            this.lbl_Info.AutoSize = true;
            this.lbl_Info.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lbl_Info.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbl_Info.Location = new System.Drawing.Point(66, 13);
            this.lbl_Info.Name = "lbl_Info";
            this.lbl_Info.Size = new System.Drawing.Size(74, 19);
            this.lbl_Info.TabIndex = 3;
            this.lbl_Info.Text = "{0} Install...";
            // 
            // pb_Progress
            // 
            this.pb_Progress.BackColor = System.Drawing.Color.Gainsboro;
            this.pb_Progress.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pb_Progress.Location = new System.Drawing.Point(70, 34);
            this.pb_Progress.Name = "pb_Progress";
            this.pb_Progress.Size = new System.Drawing.Size(408, 23);
            this.pb_Progress.TabIndex = 4;
            // 
            // frm_MAIN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(490, 67);
            this.Controls.Add(this.pb_Progress);
            this.Controls.Add(this.lbl_Info);
            this.Controls.Add(this.pb_Icon);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(500, 100);
            this.MinimizeBox = false;
            this.Name = "frm_MAIN";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Installing...";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Install_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_Install_FormClosed);
            this.Load += new System.EventHandler(this.frm_Install_Load);
            this.Shown += new System.EventHandler(this.frm_Install_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pb_Icon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pb_Icon;
        private System.Windows.Forms.Label lbl_Info;
        private System.Windows.Forms.ProgressBar pb_Progress;
    }
}