﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Cache;
using System.Windows.Forms;
using Extension.Properties;

namespace Extension.AVM.Installer
{
    public partial class frm_MAIN : Form
    {
        #region   INIT

        private static string AppPath;
        private static string DownloadURL;
        private static string TempPath;
        private static string InstallFilePath;
        
        private WebClient webClient;
        private BackgroundWorker _backgroundWorker;

        public frm_MAIN()
        {
            InitializeComponent();
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   FORM

        private void frm_Install_Load(object sender, EventArgs e)
        {
            string[] args = Environment.GetCommandLineArgs();

            // only for test
            /*MessageBox.Show("frm_Installer\n"
                            + "args[0] == " + args[0] + "\n"
                            + "args[1] == " + args[1] + "\n"
                            + "args[2] == " + args[2] + "\n"
                            + "args[3] == " + args[3] + "\n");*/

            if (args.Length < 4
                || !File.Exists(args[0])
                || !Path.GetExtension(args[0]).ToLower().Equals(".exe"))
            {
                DialogResult = DialogResult.Abort;
                Application.Exit();
            }
            else
            {
                AppPath = args[1];
                DownloadURL = args[2];
                TempPath = args[3];
            }

            //this.Text = string.Format(this.Text, Path.GetFileNameWithoutExtension(AppFilePath));
            lbl_Info.Text = Resources.status_download;
        }

        private void frm_Install_Shown(object sender, EventArgs e)
        {
            DownloadFile(DownloadURL);
        }

        private void frm_Install_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!DialogResult.Equals(DialogResult.Abort))
            {
                if (webClient.IsBusy) webClient.CancelAsync();
                if (_backgroundWorker.IsBusy) _backgroundWorker.CancelAsync();
            }
        }

        private void frm_Install_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!DialogResult.Equals(DialogResult.OK) && !DialogResult.Equals(DialogResult.Abort))
            {
                try
                {
                    MessageBox.Show("Installation aborted!", "Warning...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                catch (Win32Exception exception)
                {
                    if (exception.NativeErrorCode != 1223) throw;
                }
            }
            else
            {
                MessageBox.Show("Installation successfully!", "Information...", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   DOWNLOAD

        private void DownloadFile(string downloadURL)
        {
            webClient = new WebClient() { CachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore) };
            
            Uri uri = new Uri(downloadURL);
            InstallFilePath = Path.Combine(TempPath, GetFileName(downloadURL));

            webClient.DownloadProgressChanged += OnDownloadProgressChanged;
            webClient.DownloadFileCompleted += OnDownloadComplete;

            webClient.DownloadFileAsync(uri, InstallFilePath);
        }

        private void OnDownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            pb_Progress.Value = e.ProgressPercentage;
        }

        private void OnDownloadComplete(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                return;
            }

            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message, e.Error.GetType().ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            InstallFiles();

        }

        private static string GetFileName(string url, string httpWebRequestMethod = "HEAD")
        {
            try
            {
                var fileName = string.Empty;
                var uri = new Uri(url);

                if (uri.Scheme.Equals(Uri.UriSchemeHttp) || uri.Scheme.Equals(Uri.UriSchemeHttps))
                {
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
                    httpWebRequest.CachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore);
                    httpWebRequest.Method = httpWebRequestMethod;
                    httpWebRequest.AllowAutoRedirect = false;
                    string contentDisposition;

                    using (var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse())
                    {
                        if (httpWebResponse.StatusCode.Equals(HttpStatusCode.Redirect) ||
                            httpWebResponse.StatusCode.Equals(HttpStatusCode.Moved) ||
                            httpWebResponse.StatusCode.Equals(HttpStatusCode.MovedPermanently))
                        {
                            if (httpWebResponse.Headers["Location"] != null)
                            {
                                var location = httpWebResponse.Headers["Location"];
                                fileName = GetFileName(location);

                                return fileName;
                            }
                        }

                        contentDisposition = httpWebResponse.Headers["content-disposition"];
                    }

                    if (!string.IsNullOrEmpty(contentDisposition))
                    {
                        const string lookForFileName = "filename=";
                        var index = contentDisposition.IndexOf(lookForFileName, StringComparison.CurrentCultureIgnoreCase);

                        if (index >= 0)
                        {
                            fileName = contentDisposition.Substring(index + lookForFileName.Length);
                        }

                        if (fileName.StartsWith("\"") && fileName.EndsWith("\""))
                        {
                            fileName = fileName.Substring(1, fileName.Length - 2);
                        }
                    }
                }

                if (string.IsNullOrEmpty(fileName))
                {
                    fileName = Path.GetFileName(uri.LocalPath);
                }

                return fileName;
            }
            catch (WebException)
            {
                return GetFileName(url, "GET");
            }
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   INSTALL

        private void InstallFiles()
        {
            // set form style
            lbl_Info.Text = Resources.status_install;
            pb_Progress.Value = 0;
            
            // extract all the files
            _backgroundWorker = new BackgroundWorker
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };

            _backgroundWorker.DoWork += (o, eventArgs) =>
            {
                string extract_path = AppPath;

                // open an existing zip file for reading
                ZipStorer zip = ZipStorer.Open(InstallFilePath, FileAccess.Read);

                // read the central directory collection
                List<ZipStorer.ZipFileEntry> dir = zip.ReadCentralDir();

                for (var index = 0; index < dir.Count; index++)
                {
                    if (_backgroundWorker.CancellationPending)
                    {
                        eventArgs.Cancel = true;
                        zip.Close();

                        return;
                    }

                    ZipStorer.ZipFileEntry entry = dir[index];
                    zip.ExtractFile(entry, Path.Combine(extract_path, entry.FilenameInZip));
                    _backgroundWorker.ReportProgress((index + 1) * 100 / dir.Count, string.Format(Resources.label_extracting, entry.FilenameInZip));
                }

                zip.Close();
            };

            _backgroundWorker.ProgressChanged += (o, eventArgs) =>
            {
                pb_Progress.Value = eventArgs.ProgressPercentage;
            };

            _backgroundWorker.RunWorkerCompleted += (o, eventArgs) =>
            {
                if (!eventArgs.Cancelled)
                {
                    lbl_Info.Text = @"Finished";

                    DialogResult = DialogResult.OK;

                    Application.Exit();
                }
            };

            lbl_Info.Text = Resources.status_install;
            _backgroundWorker.RunWorkerAsync();
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    }
}
