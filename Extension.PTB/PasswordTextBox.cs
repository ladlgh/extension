﻿using System;
using System.Globalization;
using System.Text;
using System.Windows.Forms;

#region Author and File Information
/*
 *  Title				        :	Custom Password Textbox with preview
 *  Author(s)					:   Srivatsa Haridas
 *  Current Version             :   v1.4
 */
#endregion

namespace Extension.PTB
{
    /// <summary>
    /// 
    /// </summary>
    public class PasswordTextBox : TextBox
    {
        public bool IsHidden = true;

        private readonly Timer timer;
        private char[] ClearPassword; 
        private readonly char DecimalSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToCharArray()[0];
        private int m_iCaretPosition = 0;
        private bool canEdit = true;
        private static int PWD_LENGTH;

        /// <summary>
        /// 
        /// </summary>
        public PasswordTextBox()
        {
            timer         = new Timer { Interval = 250 };
            timer.Tick    += timer_Tick;
            PWD_LENGTH    = this.MaxLength;
            ClearPassword = new char[PWD_LENGTH];
        }

        /// <summary>
        /// 
        /// </summary>
        public string ShowPassword
        {
            get
            {
                return new string(ClearPassword).Trim('\0').Replace("\0", "");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void HidePassword()
        {
            StringBuilder s = new StringBuilder(this.Text);

            for (int i = 0; i < s.Length; i++)
            {
                s[i] = '*';
            }

            this.Text           = s.ToString();
            this.SelectionStart = this.Text.Length - 1;
            m_iCaretPosition    = this.SelectionStart;
            IsHidden            = true;

            timer.Enabled = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnTextChanged(EventArgs e)
        {
            if (canEdit)
            {
                base.OnTextChanged(e);
                txtInput_TextChanged(this, e);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtInput_TextChanged(object sender, EventArgs e)
        {
            HidePasswordCharacters();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
            m_iCaretPosition = this.GetCharIndexFromPosition(e.Location);
        }

        /// <summary>
        /// 
        /// </summary>
        private void HidePasswordCharacters()
        {
            if (!IsHidden)
            {
                return;
            }

            int index = this.SelectionStart;

            if (index > 1)
            {
                StringBuilder s     = new StringBuilder(this.Text);
                s[index - 2]        = '*';
                this.Text           = s.ToString();
                this.SelectionStart = index;
                m_iCaretPosition    = index;
            }

            timer.Enabled = true;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            if (e.KeyCode == Keys.Delete)
            {
                canEdit = false;
                DeleteSelectedCharacters(this, e.KeyCode);
            }
        }

        /// <summary>
        /// Windows Timer elapsed eventhandler 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Tick(object sender, EventArgs e)
        {
            timer.Enabled = false;

            int index = this.SelectionStart;

            if (index >= 1)
            {
                StringBuilder s = new StringBuilder(this.Text);
                s[index - 1]    = '*';

                this.Invoke(new Action(() =>
                {
                    this.Text           = s.ToString();
                    this.SelectionStart = index;
                    m_iCaretPosition    = index;
                }));
            }
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);

            int selectionStart = this.SelectionStart,
                length         = this.TextLength,
                selectedChars  = this.SelectionLength;

            canEdit = false;

            if (selectedChars == length)
            {
                /*
                 * Means complete text selected so clear it before using it
                 */
                ClearCharBufferPlusTextBox();
            }

            Keys eModified = (Keys)e.KeyChar;

            if (e.KeyChar == DecimalSeparator)
            {
                e.Handled = true;
            }

            if (Keys.Delete != eModified
                && Keys.Back != eModified)
            {
                if (Keys.Space != eModified)
                {
                    if (e.KeyChar != '-')
                    {
                        if (!char.IsLetterOrDigit(e.KeyChar))
                        {
                            e.Handled = true;
                        }
                        else
                        {
                            if (this.TextLength < PWD_LENGTH)
                            {
                                ClearPassword = new string(ClearPassword).Insert(selectionStart, e.KeyChar.ToString()).ToCharArray();
                            }
                        }
                    }
                }
                else
                {
                    if (this.TextLength == 0)
                    {
                        e.Handled = true;
                        Array.Clear(ClearPassword, 0, ClearPassword.Length);
                    }
                }
            }
            else if (Keys.Back == eModified
                     || Keys.Delete == eModified)
            {
                DeleteSelectedCharacters(this, eModified);
            }

            // replace characters with '*'
            HidePasswordCharacters();

            canEdit = true;
        }

        /// <summary>
        /// Deletes the specific characters in the char array based on the key press action
        /// </summary>
        /// <param name="sender"></param>
        private void DeleteSelectedCharacters(object sender, Keys key)
        {
            int selectionStart = this.SelectionStart,
                length         = this.TextLength,
                selectedChars  = this.SelectionLength;

            if (selectedChars == length)
            {
                ClearCharBufferPlusTextBox();

                return;
            }

            if (selectedChars > 0)
            {
                int i = selectionStart;
                this.Text.Remove(selectionStart, selectedChars);
                ClearPassword = new string(ClearPassword).Remove(selectionStart, selectedChars).ToCharArray();
            }
            else
            {
                /*
                 * Basically this portion of code is to handle the condition 
                 * when the cursor is placed at the start or in the end 
                 */
                if (selectionStart == 0)
                {
                    /*
                    * Cursor in the beginning, before the first character 
                    * Delete the character only when Del is pressed, No action when Back key is pressed
                    */
                    if (key == Keys.Delete)
                    {
                        ClearPassword = new string(ClearPassword).Remove(selectionStart, 1).ToCharArray();
                    }
                }
                else if (selectionStart > 0 && selectionStart < length)
                {
                    /*
                    * Cursor position anywhere in between 
                    * Backspace and Delete have the same effect
                    */
                    if (key == Keys.Delete)
                    {
                        ClearPassword = new string(ClearPassword).Remove(selectionStart, 1).ToCharArray();
                    }
                    else if (key == Keys.Back)
                    {
                        ClearPassword = new string(ClearPassword).Remove(selectionStart - 1, 1).ToCharArray();
                    }
                }
                else if (selectionStart == length)
                {
                    /*
                    * Cursor at the end, after the last character 
                    * Delete the character only when Back key is pressed, No action when Delete key is pressed
                    */
                    if (key == Keys.Back)
                    {
                        ClearPassword = new string(ClearPassword).Remove(selectionStart - 1, 1).ToCharArray();
                    }
                }
            }

            this.Select((selectionStart > this.Text.Length ? this.Text.Length : selectionStart), 0);

        }

        private void ClearCharBufferPlusTextBox()
        {
            Array.Clear(ClearPassword, 0, ClearPassword.Length);
            this.Clear();
        }
    }
}