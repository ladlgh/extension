﻿namespace Extension.TEST
{
    partial class frm_MAIN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tlp_TEST = new System.Windows.Forms.TableLayoutPanel();
            this.p_Filter = new System.Windows.Forms.Panel();
            this.btn_Refresh = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chb_Group = new System.Windows.Forms.CheckBox();
            this.p_Filter_Split1 = new System.Windows.Forms.Panel();
            this.btn_Filter_Clear = new System.Windows.Forms.Button();
            this.lbl_Filter = new System.Windows.Forms.Label();
            this.tb_Filter = new System.Windows.Forms.TextBox();
            this.edgv_DGV_BGWM = new Extension.EDGV.ExtendedDataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bOMFINDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sOLDERINGDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pCBNUMBERDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tIMEINTENSIVEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fINISHEDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oP2DELIVERYIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cEOSNRDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gLOBALPSDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mCUDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pCUDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vECUPNDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dELIVERYSTATUSDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_APP = new Extension.TEST.ds_APP();
            this.edgv_DGV_STP = new Extension.EDGV.ExtendedDataGridView();
            this.iDORDERDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDEMBADataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aMOUNTDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDSTARTUPGROUPDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDPRIORITYDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDLOCATIONDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pACKSTATEDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cREATIONDATEDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bOXESDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cREATORDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lASTSCANDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sAMPCOMMENTDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pICOMMENTDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pRODUCTTYPEDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDTESTSTATUSDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDSTORAGEDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tc_TEST = new System.Windows.Forms.TabControl();
            this.tb_DataGridView = new System.Windows.Forms.TabPage();
            this.tb_BarcodeGenerator = new System.Windows.Forms.TabPage();
            this.tlp_Main = new System.Windows.Forms.TableLayoutPanel();
            this.btn_Generate = new System.Windows.Forms.Button();
            this.tb_Text = new System.Windows.Forms.TextBox();
            this.pb_BarCode = new System.Windows.Forms.PictureBox();
            this.cob_BarCodeType = new System.Windows.Forms.ComboBox();
            this.nud_BarCodeSize = new System.Windows.Forms.NumericUpDown();
            this.tb_PortableSettingsProvider = new System.Windows.Forms.TabPage();
            this.tlp_PortableSettings = new System.Windows.Forms.TableLayoutPanel();
            this.p_Settings_User = new System.Windows.Forms.Panel();
            this.lbl_Setting1 = new System.Windows.Forms.Label();
            this.lbl_Setting2 = new System.Windows.Forms.Label();
            this.tb_Setting1 = new System.Windows.Forms.TextBox();
            this.tb_Setting2 = new System.Windows.Forms.TextBox();
            this.btn_Settings_SaveUser = new System.Windows.Forms.Button();
            this.p_Settings_Updater = new System.Windows.Forms.Panel();
            this.lbl_Setting10 = new System.Windows.Forms.Label();
            this.btn_Settings_SaveUpdater = new System.Windows.Forms.Button();
            this.lbl_Setting20 = new System.Windows.Forms.Label();
            this.tb_Setting20 = new System.Windows.Forms.TextBox();
            this.tb_Setting10 = new System.Windows.Forms.TextBox();
            this.lbl_Setting_User = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tp_AppVersionManager = new System.Windows.Forms.TabPage();
            this.btn_Changelog = new System.Windows.Forms.Button();
            this.btn_Update_Normal = new System.Windows.Forms.Button();
            this.btn_Update_Silence = new System.Windows.Forms.Button();
            this.tp_ServiceDesk = new System.Windows.Forms.TabPage();
            this.wb_ServiceDesk = new System.Windows.Forms.WebBrowser();
            this.tp_PasswordTextBox = new System.Windows.Forms.TabPage();
            this.btn_SeePassword = new System.Windows.Forms.Button();
            this.ptb_Password = new Extension.PTB.PasswordTextBox();
            this.tlp_TEST.SuspendLayout();
            this.p_Filter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edgv_DGV_BGWM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ds_APP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edgv_DGV_STP)).BeginInit();
            this.tc_TEST.SuspendLayout();
            this.tb_DataGridView.SuspendLayout();
            this.tb_BarcodeGenerator.SuspendLayout();
            this.tlp_Main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_BarCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_BarCodeSize)).BeginInit();
            this.tb_PortableSettingsProvider.SuspendLayout();
            this.tlp_PortableSettings.SuspendLayout();
            this.p_Settings_User.SuspendLayout();
            this.p_Settings_Updater.SuspendLayout();
            this.tp_AppVersionManager.SuspendLayout();
            this.tp_ServiceDesk.SuspendLayout();
            this.tp_PasswordTextBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlp_TEST
            // 
            this.tlp_TEST.ColumnCount = 1;
            this.tlp_TEST.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_TEST.Controls.Add(this.p_Filter, 0, 0);
            this.tlp_TEST.Controls.Add(this.edgv_DGV_BGWM, 0, 1);
            this.tlp_TEST.Controls.Add(this.edgv_DGV_STP, 0, 3);
            this.tlp_TEST.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp_TEST.Location = new System.Drawing.Point(0, 0);
            this.tlp_TEST.Margin = new System.Windows.Forms.Padding(0);
            this.tlp_TEST.Name = "tlp_TEST";
            this.tlp_TEST.RowCount = 4;
            this.tlp_TEST.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tlp_TEST.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp_TEST.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tlp_TEST.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp_TEST.Size = new System.Drawing.Size(1006, 582);
            this.tlp_TEST.TabIndex = 2;
            // 
            // p_Filter
            // 
            this.p_Filter.Controls.Add(this.btn_Refresh);
            this.p_Filter.Controls.Add(this.panel1);
            this.p_Filter.Controls.Add(this.chb_Group);
            this.p_Filter.Controls.Add(this.p_Filter_Split1);
            this.p_Filter.Controls.Add(this.btn_Filter_Clear);
            this.p_Filter.Controls.Add(this.lbl_Filter);
            this.p_Filter.Controls.Add(this.tb_Filter);
            this.p_Filter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.p_Filter.Location = new System.Drawing.Point(0, 0);
            this.p_Filter.Margin = new System.Windows.Forms.Padding(0);
            this.p_Filter.Name = "p_Filter";
            this.p_Filter.Size = new System.Drawing.Size(1006, 26);
            this.p_Filter.TabIndex = 2;
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.BackColor = System.Drawing.Color.LightBlue;
            this.btn_Refresh.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_Refresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Refresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Refresh.Location = new System.Drawing.Point(340, 2);
            this.btn_Refresh.Margin = new System.Windows.Forms.Padding(0);
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.Size = new System.Drawing.Size(70, 21);
            this.btn_Refresh.TabIndex = 22;
            this.btn_Refresh.Text = "REFRESH";
            this.btn_Refresh.UseVisualStyleBackColor = false;
            this.btn_Refresh.Click += new System.EventHandler(this.btn_Refresh_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkGray;
            this.panel1.Location = new System.Drawing.Point(334, 3);
            this.panel1.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1, 21);
            this.panel1.TabIndex = 21;
            // 
            // chb_Group
            // 
            this.chb_Group.AutoSize = true;
            this.chb_Group.Checked = true;
            this.chb_Group.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chb_Group.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chb_Group.Location = new System.Drawing.Point(277, 4);
            this.chb_Group.Name = "chb_Group";
            this.chb_Group.Size = new System.Drawing.Size(52, 17);
            this.chb_Group.TabIndex = 20;
            this.chb_Group.Text = "Group";
            this.chb_Group.UseVisualStyleBackColor = true;
            this.chb_Group.CheckedChanged += new System.EventHandler(this.chb_Group_CheckedChanged);
            // 
            // p_Filter_Split1
            // 
            this.p_Filter_Split1.BackColor = System.Drawing.Color.DarkGray;
            this.p_Filter_Split1.Location = new System.Drawing.Point(268, 2);
            this.p_Filter_Split1.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.p_Filter_Split1.Name = "p_Filter_Split1";
            this.p_Filter_Split1.Size = new System.Drawing.Size(1, 21);
            this.p_Filter_Split1.TabIndex = 0;
            // 
            // btn_Filter_Clear
            // 
            this.btn_Filter_Clear.BackColor = System.Drawing.Color.DarkSalmon;
            this.btn_Filter_Clear.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_Filter_Clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Filter_Clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Filter_Clear.Location = new System.Drawing.Point(209, 2);
            this.btn_Filter_Clear.Margin = new System.Windows.Forms.Padding(0);
            this.btn_Filter_Clear.Name = "btn_Filter_Clear";
            this.btn_Filter_Clear.Size = new System.Drawing.Size(54, 21);
            this.btn_Filter_Clear.TabIndex = 15;
            this.btn_Filter_Clear.Text = "CLEAR";
            this.btn_Filter_Clear.UseVisualStyleBackColor = false;
            this.btn_Filter_Clear.Click += new System.EventHandler(this.btn_Filter_Clear_Click);
            // 
            // lbl_Filter
            // 
            this.lbl_Filter.AutoSize = true;
            this.lbl_Filter.Location = new System.Drawing.Point(4, 6);
            this.lbl_Filter.Name = "lbl_Filter";
            this.lbl_Filter.Size = new System.Drawing.Size(88, 13);
            this.lbl_Filter.TabIndex = 11;
            this.lbl_Filter.Text = "Filter by ORDER:";
            // 
            // tb_Filter
            // 
            this.tb_Filter.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_Filter.Location = new System.Drawing.Point(94, 2);
            this.tb_Filter.Name = "tb_Filter";
            this.tb_Filter.Size = new System.Drawing.Size(110, 20);
            this.tb_Filter.TabIndex = 10;
            this.tb_Filter.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tb_Filter_KeyUp);
            // 
            // edgv_DGV_BGWM
            // 
            this.edgv_DGV_BGWM.AllowUserToAddRows = false;
            this.edgv_DGV_BGWM.AllowUserToDeleteRows = false;
            this.edgv_DGV_BGWM.AllowUserToOrderColumns = true;
            this.edgv_DGV_BGWM.AllowUserToResizeRows = false;
            this.edgv_DGV_BGWM.AutoGenerateColumns = false;
            this.edgv_DGV_BGWM.AutoGenerateContextFilters = true;
            this.edgv_DGV_BGWM.BackgroundColor = System.Drawing.Color.DarkGray;
            this.edgv_DGV_BGWM.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edgv_DGV_BGWM.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.edgv_DGV_BGWM.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.edgv_DGV_BGWM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.edgv_DGV_BGWM.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.bOMFINDataGridViewTextBoxColumn,
            this.sOLDERINGDataGridViewTextBoxColumn,
            this.pCBNUMBERDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.tIMEINTENSIVEDataGridViewTextBoxColumn,
            this.fINISHEDDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36,
            this.oP2DELIVERYIDDataGridViewTextBoxColumn,
            this.cEOSNRDataGridViewTextBoxColumn,
            this.gLOBALPSDataGridViewTextBoxColumn,
            this.mCUDataGridViewTextBoxColumn,
            this.pCUDataGridViewTextBoxColumn,
            this.vECUPNDataGridViewTextBoxColumn,
            this.dELIVERYSTATUSDataGridViewTextBoxColumn});
            this.edgv_DGV_BGWM.DataMember = "TV_ORDER";
            this.edgv_DGV_BGWM.DataSource = this.ds_APP;
            this.edgv_DGV_BGWM.DateWithTime = false;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.edgv_DGV_BGWM.DefaultCellStyle = dataGridViewCellStyle10;
            this.edgv_DGV_BGWM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.edgv_DGV_BGWM.EnableHeadersVisualStyles = false;
            this.edgv_DGV_BGWM.GridColor = System.Drawing.Color.Silver;
            this.edgv_DGV_BGWM.Location = new System.Drawing.Point(0, 26);
            this.edgv_DGV_BGWM.Margin = new System.Windows.Forms.Padding(0);
            this.edgv_DGV_BGWM.MultiSelect = false;
            this.edgv_DGV_BGWM.Name = "edgv_DGV_BGWM";
            this.edgv_DGV_BGWM.ReadOnly = true;
            this.edgv_DGV_BGWM.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.edgv_DGV_BGWM.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.edgv_DGV_BGWM.RowHeadersVisible = false;
            this.edgv_DGV_BGWM.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            this.edgv_DGV_BGWM.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.edgv_DGV_BGWM.RowTemplate.Height = 25;
            this.edgv_DGV_BGWM.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.edgv_DGV_BGWM.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.edgv_DGV_BGWM.Size = new System.Drawing.Size(1006, 275);
            this.edgv_DGV_BGWM.TabIndex = 3;
            this.edgv_DGV_BGWM.TimeFilter = false;
            this.edgv_DGV_BGWM.SortStringChanged += new System.EventHandler(this.edgv_DGV_SortStringChanged);
            this.edgv_DGV_BGWM.FilterStringChanged += new System.EventHandler(this.edgv_DGV_FilterStringChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "TIME_TO_FINISH";
            this.dataGridViewTextBoxColumn1.HeaderText = "TIME_TO_FINISH";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "TIMESPAN_TO_FINISH";
            this.dataGridViewTextBoxColumn2.HeaderText = "TIMESPAN_TO_FINISH";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "LOCATION_FINISH_DATE";
            this.dataGridViewTextBoxColumn3.HeaderText = "LOCATION_FINISH_DATE";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ID_ORDER";
            this.dataGridViewTextBoxColumn4.HeaderText = "ID_ORDER";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "ID_EMBA";
            this.dataGridViewTextBoxColumn5.HeaderText = "ID_EMBA";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "PROJECT_NAME";
            this.dataGridViewTextBoxColumn6.HeaderText = "PROJECT_NAME";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // bOMFINDataGridViewTextBoxColumn
            // 
            this.bOMFINDataGridViewTextBoxColumn.DataPropertyName = "BOM_FIN";
            this.bOMFINDataGridViewTextBoxColumn.HeaderText = "BOM_FIN";
            this.bOMFINDataGridViewTextBoxColumn.MinimumWidth = 18;
            this.bOMFINDataGridViewTextBoxColumn.Name = "bOMFINDataGridViewTextBoxColumn";
            this.bOMFINDataGridViewTextBoxColumn.ReadOnly = true;
            this.bOMFINDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // sOLDERINGDataGridViewTextBoxColumn
            // 
            this.sOLDERINGDataGridViewTextBoxColumn.DataPropertyName = "SOLDERING";
            this.sOLDERINGDataGridViewTextBoxColumn.HeaderText = "SOLDERING";
            this.sOLDERINGDataGridViewTextBoxColumn.MinimumWidth = 18;
            this.sOLDERINGDataGridViewTextBoxColumn.Name = "sOLDERINGDataGridViewTextBoxColumn";
            this.sOLDERINGDataGridViewTextBoxColumn.ReadOnly = true;
            this.sOLDERINGDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // pCBNUMBERDataGridViewTextBoxColumn
            // 
            this.pCBNUMBERDataGridViewTextBoxColumn.DataPropertyName = "PCB_NUMBER";
            this.pCBNUMBERDataGridViewTextBoxColumn.HeaderText = "PCB_NUMBER";
            this.pCBNUMBERDataGridViewTextBoxColumn.MinimumWidth = 18;
            this.pCBNUMBERDataGridViewTextBoxColumn.Name = "pCBNUMBERDataGridViewTextBoxColumn";
            this.pCBNUMBERDataGridViewTextBoxColumn.ReadOnly = true;
            this.pCBNUMBERDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "AMOUNT";
            this.dataGridViewTextBoxColumn7.HeaderText = "AMOUNT";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "BOOKED";
            this.dataGridViewTextBoxColumn8.HeaderText = "BOOKED";
            this.dataGridViewTextBoxColumn8.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "ID_STARTUP_GROUP";
            this.dataGridViewTextBoxColumn9.HeaderText = "ID_STARTUP_GROUP";
            this.dataGridViewTextBoxColumn9.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "STARTUP_GROUP_NAME";
            this.dataGridViewTextBoxColumn10.HeaderText = "STARTUP_GROUP_NAME";
            this.dataGridViewTextBoxColumn10.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "ID_PRIORITY";
            this.dataGridViewTextBoxColumn11.HeaderText = "ID_PRIORITY";
            this.dataGridViewTextBoxColumn11.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "PRIORITY_NAME";
            this.dataGridViewTextBoxColumn12.HeaderText = "PRIORITY_NAME";
            this.dataGridViewTextBoxColumn12.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "ID_LOCATION";
            this.dataGridViewTextBoxColumn13.HeaderText = "ID_LOCATION";
            this.dataGridViewTextBoxColumn13.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "LOCATION_NAME";
            this.dataGridViewTextBoxColumn14.HeaderText = "LOCATION_NAME";
            this.dataGridViewTextBoxColumn14.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "LOCATION_PROCESSQUOTA";
            this.dataGridViewTextBoxColumn15.HeaderText = "LOCATION_PROCESSQUOTA";
            this.dataGridViewTextBoxColumn15.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // tIMEINTENSIVEDataGridViewTextBoxColumn
            // 
            this.tIMEINTENSIVEDataGridViewTextBoxColumn.DataPropertyName = "TIME_INTENSIVE";
            this.tIMEINTENSIVEDataGridViewTextBoxColumn.HeaderText = "TIME_INTENSIVE";
            this.tIMEINTENSIVEDataGridViewTextBoxColumn.MinimumWidth = 18;
            this.tIMEINTENSIVEDataGridViewTextBoxColumn.Name = "tIMEINTENSIVEDataGridViewTextBoxColumn";
            this.tIMEINTENSIVEDataGridViewTextBoxColumn.ReadOnly = true;
            this.tIMEINTENSIVEDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // fINISHEDDataGridViewTextBoxColumn
            // 
            this.fINISHEDDataGridViewTextBoxColumn.DataPropertyName = "FINISHED";
            this.fINISHEDDataGridViewTextBoxColumn.HeaderText = "FINISHED";
            this.fINISHEDDataGridViewTextBoxColumn.MinimumWidth = 18;
            this.fINISHEDDataGridViewTextBoxColumn.Name = "fINISHEDDataGridViewTextBoxColumn";
            this.fINISHEDDataGridViewTextBoxColumn.ReadOnly = true;
            this.fINISHEDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "ESTIMATED_DATE";
            this.dataGridViewTextBoxColumn16.HeaderText = "ESTIMATED_DATE";
            this.dataGridViewTextBoxColumn16.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "PACKSTATE";
            this.dataGridViewTextBoxColumn17.HeaderText = "PACKSTATE";
            this.dataGridViewTextBoxColumn17.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "CREATION_DATE";
            this.dataGridViewTextBoxColumn18.HeaderText = "CREATION_DATE";
            this.dataGridViewTextBoxColumn18.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "BOXES";
            this.dataGridViewTextBoxColumn19.HeaderText = "BOXES";
            this.dataGridViewTextBoxColumn19.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "CREATOR";
            this.dataGridViewTextBoxColumn20.HeaderText = "CREATOR";
            this.dataGridViewTextBoxColumn20.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "LAST_SCAN";
            this.dataGridViewTextBoxColumn21.HeaderText = "LAST_SCAN";
            this.dataGridViewTextBoxColumn21.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "SAMP_COMMENT";
            this.dataGridViewTextBoxColumn22.HeaderText = "SAMP_COMMENT";
            this.dataGridViewTextBoxColumn22.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "PI_COMMENT";
            this.dataGridViewTextBoxColumn23.HeaderText = "PI_COMMENT";
            this.dataGridViewTextBoxColumn23.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "ID_SAMPLE_PRODUCTION_STATE";
            this.dataGridViewTextBoxColumn24.HeaderText = "ID_SAMPLE_PRODUCTION_STATE";
            this.dataGridViewTextBoxColumn24.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "SAMPLE_PRODUCTION_STATE_NAME";
            this.dataGridViewTextBoxColumn25.HeaderText = "SAMPLE_PRODUCTION_STATE_NAME";
            this.dataGridViewTextBoxColumn25.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.DataPropertyName = "PRODUCT_TYPE";
            this.dataGridViewTextBoxColumn26.HeaderText = "PRODUCT_TYPE";
            this.dataGridViewTextBoxColumn26.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.DataPropertyName = "VARIANT_TYPE";
            this.dataGridViewTextBoxColumn27.HeaderText = "VARIANT_TYPE";
            this.dataGridViewTextBoxColumn27.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.DataPropertyName = "SAP_DELIVERY_NUMBER";
            this.dataGridViewTextBoxColumn28.HeaderText = "SAP_DELIVERY_NUMBER";
            this.dataGridViewTextBoxColumn28.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            this.dataGridViewTextBoxColumn28.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.DataPropertyName = "TARGET_DATE";
            this.dataGridViewTextBoxColumn29.HeaderText = "TARGET_DATE";
            this.dataGridViewTextBoxColumn29.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            this.dataGridViewTextBoxColumn29.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.DataPropertyName = "ID_ORDERSTATUS";
            this.dataGridViewTextBoxColumn30.HeaderText = "ID_ORDERSTATUS";
            this.dataGridViewTextBoxColumn30.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            this.dataGridViewTextBoxColumn30.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.DataPropertyName = "ORDERSTATUS_NAME";
            this.dataGridViewTextBoxColumn31.HeaderText = "ORDERSTATUS_NAME";
            this.dataGridViewTextBoxColumn31.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            this.dataGridViewTextBoxColumn31.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.DataPropertyName = "ID_TESTSTATUS";
            this.dataGridViewTextBoxColumn32.HeaderText = "ID_TESTSTATUS";
            this.dataGridViewTextBoxColumn32.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.ReadOnly = true;
            this.dataGridViewTextBoxColumn32.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.DataPropertyName = "TESTSTATUS_NAME";
            this.dataGridViewTextBoxColumn33.HeaderText = "TESTSTATUS_NAME";
            this.dataGridViewTextBoxColumn33.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.ReadOnly = true;
            this.dataGridViewTextBoxColumn33.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.DataPropertyName = "SKZ_NUMBER";
            this.dataGridViewTextBoxColumn34.HeaderText = "SKZ_NUMBER";
            this.dataGridViewTextBoxColumn34.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.ReadOnly = true;
            this.dataGridViewTextBoxColumn34.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.DataPropertyName = "GLOBAL_PS_NUMBER";
            this.dataGridViewTextBoxColumn35.HeaderText = "GLOBAL_PS_NUMBER";
            this.dataGridViewTextBoxColumn35.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.ReadOnly = true;
            this.dataGridViewTextBoxColumn35.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dataGridViewTextBoxColumn36
            // 
            this.dataGridViewTextBoxColumn36.DataPropertyName = "ID_STORAGE";
            this.dataGridViewTextBoxColumn36.HeaderText = "ID_STORAGE";
            this.dataGridViewTextBoxColumn36.MinimumWidth = 18;
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.ReadOnly = true;
            this.dataGridViewTextBoxColumn36.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // oP2DELIVERYIDDataGridViewTextBoxColumn
            // 
            this.oP2DELIVERYIDDataGridViewTextBoxColumn.DataPropertyName = "OP2DELIVERYID";
            this.oP2DELIVERYIDDataGridViewTextBoxColumn.HeaderText = "OP2DELIVERYID";
            this.oP2DELIVERYIDDataGridViewTextBoxColumn.MinimumWidth = 18;
            this.oP2DELIVERYIDDataGridViewTextBoxColumn.Name = "oP2DELIVERYIDDataGridViewTextBoxColumn";
            this.oP2DELIVERYIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.oP2DELIVERYIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // cEOSNRDataGridViewTextBoxColumn
            // 
            this.cEOSNRDataGridViewTextBoxColumn.DataPropertyName = "CEOS_NR";
            this.cEOSNRDataGridViewTextBoxColumn.HeaderText = "CEOS_NR";
            this.cEOSNRDataGridViewTextBoxColumn.MinimumWidth = 18;
            this.cEOSNRDataGridViewTextBoxColumn.Name = "cEOSNRDataGridViewTextBoxColumn";
            this.cEOSNRDataGridViewTextBoxColumn.ReadOnly = true;
            this.cEOSNRDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // gLOBALPSDataGridViewTextBoxColumn
            // 
            this.gLOBALPSDataGridViewTextBoxColumn.DataPropertyName = "GLOBALPS";
            this.gLOBALPSDataGridViewTextBoxColumn.HeaderText = "GLOBALPS";
            this.gLOBALPSDataGridViewTextBoxColumn.MinimumWidth = 18;
            this.gLOBALPSDataGridViewTextBoxColumn.Name = "gLOBALPSDataGridViewTextBoxColumn";
            this.gLOBALPSDataGridViewTextBoxColumn.ReadOnly = true;
            this.gLOBALPSDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // mCUDataGridViewTextBoxColumn
            // 
            this.mCUDataGridViewTextBoxColumn.DataPropertyName = "MCU";
            this.mCUDataGridViewTextBoxColumn.HeaderText = "MCU";
            this.mCUDataGridViewTextBoxColumn.MinimumWidth = 18;
            this.mCUDataGridViewTextBoxColumn.Name = "mCUDataGridViewTextBoxColumn";
            this.mCUDataGridViewTextBoxColumn.ReadOnly = true;
            this.mCUDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // pCUDataGridViewTextBoxColumn
            // 
            this.pCUDataGridViewTextBoxColumn.DataPropertyName = "PCU";
            this.pCUDataGridViewTextBoxColumn.HeaderText = "PCU";
            this.pCUDataGridViewTextBoxColumn.MinimumWidth = 18;
            this.pCUDataGridViewTextBoxColumn.Name = "pCUDataGridViewTextBoxColumn";
            this.pCUDataGridViewTextBoxColumn.ReadOnly = true;
            this.pCUDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // vECUPNDataGridViewTextBoxColumn
            // 
            this.vECUPNDataGridViewTextBoxColumn.DataPropertyName = "VECUPN";
            this.vECUPNDataGridViewTextBoxColumn.HeaderText = "VECUPN";
            this.vECUPNDataGridViewTextBoxColumn.MinimumWidth = 18;
            this.vECUPNDataGridViewTextBoxColumn.Name = "vECUPNDataGridViewTextBoxColumn";
            this.vECUPNDataGridViewTextBoxColumn.ReadOnly = true;
            this.vECUPNDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // dELIVERYSTATUSDataGridViewTextBoxColumn
            // 
            this.dELIVERYSTATUSDataGridViewTextBoxColumn.DataPropertyName = "DELIVERY_STATUS";
            this.dELIVERYSTATUSDataGridViewTextBoxColumn.HeaderText = "DELIVERY_STATUS";
            this.dELIVERYSTATUSDataGridViewTextBoxColumn.MinimumWidth = 18;
            this.dELIVERYSTATUSDataGridViewTextBoxColumn.Name = "dELIVERYSTATUSDataGridViewTextBoxColumn";
            this.dELIVERYSTATUSDataGridViewTextBoxColumn.ReadOnly = true;
            this.dELIVERYSTATUSDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // ds_APP
            // 
            this.ds_APP.DataSetName = "ds_APP";
            this.ds_APP.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // edgv_DGV_STP
            // 
            this.edgv_DGV_STP.AllowUserToAddRows = false;
            this.edgv_DGV_STP.AllowUserToDeleteRows = false;
            this.edgv_DGV_STP.AllowUserToOrderColumns = true;
            this.edgv_DGV_STP.AllowUserToResizeRows = false;
            this.edgv_DGV_STP.AutoGenerateColumns = false;
            this.edgv_DGV_STP.AutoGenerateContextFilters = true;
            this.edgv_DGV_STP.BackgroundColor = System.Drawing.Color.DarkGray;
            this.edgv_DGV_STP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edgv_DGV_STP.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.edgv_DGV_STP.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.edgv_DGV_STP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.edgv_DGV_STP.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDORDERDataGridViewTextBoxColumn1,
            this.iDEMBADataGridViewTextBoxColumn1,
            this.aMOUNTDataGridViewTextBoxColumn1,
            this.iDSTARTUPGROUPDataGridViewTextBoxColumn1,
            this.iDPRIORITYDataGridViewTextBoxColumn1,
            this.iDLOCATIONDataGridViewTextBoxColumn1,
            this.pACKSTATEDataGridViewTextBoxColumn1,
            this.cREATIONDATEDataGridViewTextBoxColumn1,
            this.bOXESDataGridViewTextBoxColumn1,
            this.cREATORDataGridViewTextBoxColumn1,
            this.lASTSCANDataGridViewTextBoxColumn1,
            this.sAMPCOMMENTDataGridViewTextBoxColumn1,
            this.pICOMMENTDataGridViewTextBoxColumn1,
            this.pRODUCTTYPEDataGridViewTextBoxColumn1,
            this.iDTESTSTATUSDataGridViewTextBoxColumn1,
            this.iDSTORAGEDataGridViewTextBoxColumn1});
            this.edgv_DGV_STP.DataMember = "ORDER";
            this.edgv_DGV_STP.DataSource = this.ds_APP;
            this.edgv_DGV_STP.DateWithTime = false;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.edgv_DGV_STP.DefaultCellStyle = dataGridViewCellStyle14;
            this.edgv_DGV_STP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.edgv_DGV_STP.EnableHeadersVisualStyles = false;
            this.edgv_DGV_STP.GridColor = System.Drawing.Color.Silver;
            this.edgv_DGV_STP.Location = new System.Drawing.Point(0, 306);
            this.edgv_DGV_STP.Margin = new System.Windows.Forms.Padding(0);
            this.edgv_DGV_STP.MultiSelect = false;
            this.edgv_DGV_STP.Name = "edgv_DGV_STP";
            this.edgv_DGV_STP.ReadOnly = true;
            this.edgv_DGV_STP.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.edgv_DGV_STP.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.edgv_DGV_STP.RowHeadersVisible = false;
            this.edgv_DGV_STP.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black;
            this.edgv_DGV_STP.RowsDefaultCellStyle = dataGridViewCellStyle16;
            this.edgv_DGV_STP.RowTemplate.Height = 25;
            this.edgv_DGV_STP.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.edgv_DGV_STP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.edgv_DGV_STP.Size = new System.Drawing.Size(1006, 276);
            this.edgv_DGV_STP.TabIndex = 3;
            this.edgv_DGV_STP.TimeFilter = false;
            this.edgv_DGV_STP.SortStringChanged += new System.EventHandler(this.edgv_DGV_SortStringChanged);
            this.edgv_DGV_STP.FilterStringChanged += new System.EventHandler(this.edgv_DGV_FilterStringChanged);
            // 
            // iDORDERDataGridViewTextBoxColumn1
            // 
            this.iDORDERDataGridViewTextBoxColumn1.DataPropertyName = "ID_ORDER";
            this.iDORDERDataGridViewTextBoxColumn1.HeaderText = "ID_ORDER";
            this.iDORDERDataGridViewTextBoxColumn1.MinimumWidth = 18;
            this.iDORDERDataGridViewTextBoxColumn1.Name = "iDORDERDataGridViewTextBoxColumn1";
            this.iDORDERDataGridViewTextBoxColumn1.ReadOnly = true;
            this.iDORDERDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // iDEMBADataGridViewTextBoxColumn1
            // 
            this.iDEMBADataGridViewTextBoxColumn1.DataPropertyName = "ID_EMBA";
            this.iDEMBADataGridViewTextBoxColumn1.HeaderText = "ID_EMBA";
            this.iDEMBADataGridViewTextBoxColumn1.MinimumWidth = 18;
            this.iDEMBADataGridViewTextBoxColumn1.Name = "iDEMBADataGridViewTextBoxColumn1";
            this.iDEMBADataGridViewTextBoxColumn1.ReadOnly = true;
            this.iDEMBADataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // aMOUNTDataGridViewTextBoxColumn1
            // 
            this.aMOUNTDataGridViewTextBoxColumn1.DataPropertyName = "AMOUNT";
            this.aMOUNTDataGridViewTextBoxColumn1.HeaderText = "AMOUNT";
            this.aMOUNTDataGridViewTextBoxColumn1.MinimumWidth = 18;
            this.aMOUNTDataGridViewTextBoxColumn1.Name = "aMOUNTDataGridViewTextBoxColumn1";
            this.aMOUNTDataGridViewTextBoxColumn1.ReadOnly = true;
            this.aMOUNTDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // iDSTARTUPGROUPDataGridViewTextBoxColumn1
            // 
            this.iDSTARTUPGROUPDataGridViewTextBoxColumn1.DataPropertyName = "ID_STARTUP_GROUP";
            this.iDSTARTUPGROUPDataGridViewTextBoxColumn1.HeaderText = "ID_STARTUP_GROUP";
            this.iDSTARTUPGROUPDataGridViewTextBoxColumn1.MinimumWidth = 18;
            this.iDSTARTUPGROUPDataGridViewTextBoxColumn1.Name = "iDSTARTUPGROUPDataGridViewTextBoxColumn1";
            this.iDSTARTUPGROUPDataGridViewTextBoxColumn1.ReadOnly = true;
            this.iDSTARTUPGROUPDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // iDPRIORITYDataGridViewTextBoxColumn1
            // 
            this.iDPRIORITYDataGridViewTextBoxColumn1.DataPropertyName = "ID_PRIORITY";
            this.iDPRIORITYDataGridViewTextBoxColumn1.HeaderText = "ID_PRIORITY";
            this.iDPRIORITYDataGridViewTextBoxColumn1.MinimumWidth = 18;
            this.iDPRIORITYDataGridViewTextBoxColumn1.Name = "iDPRIORITYDataGridViewTextBoxColumn1";
            this.iDPRIORITYDataGridViewTextBoxColumn1.ReadOnly = true;
            this.iDPRIORITYDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // iDLOCATIONDataGridViewTextBoxColumn1
            // 
            this.iDLOCATIONDataGridViewTextBoxColumn1.DataPropertyName = "ID_LOCATION";
            this.iDLOCATIONDataGridViewTextBoxColumn1.HeaderText = "ID_LOCATION";
            this.iDLOCATIONDataGridViewTextBoxColumn1.MinimumWidth = 18;
            this.iDLOCATIONDataGridViewTextBoxColumn1.Name = "iDLOCATIONDataGridViewTextBoxColumn1";
            this.iDLOCATIONDataGridViewTextBoxColumn1.ReadOnly = true;
            this.iDLOCATIONDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // pACKSTATEDataGridViewTextBoxColumn1
            // 
            this.pACKSTATEDataGridViewTextBoxColumn1.DataPropertyName = "PACKSTATE";
            this.pACKSTATEDataGridViewTextBoxColumn1.HeaderText = "PACKSTATE";
            this.pACKSTATEDataGridViewTextBoxColumn1.MinimumWidth = 18;
            this.pACKSTATEDataGridViewTextBoxColumn1.Name = "pACKSTATEDataGridViewTextBoxColumn1";
            this.pACKSTATEDataGridViewTextBoxColumn1.ReadOnly = true;
            this.pACKSTATEDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // cREATIONDATEDataGridViewTextBoxColumn1
            // 
            this.cREATIONDATEDataGridViewTextBoxColumn1.DataPropertyName = "CREATION_DATE";
            this.cREATIONDATEDataGridViewTextBoxColumn1.HeaderText = "CREATION_DATE";
            this.cREATIONDATEDataGridViewTextBoxColumn1.MinimumWidth = 18;
            this.cREATIONDATEDataGridViewTextBoxColumn1.Name = "cREATIONDATEDataGridViewTextBoxColumn1";
            this.cREATIONDATEDataGridViewTextBoxColumn1.ReadOnly = true;
            this.cREATIONDATEDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // bOXESDataGridViewTextBoxColumn1
            // 
            this.bOXESDataGridViewTextBoxColumn1.DataPropertyName = "BOXES";
            this.bOXESDataGridViewTextBoxColumn1.HeaderText = "BOXES";
            this.bOXESDataGridViewTextBoxColumn1.MinimumWidth = 18;
            this.bOXESDataGridViewTextBoxColumn1.Name = "bOXESDataGridViewTextBoxColumn1";
            this.bOXESDataGridViewTextBoxColumn1.ReadOnly = true;
            this.bOXESDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // cREATORDataGridViewTextBoxColumn1
            // 
            this.cREATORDataGridViewTextBoxColumn1.DataPropertyName = "CREATOR";
            this.cREATORDataGridViewTextBoxColumn1.HeaderText = "CREATOR";
            this.cREATORDataGridViewTextBoxColumn1.MinimumWidth = 18;
            this.cREATORDataGridViewTextBoxColumn1.Name = "cREATORDataGridViewTextBoxColumn1";
            this.cREATORDataGridViewTextBoxColumn1.ReadOnly = true;
            this.cREATORDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // lASTSCANDataGridViewTextBoxColumn1
            // 
            this.lASTSCANDataGridViewTextBoxColumn1.DataPropertyName = "LAST_SCAN";
            this.lASTSCANDataGridViewTextBoxColumn1.HeaderText = "LAST_SCAN";
            this.lASTSCANDataGridViewTextBoxColumn1.MinimumWidth = 18;
            this.lASTSCANDataGridViewTextBoxColumn1.Name = "lASTSCANDataGridViewTextBoxColumn1";
            this.lASTSCANDataGridViewTextBoxColumn1.ReadOnly = true;
            this.lASTSCANDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // sAMPCOMMENTDataGridViewTextBoxColumn1
            // 
            this.sAMPCOMMENTDataGridViewTextBoxColumn1.DataPropertyName = "SAMP_COMMENT";
            this.sAMPCOMMENTDataGridViewTextBoxColumn1.HeaderText = "SAMP_COMMENT";
            this.sAMPCOMMENTDataGridViewTextBoxColumn1.MinimumWidth = 18;
            this.sAMPCOMMENTDataGridViewTextBoxColumn1.Name = "sAMPCOMMENTDataGridViewTextBoxColumn1";
            this.sAMPCOMMENTDataGridViewTextBoxColumn1.ReadOnly = true;
            this.sAMPCOMMENTDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // pICOMMENTDataGridViewTextBoxColumn1
            // 
            this.pICOMMENTDataGridViewTextBoxColumn1.DataPropertyName = "PI_COMMENT";
            this.pICOMMENTDataGridViewTextBoxColumn1.HeaderText = "PI_COMMENT";
            this.pICOMMENTDataGridViewTextBoxColumn1.MinimumWidth = 18;
            this.pICOMMENTDataGridViewTextBoxColumn1.Name = "pICOMMENTDataGridViewTextBoxColumn1";
            this.pICOMMENTDataGridViewTextBoxColumn1.ReadOnly = true;
            this.pICOMMENTDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // pRODUCTTYPEDataGridViewTextBoxColumn1
            // 
            this.pRODUCTTYPEDataGridViewTextBoxColumn1.DataPropertyName = "PRODUCT_TYPE";
            this.pRODUCTTYPEDataGridViewTextBoxColumn1.HeaderText = "PRODUCT_TYPE";
            this.pRODUCTTYPEDataGridViewTextBoxColumn1.MinimumWidth = 18;
            this.pRODUCTTYPEDataGridViewTextBoxColumn1.Name = "pRODUCTTYPEDataGridViewTextBoxColumn1";
            this.pRODUCTTYPEDataGridViewTextBoxColumn1.ReadOnly = true;
            this.pRODUCTTYPEDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // iDTESTSTATUSDataGridViewTextBoxColumn1
            // 
            this.iDTESTSTATUSDataGridViewTextBoxColumn1.DataPropertyName = "ID_TESTSTATUS";
            this.iDTESTSTATUSDataGridViewTextBoxColumn1.HeaderText = "ID_TESTSTATUS";
            this.iDTESTSTATUSDataGridViewTextBoxColumn1.MinimumWidth = 18;
            this.iDTESTSTATUSDataGridViewTextBoxColumn1.Name = "iDTESTSTATUSDataGridViewTextBoxColumn1";
            this.iDTESTSTATUSDataGridViewTextBoxColumn1.ReadOnly = true;
            this.iDTESTSTATUSDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // iDSTORAGEDataGridViewTextBoxColumn1
            // 
            this.iDSTORAGEDataGridViewTextBoxColumn1.DataPropertyName = "ID_STORAGE";
            this.iDSTORAGEDataGridViewTextBoxColumn1.HeaderText = "ID_STORAGE";
            this.iDSTORAGEDataGridViewTextBoxColumn1.MinimumWidth = 18;
            this.iDSTORAGEDataGridViewTextBoxColumn1.Name = "iDSTORAGEDataGridViewTextBoxColumn1";
            this.iDSTORAGEDataGridViewTextBoxColumn1.ReadOnly = true;
            this.iDSTORAGEDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // tc_TEST
            // 
            this.tc_TEST.Controls.Add(this.tb_DataGridView);
            this.tc_TEST.Controls.Add(this.tb_BarcodeGenerator);
            this.tc_TEST.Controls.Add(this.tb_PortableSettingsProvider);
            this.tc_TEST.Controls.Add(this.tp_AppVersionManager);
            this.tc_TEST.Controls.Add(this.tp_PasswordTextBox);
            this.tc_TEST.Controls.Add(this.tp_ServiceDesk);
            this.tc_TEST.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tc_TEST.Location = new System.Drawing.Point(0, 0);
            this.tc_TEST.Margin = new System.Windows.Forms.Padding(0);
            this.tc_TEST.Name = "tc_TEST";
            this.tc_TEST.SelectedIndex = 0;
            this.tc_TEST.Size = new System.Drawing.Size(1014, 608);
            this.tc_TEST.TabIndex = 3;
            // 
            // tb_DataGridView
            // 
            this.tb_DataGridView.BackColor = System.Drawing.Color.LightGray;
            this.tb_DataGridView.Controls.Add(this.tlp_TEST);
            this.tb_DataGridView.ForeColor = System.Drawing.Color.Black;
            this.tb_DataGridView.Location = new System.Drawing.Point(4, 22);
            this.tb_DataGridView.Margin = new System.Windows.Forms.Padding(0);
            this.tb_DataGridView.Name = "tb_DataGridView";
            this.tb_DataGridView.Size = new System.Drawing.Size(1006, 582);
            this.tb_DataGridView.TabIndex = 0;
            this.tb_DataGridView.Text = "DataGridView";
            // 
            // tb_BarcodeGenerator
            // 
            this.tb_BarcodeGenerator.BackColor = System.Drawing.Color.LightGray;
            this.tb_BarcodeGenerator.Controls.Add(this.tlp_Main);
            this.tb_BarcodeGenerator.ForeColor = System.Drawing.Color.Black;
            this.tb_BarcodeGenerator.Location = new System.Drawing.Point(4, 22);
            this.tb_BarcodeGenerator.Margin = new System.Windows.Forms.Padding(0);
            this.tb_BarcodeGenerator.Name = "tb_BarcodeGenerator";
            this.tb_BarcodeGenerator.Size = new System.Drawing.Size(1006, 582);
            this.tb_BarcodeGenerator.TabIndex = 1;
            this.tb_BarcodeGenerator.Text = "BarcodeGenerator";
            // 
            // tlp_Main
            // 
            this.tlp_Main.ColumnCount = 2;
            this.tlp_Main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_Main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tlp_Main.Controls.Add(this.btn_Generate, 1, 0);
            this.tlp_Main.Controls.Add(this.tb_Text, 0, 0);
            this.tlp_Main.Controls.Add(this.pb_BarCode, 0, 1);
            this.tlp_Main.Controls.Add(this.cob_BarCodeType, 0, 2);
            this.tlp_Main.Controls.Add(this.nud_BarCodeSize, 1, 2);
            this.tlp_Main.Dock = System.Windows.Forms.DockStyle.Left;
            this.tlp_Main.Location = new System.Drawing.Point(0, 0);
            this.tlp_Main.Margin = new System.Windows.Forms.Padding(0);
            this.tlp_Main.Name = "tlp_Main";
            this.tlp_Main.Padding = new System.Windows.Forms.Padding(5);
            this.tlp_Main.RowCount = 3;
            this.tlp_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tlp_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tlp_Main.Size = new System.Drawing.Size(500, 582);
            this.tlp_Main.TabIndex = 4;
            // 
            // btn_Generate
            // 
            this.btn_Generate.BackColor = System.Drawing.Color.Silver;
            this.btn_Generate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_Generate.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_Generate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Generate.Location = new System.Drawing.Point(415, 7);
            this.btn_Generate.Margin = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.btn_Generate.Name = "btn_Generate";
            this.btn_Generate.Size = new System.Drawing.Size(80, 22);
            this.btn_Generate.TabIndex = 2;
            this.btn_Generate.Text = "GENERATE";
            this.btn_Generate.UseVisualStyleBackColor = false;
            this.btn_Generate.Click += new System.EventHandler(this.btn_Generate_Click);
            // 
            // tb_Text
            // 
            this.tb_Text.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tb_Text.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_Text.Location = new System.Drawing.Point(5, 8);
            this.tb_Text.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.tb_Text.Name = "tb_Text";
            this.tb_Text.Size = new System.Drawing.Size(405, 20);
            this.tb_Text.TabIndex = 0;
            this.tb_Text.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tb_Text_KeyUp);
            // 
            // pb_BarCode
            // 
            this.pb_BarCode.BackColor = System.Drawing.Color.Gainsboro;
            this.tlp_Main.SetColumnSpan(this.pb_BarCode, 2);
            this.pb_BarCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pb_BarCode.Location = new System.Drawing.Point(5, 33);
            this.pb_BarCode.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.pb_BarCode.Name = "pb_BarCode";
            this.pb_BarCode.Padding = new System.Windows.Forms.Padding(50);
            this.pb_BarCode.Size = new System.Drawing.Size(490, 521);
            this.pb_BarCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pb_BarCode.TabIndex = 1;
            this.pb_BarCode.TabStop = false;
            // 
            // cob_BarCodeType
            // 
            this.cob_BarCodeType.BackColor = System.Drawing.Color.Gainsboro;
            this.cob_BarCodeType.DisplayMember = "0";
            this.cob_BarCodeType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cob_BarCodeType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cob_BarCodeType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cob_BarCodeType.FormattingEnabled = true;
            this.cob_BarCodeType.Items.AddRange(new object[] {
            "Code128",
            "DataMatrix"});
            this.cob_BarCodeType.Location = new System.Drawing.Point(5, 556);
            this.cob_BarCodeType.Margin = new System.Windows.Forms.Padding(0);
            this.cob_BarCodeType.Name = "cob_BarCodeType";
            this.cob_BarCodeType.Size = new System.Drawing.Size(405, 21);
            this.cob_BarCodeType.Sorted = true;
            this.cob_BarCodeType.TabIndex = 3;
            this.cob_BarCodeType.SelectedValueChanged += new System.EventHandler(this.cob_BarCodeType_SelectedValueChanged);
            // 
            // nud_BarCodeSize
            // 
            this.nud_BarCodeSize.BackColor = System.Drawing.Color.Gainsboro;
            this.nud_BarCodeSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nud_BarCodeSize.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nud_BarCodeSize.Location = new System.Drawing.Point(410, 556);
            this.nud_BarCodeSize.Margin = new System.Windows.Forms.Padding(0);
            this.nud_BarCodeSize.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nud_BarCodeSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nud_BarCodeSize.Name = "nud_BarCodeSize";
            this.nud_BarCodeSize.ReadOnly = true;
            this.nud_BarCodeSize.Size = new System.Drawing.Size(85, 20);
            this.nud_BarCodeSize.TabIndex = 4;
            this.nud_BarCodeSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nud_BarCodeSize.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nud_BarCodeSize.ValueChanged += new System.EventHandler(this.nud_BarCodeSize_ValueChanged);
            // 
            // tb_PortableSettingsProvider
            // 
            this.tb_PortableSettingsProvider.BackColor = System.Drawing.Color.LightGray;
            this.tb_PortableSettingsProvider.Controls.Add(this.tlp_PortableSettings);
            this.tb_PortableSettingsProvider.Location = new System.Drawing.Point(4, 22);
            this.tb_PortableSettingsProvider.Margin = new System.Windows.Forms.Padding(0);
            this.tb_PortableSettingsProvider.Name = "tb_PortableSettingsProvider";
            this.tb_PortableSettingsProvider.Size = new System.Drawing.Size(1006, 582);
            this.tb_PortableSettingsProvider.TabIndex = 2;
            this.tb_PortableSettingsProvider.Text = "PortableSettingsProvider";
            // 
            // tlp_PortableSettings
            // 
            this.tlp_PortableSettings.ColumnCount = 2;
            this.tlp_PortableSettings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp_PortableSettings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp_PortableSettings.Controls.Add(this.p_Settings_User, 0, 1);
            this.tlp_PortableSettings.Controls.Add(this.p_Settings_Updater, 1, 1);
            this.tlp_PortableSettings.Controls.Add(this.lbl_Setting_User, 0, 0);
            this.tlp_PortableSettings.Controls.Add(this.label4, 1, 0);
            this.tlp_PortableSettings.Location = new System.Drawing.Point(0, 0);
            this.tlp_PortableSettings.Margin = new System.Windows.Forms.Padding(0);
            this.tlp_PortableSettings.Name = "tlp_PortableSettings";
            this.tlp_PortableSettings.RowCount = 2;
            this.tlp_PortableSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tlp_PortableSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_PortableSettings.Size = new System.Drawing.Size(632, 105);
            this.tlp_PortableSettings.TabIndex = 9;
            // 
            // p_Settings_User
            // 
            this.p_Settings_User.Controls.Add(this.lbl_Setting1);
            this.p_Settings_User.Controls.Add(this.lbl_Setting2);
            this.p_Settings_User.Controls.Add(this.tb_Setting1);
            this.p_Settings_User.Controls.Add(this.tb_Setting2);
            this.p_Settings_User.Controls.Add(this.btn_Settings_SaveUser);
            this.p_Settings_User.Dock = System.Windows.Forms.DockStyle.Fill;
            this.p_Settings_User.Location = new System.Drawing.Point(0, 22);
            this.p_Settings_User.Margin = new System.Windows.Forms.Padding(0);
            this.p_Settings_User.Name = "p_Settings_User";
            this.p_Settings_User.Size = new System.Drawing.Size(316, 83);
            this.p_Settings_User.TabIndex = 0;
            // 
            // lbl_Setting1
            // 
            this.lbl_Setting1.AutoSize = true;
            this.lbl_Setting1.Location = new System.Drawing.Point(5, 10);
            this.lbl_Setting1.Name = "lbl_Setting1";
            this.lbl_Setting1.Size = new System.Drawing.Size(49, 13);
            this.lbl_Setting1.TabIndex = 0;
            this.lbl_Setting1.Text = "Setting1:";
            // 
            // lbl_Setting2
            // 
            this.lbl_Setting2.AutoSize = true;
            this.lbl_Setting2.Location = new System.Drawing.Point(5, 36);
            this.lbl_Setting2.Name = "lbl_Setting2";
            this.lbl_Setting2.Size = new System.Drawing.Size(49, 13);
            this.lbl_Setting2.TabIndex = 1;
            this.lbl_Setting2.Text = "Setting2:";
            // 
            // tb_Setting1
            // 
            this.tb_Setting1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_Setting1.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Extension.TEST.set_USER.Default, "test1", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tb_Setting1.Location = new System.Drawing.Point(60, 7);
            this.tb_Setting1.Name = "tb_Setting1";
            this.tb_Setting1.Size = new System.Drawing.Size(253, 20);
            this.tb_Setting1.TabIndex = 2;
            this.tb_Setting1.Text = global::Extension.TEST.set_USER.Default.test1;
            this.tb_Setting1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tb_Setting2
            // 
            this.tb_Setting2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_Setting2.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Extension.TEST.set_USER.Default, "test2", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tb_Setting2.Location = new System.Drawing.Point(60, 33);
            this.tb_Setting2.Name = "tb_Setting2";
            this.tb_Setting2.Size = new System.Drawing.Size(253, 20);
            this.tb_Setting2.TabIndex = 2;
            this.tb_Setting2.Text = global::Extension.TEST.set_USER.Default.test2;
            this.tb_Setting2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btn_Settings_SaveUser
            // 
            this.btn_Settings_SaveUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Settings_SaveUser.BackColor = System.Drawing.Color.Silver;
            this.btn_Settings_SaveUser.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_Settings_SaveUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Settings_SaveUser.Location = new System.Drawing.Point(233, 58);
            this.btn_Settings_SaveUser.Margin = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.btn_Settings_SaveUser.Name = "btn_Settings_SaveUser";
            this.btn_Settings_SaveUser.Size = new System.Drawing.Size(80, 22);
            this.btn_Settings_SaveUser.TabIndex = 3;
            this.btn_Settings_SaveUser.Text = "SAVE";
            this.btn_Settings_SaveUser.UseVisualStyleBackColor = false;
            this.btn_Settings_SaveUser.Click += new System.EventHandler(this.btn_Settings_SaveUser_Click);
            // 
            // p_Settings_Updater
            // 
            this.p_Settings_Updater.Controls.Add(this.lbl_Setting10);
            this.p_Settings_Updater.Controls.Add(this.btn_Settings_SaveUpdater);
            this.p_Settings_Updater.Controls.Add(this.lbl_Setting20);
            this.p_Settings_Updater.Controls.Add(this.tb_Setting20);
            this.p_Settings_Updater.Controls.Add(this.tb_Setting10);
            this.p_Settings_Updater.Dock = System.Windows.Forms.DockStyle.Fill;
            this.p_Settings_Updater.Location = new System.Drawing.Point(316, 22);
            this.p_Settings_Updater.Margin = new System.Windows.Forms.Padding(0);
            this.p_Settings_Updater.Name = "p_Settings_Updater";
            this.p_Settings_Updater.Size = new System.Drawing.Size(316, 83);
            this.p_Settings_Updater.TabIndex = 1;
            // 
            // lbl_Setting10
            // 
            this.lbl_Setting10.AutoSize = true;
            this.lbl_Setting10.Location = new System.Drawing.Point(5, 10);
            this.lbl_Setting10.Name = "lbl_Setting10";
            this.lbl_Setting10.Size = new System.Drawing.Size(55, 13);
            this.lbl_Setting10.TabIndex = 4;
            this.lbl_Setting10.Text = "Setting10:";
            // 
            // btn_Settings_SaveUpdater
            // 
            this.btn_Settings_SaveUpdater.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Settings_SaveUpdater.BackColor = System.Drawing.Color.Silver;
            this.btn_Settings_SaveUpdater.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_Settings_SaveUpdater.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Settings_SaveUpdater.Location = new System.Drawing.Point(233, 58);
            this.btn_Settings_SaveUpdater.Margin = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.btn_Settings_SaveUpdater.Name = "btn_Settings_SaveUpdater";
            this.btn_Settings_SaveUpdater.Size = new System.Drawing.Size(80, 22);
            this.btn_Settings_SaveUpdater.TabIndex = 8;
            this.btn_Settings_SaveUpdater.Text = "SAVE";
            this.btn_Settings_SaveUpdater.UseVisualStyleBackColor = false;
            this.btn_Settings_SaveUpdater.Click += new System.EventHandler(this.btn_Settings_SaveUpdater_Click);
            // 
            // lbl_Setting20
            // 
            this.lbl_Setting20.AutoSize = true;
            this.lbl_Setting20.Location = new System.Drawing.Point(5, 36);
            this.lbl_Setting20.Name = "lbl_Setting20";
            this.lbl_Setting20.Size = new System.Drawing.Size(55, 13);
            this.lbl_Setting20.TabIndex = 5;
            this.lbl_Setting20.Text = "Setting20:";
            // 
            // tb_Setting20
            // 
            this.tb_Setting20.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_Setting20.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Extension.TEST.set_UPDATER.Default, "test20", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tb_Setting20.Location = new System.Drawing.Point(60, 33);
            this.tb_Setting20.Name = "tb_Setting20";
            this.tb_Setting20.Size = new System.Drawing.Size(253, 20);
            this.tb_Setting20.TabIndex = 6;
            this.tb_Setting20.Text = global::Extension.TEST.set_UPDATER.Default.test20;
            this.tb_Setting20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tb_Setting10
            // 
            this.tb_Setting10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_Setting10.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Extension.TEST.set_UPDATER.Default, "test10", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tb_Setting10.Location = new System.Drawing.Point(60, 7);
            this.tb_Setting10.Name = "tb_Setting10";
            this.tb_Setting10.Size = new System.Drawing.Size(253, 20);
            this.tb_Setting10.TabIndex = 7;
            this.tb_Setting10.Text = global::Extension.TEST.set_UPDATER.Default.test10;
            this.tb_Setting10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbl_Setting_User
            // 
            this.lbl_Setting_User.AutoSize = true;
            this.lbl_Setting_User.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_Setting_User.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Setting_User.Location = new System.Drawing.Point(3, 0);
            this.lbl_Setting_User.Name = "lbl_Setting_User";
            this.lbl_Setting_User.Size = new System.Drawing.Size(310, 22);
            this.lbl_Setting_User.TabIndex = 2;
            this.lbl_Setting_User.Text = "UserSettingsProvider";
            this.lbl_Setting_User.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(319, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(310, 22);
            this.label4.TabIndex = 2;
            this.label4.Text = "UpdaterSettingsProvider";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tp_AppVersionManager
            // 
            this.tp_AppVersionManager.BackColor = System.Drawing.Color.LightGray;
            this.tp_AppVersionManager.Controls.Add(this.btn_Changelog);
            this.tp_AppVersionManager.Controls.Add(this.btn_Update_Normal);
            this.tp_AppVersionManager.Controls.Add(this.btn_Update_Silence);
            this.tp_AppVersionManager.ForeColor = System.Drawing.Color.Black;
            this.tp_AppVersionManager.Location = new System.Drawing.Point(4, 22);
            this.tp_AppVersionManager.Margin = new System.Windows.Forms.Padding(0);
            this.tp_AppVersionManager.Name = "tp_AppVersionManager";
            this.tp_AppVersionManager.Size = new System.Drawing.Size(1006, 582);
            this.tp_AppVersionManager.TabIndex = 3;
            this.tp_AppVersionManager.Text = "AppVersionManager";
            // 
            // btn_Changelog
            // 
            this.btn_Changelog.BackColor = System.Drawing.Color.Silver;
            this.btn_Changelog.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_Changelog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Changelog.Location = new System.Drawing.Point(10, 90);
            this.btn_Changelog.Margin = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.btn_Changelog.Name = "btn_Changelog";
            this.btn_Changelog.Size = new System.Drawing.Size(120, 24);
            this.btn_Changelog.TabIndex = 4;
            this.btn_Changelog.Text = "Changelog";
            this.btn_Changelog.UseVisualStyleBackColor = false;
            this.btn_Changelog.Click += new System.EventHandler(this.btn_Changelog_Click);
            // 
            // btn_Update_Normal
            // 
            this.btn_Update_Normal.BackColor = System.Drawing.Color.Silver;
            this.btn_Update_Normal.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_Update_Normal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Update_Normal.Location = new System.Drawing.Point(10, 40);
            this.btn_Update_Normal.Margin = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.btn_Update_Normal.Name = "btn_Update_Normal";
            this.btn_Update_Normal.Size = new System.Drawing.Size(120, 24);
            this.btn_Update_Normal.TabIndex = 4;
            this.btn_Update_Normal.Text = "UPDATE [Normal]";
            this.btn_Update_Normal.UseVisualStyleBackColor = false;
            this.btn_Update_Normal.Click += new System.EventHandler(this.btn_Update_Normal_Click);
            // 
            // btn_Update_Silence
            // 
            this.btn_Update_Silence.BackColor = System.Drawing.Color.Silver;
            this.btn_Update_Silence.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_Update_Silence.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Update_Silence.Location = new System.Drawing.Point(10, 10);
            this.btn_Update_Silence.Margin = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.btn_Update_Silence.Name = "btn_Update_Silence";
            this.btn_Update_Silence.Size = new System.Drawing.Size(120, 24);
            this.btn_Update_Silence.TabIndex = 4;
            this.btn_Update_Silence.Text = "UPDATE [Silence]";
            this.btn_Update_Silence.UseVisualStyleBackColor = false;
            this.btn_Update_Silence.Click += new System.EventHandler(this.btn_Update_Silence_Click);
            // 
            // tp_ServiceDesk
            // 
            this.tp_ServiceDesk.BackColor = System.Drawing.Color.LightGray;
            this.tp_ServiceDesk.Controls.Add(this.wb_ServiceDesk);
            this.tp_ServiceDesk.Location = new System.Drawing.Point(4, 22);
            this.tp_ServiceDesk.Name = "tp_ServiceDesk";
            this.tp_ServiceDesk.Padding = new System.Windows.Forms.Padding(3);
            this.tp_ServiceDesk.Size = new System.Drawing.Size(1006, 582);
            this.tp_ServiceDesk.TabIndex = 4;
            this.tp_ServiceDesk.Text = "ServiceDesk";
            // 
            // wb_ServiceDesk
            // 
            this.wb_ServiceDesk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wb_ServiceDesk.IsWebBrowserContextMenuEnabled = false;
            this.wb_ServiceDesk.Location = new System.Drawing.Point(3, 3);
            this.wb_ServiceDesk.Margin = new System.Windows.Forms.Padding(0);
            this.wb_ServiceDesk.MinimumSize = new System.Drawing.Size(20, 20);
            this.wb_ServiceDesk.Name = "wb_ServiceDesk";
            this.wb_ServiceDesk.Size = new System.Drawing.Size(1000, 576);
            this.wb_ServiceDesk.TabIndex = 0;
            this.wb_ServiceDesk.Url = new System.Uri("https://cws7.conti.de/content/11013784/default.aspx", System.UriKind.Absolute);
            // 
            // tp_PasswordTextBox
            // 
            this.tp_PasswordTextBox.BackColor = System.Drawing.Color.LightGray;
            this.tp_PasswordTextBox.Controls.Add(this.btn_SeePassword);
            this.tp_PasswordTextBox.Controls.Add(this.ptb_Password);
            this.tp_PasswordTextBox.Location = new System.Drawing.Point(4, 22);
            this.tp_PasswordTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.tp_PasswordTextBox.Name = "tp_PasswordTextBox";
            this.tp_PasswordTextBox.Size = new System.Drawing.Size(1006, 582);
            this.tp_PasswordTextBox.TabIndex = 5;
            this.tp_PasswordTextBox.Text = "PasswordTextBox";
            // 
            // btn_SeePassword
            // 
            this.btn_SeePassword.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btn_SeePassword.FlatAppearance.BorderSize = 0;
            this.btn_SeePassword.Location = new System.Drawing.Point(160, 9);
            this.btn_SeePassword.Name = "btn_SeePassword";
            this.btn_SeePassword.Size = new System.Drawing.Size(20, 22);
            this.btn_SeePassword.TabIndex = 3;
            this.btn_SeePassword.Text = "S";
            this.btn_SeePassword.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_SeePassword.UseVisualStyleBackColor = true;
            this.btn_SeePassword.Click += new System.EventHandler(this.btn_SeePassword_Click_1);
            // 
            // ptb_Password
            // 
            this.ptb_Password.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ptb_Password.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.ptb_Password.Location = new System.Drawing.Point(10, 10);
            this.ptb_Password.Margin = new System.Windows.Forms.Padding(0);
            this.ptb_Password.MaxLength = 255;
            this.ptb_Password.Name = "ptb_Password";
            this.ptb_Password.Size = new System.Drawing.Size(150, 20);
            this.ptb_Password.TabIndex = 2;
            // 
            // frm_MAIN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(1014, 608);
            this.Controls.Add(this.tc_TEST);
            this.Name = "frm_MAIN";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "...TEST...";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_MAIN_FormClosing);
            this.Load += new System.EventHandler(this.frm_MAIN_Load);
            this.LocationChanged += new System.EventHandler(this.frm_MAIN_LocationChanged);
            this.tlp_TEST.ResumeLayout(false);
            this.p_Filter.ResumeLayout(false);
            this.p_Filter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edgv_DGV_BGWM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ds_APP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edgv_DGV_STP)).EndInit();
            this.tc_TEST.ResumeLayout(false);
            this.tb_DataGridView.ResumeLayout(false);
            this.tb_BarcodeGenerator.ResumeLayout(false);
            this.tlp_Main.ResumeLayout(false);
            this.tlp_Main.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_BarCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_BarCodeSize)).EndInit();
            this.tb_PortableSettingsProvider.ResumeLayout(false);
            this.tlp_PortableSettings.ResumeLayout(false);
            this.tlp_PortableSettings.PerformLayout();
            this.p_Settings_User.ResumeLayout(false);
            this.p_Settings_User.PerformLayout();
            this.p_Settings_Updater.ResumeLayout(false);
            this.p_Settings_Updater.PerformLayout();
            this.tp_AppVersionManager.ResumeLayout(false);
            this.tp_ServiceDesk.ResumeLayout(false);
            this.tp_PasswordTextBox.ResumeLayout(false);
            this.tp_PasswordTextBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.CheckBox chb_Group;
        public Extension.EDGV.ExtendedDataGridView edgv_DGV_BGWM;
        public System.Windows.Forms.TableLayoutPanel tlp_TEST;
        public System.Windows.Forms.Panel p_Filter;
        public System.Windows.Forms.Panel p_Filter_Split1;
        public System.Windows.Forms.Button btn_Filter_Clear;
        public System.Windows.Forms.Label lbl_Filter;
        public System.Windows.Forms.TextBox tb_Filter;
        public System.Windows.Forms.TabControl tc_TEST;
        public System.Windows.Forms.TabPage tb_DataGridView;
        public System.Windows.Forms.TabPage tb_BarcodeGenerator;
        public System.Windows.Forms.Button btn_Refresh;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.TableLayoutPanel tlp_Main;
        public System.Windows.Forms.Button btn_Generate;
        public System.Windows.Forms.TextBox tb_Text;
        public System.Windows.Forms.PictureBox pb_BarCode;
        public System.Windows.Forms.ComboBox cob_BarCodeType;
        public System.Windows.Forms.NumericUpDown nud_BarCodeSize;
        public System.Windows.Forms.TabPage tb_PortableSettingsProvider;
        public System.Windows.Forms.Button btn_Settings_SaveUser;
        public System.Windows.Forms.TextBox tb_Setting2;
        public System.Windows.Forms.TextBox tb_Setting1;
        public System.Windows.Forms.Label lbl_Setting2;
        public System.Windows.Forms.Label lbl_Setting1;
        public System.Windows.Forms.TableLayoutPanel tlp_PortableSettings;
        public System.Windows.Forms.Panel p_Settings_User;
        public System.Windows.Forms.Panel p_Settings_Updater;
        public System.Windows.Forms.Label lbl_Setting10;
        public System.Windows.Forms.Button btn_Settings_SaveUpdater;
        public System.Windows.Forms.Label lbl_Setting20;
        public System.Windows.Forms.TextBox tb_Setting20;
        public System.Windows.Forms.TextBox tb_Setting10;
        public System.Windows.Forms.Label lbl_Setting_User;
        public System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tp_AppVersionManager;
        public System.Windows.Forms.Button btn_Update_Normal;
        public System.Windows.Forms.Button btn_Update_Silence;
        public System.Windows.Forms.Button btn_Changelog;
        private ds_APP ds_APP;
        //private System.Windows.Forms.DataGridViewTextBoxColumn tIMETOFINISHDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn tIMESPANTOFINISHDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn lOCATIONFINISHDATEDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn iDORDERDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn iDEMBADataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn pROJECTNAMEDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn aMOUNTDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn bOOKEDDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn iDSTARTUPGROUPDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn sTARTUPGROUPNAMEDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn iDPRIORITYDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn pRIORITYNAMEDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn iDLOCATIONDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn lOCATIONNAMEDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn lOCATIONPROCESSQUOTADataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn eSTIMATEDDATEDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn pACKSTATEDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn cREATIONDATEDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn bOXESDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn cREATORDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn lASTSCANDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn sAMPCOMMENTDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn pICOMMENTDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn iDSAMPLEPRODUCTIONSTATEDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn sAMPLEPRODUCTIONSTATENAMEDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn pRODUCTTYPEDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn vARIANTTYPEDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn sAPDELIVERYNUMBERDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn tARGETDATEDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn iDORDERSTATUSDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn oRDERSTATUSNAMEDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn iDTESTSTATUSDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn tESTSTATUSNAMEDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn iDSTORAGEDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn sKZNUMBERDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn gLOBALPSNUMBERDataGridViewTextBoxColumn;
        public EDGV.ExtendedDataGridView edgv_DGV_STP;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDORDERDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDEMBADataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn aMOUNTDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDSTARTUPGROUPDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDPRIORITYDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDLOCATIONDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn pACKSTATEDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cREATIONDATEDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn bOXESDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cREATORDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn lASTSCANDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn sAMPCOMMENTDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn pICOMMENTDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn pRODUCTTYPEDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDTESTSTATUSDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDSTORAGEDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn bOMFINDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sOLDERINGDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pCBNUMBERDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn tIMEINTENSIVEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fINISHEDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn oP2DELIVERYIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cEOSNRDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gLOBALPSDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mCUDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pCUDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vECUPNDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dELIVERYSTATUSDataGridViewTextBoxColumn;
        private System.Windows.Forms.TabPage tp_ServiceDesk;
        private System.Windows.Forms.WebBrowser wb_ServiceDesk;
        private System.Windows.Forms.TabPage tp_PasswordTextBox;
        public System.Windows.Forms.Button btn_SeePassword;
        public PTB.PasswordTextBox ptb_Password;
    }
}

