﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Extension.BCG;
using Extension.EDGV;
using Extension.DGVG;
using Extension.AVM;
using System.IO;
using Extension.TEST.ds_APPTableAdapters;
using static Extension.TEST.ds_APP;
using System.Threading;

namespace Extension.TEST
{
    public partial class frm_MAIN : Form
    {
        #region   INIT
        
        private static string UpdateURL = Path.Combine(Path.GetDirectoryName(Application.StartupPath), @"_updatesample\_UPDATE.xml");

        // add tableadapter
        private TV_ORDERTableAdapter ta_TV_Order = new TV_ORDERTableAdapter();
        private ORDERTableAdapter    ta_Order    = new ORDERTableAdapter();
        
        // add datatable
        private TV_ORDERDataTable dt_TV_Order = new TV_ORDERDataTable();
        private ORDERDataTable    dt_Order    = new ORDERDataTable();

        // add datagridview grouper
        private DataGridViewGrouper dgvg_DGV_BGWM;
        private DataGridViewGrouper dgvg_DGV_STP;

        public frm_MAIN()
        {
            InitializeComponent();
            
            // init datagridview double buffering
            edgv_DGV_BGWM.DoubleBuffered(true);
            edgv_DGV_STP.DoubleBuffered(true);

            // init datagridview grouper
            dgvg_DGV_BGWM = new DataGridViewGrouper(edgv_DGV_BGWM);
            dgvg_DGV_STP  = new DataGridViewGrouper(edgv_DGV_STP);
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   FORM

        private void frm_MAIN_Load(object sender, EventArgs e)
        {
            this.Location    = set_USER.Default.app_location;
            this.WindowState = set_USER.Default.app_windowstate;
        }

        private void frm_MAIN_FormClosing(object sender, FormClosingEventArgs e)
        {
            set_USER.Default.app_location    = (this.WindowState != FormWindowState.Maximized) ? this.Location : set_USER.Default.app_location;
            set_USER.Default.app_windowstate = this.WindowState;
            set_USER.Default.Save();
        }

        private void frm_MAIN_LocationChanged(object sender, EventArgs e)
        {
            if (Control.MouseButtons == MouseButtons.Left
                && this.WindowState == FormWindowState.Normal)
            {
                set_USER.Default.app_location = this.Location;
            }
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   DATA_BASE

        public void Refresh_Data()
        {
            dt_TV_Order = ta_TV_Order.GetDTBy_PackStatus_CreationDate(1, Convert.ToDateTime("01.01.2018 00:00:00"));

            edgv_DGV_BGWM.Invoke(() =>
            {
                dgvg_DGV_BGWM.ResetGrouping();

                try
                {
                    DataView dataview = dt_TV_Order.AsDataView();
                    dataview.Sort = "";
                    edgv_DGV_BGWM.DataSource = dataview;
                    edgv_DGV_BGWM.Refresh();
                    edgv_DGV_BGWM.Update();
                }
                catch (Exception)
                {
                    MessageBox.Show("Something is wrong, can`t set datasource !!!",
                                    "ERROR",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }

                if (chb_Group.Checked)
                {
                    dgvg_DGV_BGWM.SetGroupOn("CREATOR");
                }
                else
                {
                    try
                    {
                        ((GroupingSource)edgv_DGV_BGWM.DataSource).Filter = "";
                        ((GroupingSource)edgv_DGV_BGWM.DataSource).Sort = "";
                    }
                    catch { }

                    dgvg_DGV_BGWM.RemoveGrouping();
                }
            });
        }

        public void Refresh_Data_STP()
        {
            dt_Order = ta_Order.GetDTBy_PackStatus_CreationDate(1, Convert.ToDateTime("01.01.2018 00:00:00"));

            edgv_DGV_STP.Invoke(() =>
            {
                dgvg_DGV_STP.ResetGrouping();

                try
                {
                    DataView dataview = dt_Order.AsDataView();
                    dataview.Sort = "";
                    edgv_DGV_STP.DataSource = dataview;
                    edgv_DGV_STP.Refresh();
                    edgv_DGV_STP.Update();
                }
                catch (Exception)
                {
                    MessageBox.Show("Something is wrong, can`t set datasource !!!",
                                    "ERROR",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }

                if (chb_Group.Checked)
                {
                    dgvg_DGV_STP.SetGroupOn("CREATOR");
                }
                else
                {
                    try
                    {
                        ((GroupingSource)edgv_DGV_STP.DataSource).Filter = "";
                        ((GroupingSource)edgv_DGV_STP.DataSource).Sort = "";
                    }
                    catch { }

                    dgvg_DGV_STP.RemoveGrouping();
                }
            });
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   EDGV   DGV

        private void edgv_DGV_FilterStringChanged(object sender, EventArgs e)
        {
            try
            {
                edgv_DGV_SetFilter();
            }
            catch (Exception)
            {
                MessageBox.Show(this,
                                "Something is wrong, can`t set datatable filter!",
                                "Error...",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void edgv_DGV_SortStringChanged(object sender, EventArgs e)
        {
            try
            {
                edgv_DGV_SetSort();
            }
            catch (Exception)
            {
                MessageBox.Show(this,
                                "Something is wrong, can`t set datatable sort!",
                                "Error...",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   CONTROLS

        private void tb_Filter_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter
                || e.KeyCode == Keys.Return)
            {
                edgv_DGV_SetFilter();
            }
        }

        private void btn_Filter_Clear_Click(object sender, EventArgs e)
        {
            edgv_DGV_ResetFilter();
            edgv_DGV_BGWM.Refresh();
        }

        private void chb_Group_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkbox = (CheckBox)sender;

            if (checkbox.Checked)
            {
                dgvg_DGV_BGWM.SetGroupOn("CREATOR");
            }
            else
            {
                try
                {
                    ((GroupingSource)edgv_DGV_BGWM.DataSource).Filter = "";
                    ((GroupingSource)edgv_DGV_BGWM.DataSource).Sort = "";
                }
                catch { }

                dgvg_DGV_BGWM.RemoveGrouping();
                edgv_DGV_SetFilter();
            }
        }

        private void btn_Refresh_Click(object sender, EventArgs e)
        {
            //Refresh_Data();
            //Refresh_Data_STP();

            Thread t1 = new Thread(delegate () { Refresh_Data(); });
            Thread t2 = new Thread(delegate () { Refresh_Data_STP(); });

            t1.Start();
            t2.Start();
        }

        // barcode generator
        private void tb_Text_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return
                || e.KeyCode == Keys.Enter)
            {
                btn_Generate.PerformClick();
            }
        }

        private void btn_Generate_Click(object sender, EventArgs e)
        {
            if (tb_Text.Text == "")
            {
                pb_BarCode.Image = null;
                return;
            }

            pb_BarCode.Image = GetBarcodeImage(tb_Text.Text);
        }

        private void nud_BarCodeSize_ValueChanged(object sender, EventArgs e)
        {
            btn_Generate.PerformClick();
        }

        private void cob_BarCodeType_SelectedValueChanged(object sender, EventArgs e)
        {
            nud_BarCodeSize.Value = 1;
            btn_Generate.PerformClick();
        }

        // portable user settings provider
        private void btn_Settings_SaveUser_Click(object sender, EventArgs e)
        {
            set_USER.Default.Save();
        }

        private void btn_Settings_SaveUpdater_Click(object sender, EventArgs e)
        {
            set_UPDATER.Default.Save();
        }

        // application version manager
        private void btn_Update_Silence_Click(object sender, EventArgs e)
        {
            AppUpdater.Start(UpdateURL, true);
        }

        private void btn_Update_Normal_Click(object sender, EventArgs e)
        {
            AppUpdater.Start(UpdateURL);
        }

        private void btn_Changelog_Click(object sender, EventArgs e)
        {
            Changelog.Show(UpdateURL);
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   DGV_GROUPER

        // display group

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   FILTER

        private void edgv_DGV_SetFilter()
        {
            string[] txt = null;

            if (tb_Filter.Text != "") txt = tb_Filter.Text.Split(' ');

            try
            {
                StringBuilder filterstring = new StringBuilder();

                if (txt != null)
                {
                    for (int i = 0; i < txt.Count(); i++)
                    {
                        if (txt[i].Length > 0)
                        {
                            filterstring.Append(string.Format("Convert([{0}], 'System.String') LIKE '%{1}%'", "ID_ORDER", txt[i].Trim().Replace("'", "''")));
                            filterstring.Append((txt.Count() > 1 && i < txt.Count() - 1) ? " AND " : "");
                        }
                    }
                }

                if (edgv_DGV_BGWM.FilterString != "")
                {
                    if (filterstring.Length > 0) filterstring.Append(" AND ");

                    filterstring.Append(edgv_DGV_BGWM.FilterString);
                }

                if (edgv_DGV_BGWM.DataSource is GroupingSource)
                {
                    ((GroupingSource)edgv_DGV_BGWM.DataSource).Filter = filterstring.ToString();
                }
                else if (edgv_DGV_BGWM.DataSource is DataView)
                {
                    ((DataView)edgv_DGV_BGWM.DataSource).RowFilter = filterstring.ToString();
                }

                edgv_DGV_BGWM.Update();
            }
            catch (Exception)
            {
                MessageBox.Show(this,
                                "Something is wrong, can`t set datatable filter!",
                                "Error...",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        public void edgv_DGV_ResetFilter()
        {
            tb_Filter.Clear();
            edgv_DGV_BGWM.ClearFilter(true);
            edgv_DGV_BGWM.ClearSort(true);

            try
            {
                if (edgv_DGV_BGWM.DataSource is GroupingSource)
                {
                    ((GroupingSource)edgv_DGV_BGWM.DataSource).Filter = edgv_DGV_BGWM.FilterString;
                    ((GroupingSource)edgv_DGV_BGWM.DataSource).Sort = "";
                }
                else if (edgv_DGV_BGWM.DataSource is DataView)
                {
                    ((DataView)edgv_DGV_BGWM.DataSource).RowFilter = edgv_DGV_BGWM.FilterString;
                    ((DataView)edgv_DGV_BGWM.DataSource).Sort = "";
                }
            }
            catch (Exception) { }
        }

        private void edgv_DGV_SetSort()
        {
            if (edgv_DGV_BGWM.DataSource is GroupingSource)
            {
                ((GroupingSource)edgv_DGV_BGWM.DataSource).Sort = (edgv_DGV_BGWM.SortString != null && edgv_DGV_BGWM.SortString != "") ? edgv_DGV_BGWM.SortString
                                                                                                                        : "";
            }
            else if (edgv_DGV_BGWM.DataSource is DataView)
            {
                ((DataView)edgv_DGV_BGWM.DataSource).Sort = (edgv_DGV_BGWM.SortString != null && edgv_DGV_BGWM.SortString != "") ? edgv_DGV_BGWM.SortString
                                                                                                                  : "";
            }

            edgv_DGV_BGWM.Update();
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   FUNCTION

        private Image GetBarcodeImage(string text)
        {
            switch (cob_BarCodeType.Text)
            {
                case "Code128":
                    Barcode code128       = new Barcode();
                    code128.Alignment     = AlignmentPositions.CENTER;
                    code128.BarWidth      = Convert.ToInt16(nud_BarCodeSize.Value);
                    code128.IncludeLabel  = true;
                    code128.LabelPosition = LabelPositions.BOTTOMCENTER;
                    code128.Height        = 25 + Convert.ToInt16(nud_BarCodeSize.Value) * 25;
                    code128.LabelFont     = new Font(code128.LabelFont.Name, 20, FontStyle.Regular);
                    code128.ForeColor     = Color.Black;
                    code128.BackColor     = pb_BarCode.BackColor;

                    return code128.Encode(BarCodeType.CODE128, text.Trim());

                case "DataMatrix":
                    DataMatrixCode datamatrix       = new DataMatrixCode();
                    DmtxImageEncoderOptions options = new DmtxImageEncoderOptions();
                    options.ModuleSize              = Convert.ToInt16(nud_BarCodeSize.Value); // image start size
                    options.MarginSize              = 0; // image border size
                    options.BackColor               = pb_BarCode.BackColor;
                    options.ForeColor               = Color.Black;

                    return datamatrix.EncodeImage(text.Trim(), options);
            }

            return null;
        }
        
        private void btn_SeePassword_Click_1(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            button.DialogResult = (button.DialogResult == DialogResult.Yes) ? DialogResult.No : DialogResult.Yes;

            switch (button.DialogResult)
            {
                case DialogResult.Yes:
                    ptb_Password.Text = ptb_Password.ShowPassword;
                    ptb_Password.IsHidden = false;
                    break;

                case DialogResult.No:
                    ptb_Password.HidePassword();
                    break;
            }
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    }
}
