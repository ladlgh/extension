﻿using System;
using System.Windows.Forms;
using System.Configuration;

namespace Extension.TEST
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frm_MAIN());
        }
    }

    // Extension.TEST.psp_User
    public sealed class psp_User : PortableSettingsProvider
    {
        public override string ClassName            { get { return "UserSettingsProvider";  } }
        public override string RootSubPath          { get { return "cfg";                   } }
        public override bool   RootSubPathIsHidden  { get { return false;                   } }
        public override string FileName             { get { return "user";                  } }
        public override string XmlRootNodeName      { get { return "UserSettings";          } }
    }

    // Extension.TEST.psp_Updater
    public sealed class psp_Updater : PortableSettingsProvider
    {
        public override string ClassName            { get { return "UpdaterSettingsProvider";   } }
        public override string RootSubPath          { get { return "cfg";                       } }
        public override bool   RootSubPathIsHidden  { get { return false;                       } }
        public override string FileName             { get { return "updater";                   } }
        public override string XmlRootNodeName      { get { return "UpdaterSettings";           } }
    }
}
