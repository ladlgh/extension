﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Extension.TEST {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
    internal sealed partial class set_UPDATER : global::System.Configuration.ApplicationSettingsBase {
        
        private static set_UPDATER defaultInstance = ((set_UPDATER)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new set_UPDATER())));
        
        public static set_UPDATER Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.SettingsProviderAttribute(typeof(Extension.TEST.psp_Updater))]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("test11")]
        public string test10 {
            get {
                return ((string)(this["test10"]));
            }
            set {
                this["test10"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.SettingsProviderAttribute(typeof(Extension.TEST.psp_Updater))]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("test21")]
        public string test20 {
            get {
                return ((string)(this["test20"]));
            }
            set {
                this["test20"] = value;
            }
        }
    }
}
