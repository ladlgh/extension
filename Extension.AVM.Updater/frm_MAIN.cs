﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Cache;
using System.Windows.Forms;
using Extension.Properties;

namespace Extension.AVM.Updater
{
    public partial class frm_MAIN : Form
    {
        #region   INIT

        private static string AppFilePath;
        private static string DownloadURL;
        private static string TempUpdatePath;
        private static string UpdateFilePath;
        private static bool   IsSilenceUpdate;
        
        private WebClient webClient;
        private BackgroundWorker _backgroundWorker;

        public frm_MAIN()
        {
            InitializeComponent();
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   FORM

        private void frm_Update_Load(object sender, EventArgs e)
        {
            string[] args = Environment.GetCommandLineArgs();

            // only for test
            /*MessageBox.Show("frm_Installer\n"
                            + "args[0] == " + args[0] + "\n"
                            + "args[1] == " + args[1] + "\n"
                            + "args[2] == " + args[2] + "\n"
                            + "args[3] == " + args[3] + "\n");*/

            if (args.Length < 4
                || !File.Exists(args[1])
                || !Path.GetExtension(args[1]).ToLower().Equals(".exe"))
            {
                DialogResult = DialogResult.Abort;
                this.Close();
            }
            else
            {
                AppFilePath     = args[1];
                DownloadURL     = args[2];
                TempUpdatePath  = args[3];
                IsSilenceUpdate = Convert.ToBoolean(args[4]);
            }
            
            lbl_Info.Text   = Resources.status_download;
        }

        private void frm_Update_Shown(object sender, EventArgs e)
        {
            DownloadFile(DownloadURL);
        }

        private void frm_Update_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!DialogResult.Equals(DialogResult.Abort))
            {
                if (webClient.IsBusy)
                {
                    webClient.CancelAsync();
                }

                if (_backgroundWorker.IsBusy)
                {
                    _backgroundWorker.CancelAsync();
                }
            }
        }

        private void frm_Update_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!DialogResult.Equals(DialogResult.OK)
                && !DialogResult.Equals(DialogResult.Abort))
            {
                try
                {
                    if (!IsSilenceUpdate)
                    {
                        MessageBox.Show(this,
                                        "Updating aborted!",
                                        "Warning...",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Warning);
                    }

                    Process.Start(AppFilePath);
                }
                catch (Win32Exception exception)
                {
                    if (exception.NativeErrorCode != 1223)
                    {
                        throw;
                    }
                }
            }
            else
            {
                try
                {
                    if (!IsSilenceUpdate)
                    {
                        MessageBox.Show(this,
                                        "Updating successfully!",
                                        "Information...",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);
                    }

                    Process.Start(AppFilePath);
                }
                catch (Win32Exception exception)
                {
                    if (exception.NativeErrorCode != 1223)
                    {
                        throw;
                    }
                }
            }
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   DOWNLOAD

        private void DownloadFile(string downloadURL)
        {
            webClient = new WebClient();
            webClient.CachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore);
            
            Uri uri = new Uri(downloadURL);
            UpdateFilePath = Path.Combine(TempUpdatePath, GetFileName(downloadURL));

            webClient.DownloadProgressChanged += OnDownloadProgressChanged;
            webClient.DownloadFileCompleted   += OnDownloadComplete;

            webClient.DownloadFileAsync(uri, UpdateFilePath);
        }

        private void OnDownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            pb_Progress.Value = e.ProgressPercentage;
        }

        private void OnDownloadComplete(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                return;
            }

            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message,
                                e.Error.GetType().ToString(),
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return;
            }

            UpdateFiles();
        }

        private static string GetFileName(string url, string httpWebRequestMethod = "HEAD")
        {
            try
            {
                var fileName = string.Empty;
                var uri = new Uri(url);

                if (uri.Scheme.Equals(Uri.UriSchemeHttp) || uri.Scheme.Equals(Uri.UriSchemeHttps))
                {
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
                    httpWebRequest.CachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore);
                    httpWebRequest.Method = httpWebRequestMethod;
                    httpWebRequest.AllowAutoRedirect = false;
                    string contentDisposition;

                    using (var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse())
                    {
                        if (httpWebResponse.StatusCode.Equals(HttpStatusCode.Redirect) ||
                            httpWebResponse.StatusCode.Equals(HttpStatusCode.Moved) ||
                            httpWebResponse.StatusCode.Equals(HttpStatusCode.MovedPermanently))
                        {
                            if (httpWebResponse.Headers["Location"] != null)
                            {
                                var location = httpWebResponse.Headers["Location"];
                                fileName = GetFileName(location);

                                return fileName;
                            }
                        }

                        contentDisposition = httpWebResponse.Headers["content-disposition"];
                    }

                    if (!string.IsNullOrEmpty(contentDisposition))
                    {
                        const string lookForFileName = "filename=";
                        var index = contentDisposition.IndexOf(lookForFileName, StringComparison.CurrentCultureIgnoreCase);

                        if (index >= 0)
                        {
                            fileName = contentDisposition.Substring(index + lookForFileName.Length);
                        }

                        if (fileName.StartsWith("\"") && fileName.EndsWith("\""))
                        {
                            fileName = fileName.Substring(1, fileName.Length - 2);
                        }
                    }
                }

                if (string.IsNullOrEmpty(fileName))
                {
                    fileName = Path.GetFileName(uri.LocalPath);
                }

                return fileName;
            }
            catch (WebException)
            {
                return GetFileName(url, "GET");
            }
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   UPDATE

        private void UpdateFiles()
        {
            // set form style
            lbl_Info.Text     = Resources.status_install;
            pb_Progress.Value = 0;

            // check application process
            foreach (var process in Process.GetProcesses())
            {
                try
                {
                    if (process.MainModule.FileName.Equals(AppFilePath))
                    {
                        lbl_Info.Text = @"Please exit application manually...";
                        process.WaitForExit();
                    }
                }
                catch (Exception exception)
                {
                    Debug.WriteLine(exception.Message);
                }
            }

            // extract all the files
            _backgroundWorker = new BackgroundWorker
            {
                WorkerReportsProgress      = true,
                WorkerSupportsCancellation = true
            };

            _backgroundWorker.DoWork += (o, eventArgs) =>
            {
                string extract_path = Path.GetDirectoryName(AppFilePath);

                // open an existing zip file for reading
                ZipStorer zip = ZipStorer.Open(UpdateFilePath, FileAccess.Read);

                // read the central directory collection
                List<ZipStorer.ZipFileEntry> dir = zip.ReadCentralDir();

                for (var index = 0; index < dir.Count; index++)
                {
                    if (_backgroundWorker.CancellationPending)
                    {
                        eventArgs.Cancel = true;
                        zip.Close();

                        return;
                    }

                    ZipStorer.ZipFileEntry entry = dir[index];
                    zip.ExtractFile(entry, Path.Combine(extract_path, entry.FilenameInZip));
                    _backgroundWorker.ReportProgress((index + 1) * 100 / dir.Count, string.Format(Resources.label_extracting, entry.FilenameInZip));
                }

                zip.Close();
            };

            _backgroundWorker.ProgressChanged += (o, eventArgs) =>
            {
                pb_Progress.Value = eventArgs.ProgressPercentage;
            };

            _backgroundWorker.RunWorkerCompleted += (o, eventArgs) =>
            {
                if (!eventArgs.Cancelled)
                {
                    lbl_Info.Text = @"Finished";

                    DialogResult = DialogResult.OK;

                    //Application.Exit();
                    this.Close();
                }
            };

            lbl_Info.Text = Resources.status_install;
            _backgroundWorker.RunWorkerAsync();
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    }
}
