﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Cache;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Text.RegularExpressions;
using Extension.Properties;

namespace Extension.AVM
{
    /// <summary>
    /// Main class that lets you auto update applications by setting some static fields and executing its Start method.
    /// </summary>
    public static class AppInstaller
    {
        #region   INIT
        
        public static string      RootSubPath       = ".avm";
        public static string      TempInstallPath;
        public static string      AppTitle;
        public static string      AppCastURL;
        public static string      InstallCastURL;
        public static bool        OpenDownloadPage;
        public static CultureInfo CurrentCulture;
        public static bool        ShowAtOwnThread   = false;
        public static bool        ShowSkipButton    = false;
        public static bool        ReportErrors      = true;
        
        public delegate void CheckForInstallEventHandler(InstallInfoEventArgs args);
        public static event  CheckForInstallEventHandler CheckForInstallEvent;
        
        internal static string  ChangeLogURL;
        internal static string  DownloadURL;
        internal static string  DownloadURL64;
        internal static Version CurrentVersion;
        internal static Version InstalledVersion;
        internal static bool    IsWinFormsApplication;
        internal static bool    Running;

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   EXECUTION

        /// <summary>
        /// Start checking for new version of application and display dialog to the user if update is available.
        /// </summary>
        /// <param name="myAssembly">Assembly to use for version checking.</param>
        public static void Start(Assembly myAssembly = null)
        {
            if (InstallCastURL != null)
            {
                if (AppCastURL != null)
                {
                    Start(InstallCastURL, AppCastURL, myAssembly);
                }
                else
                {
                    Start(InstallCastURL, myAssembly);
                }
            }
            else
            {
                MessageBox.Show("Please set all needed variables!\n"
                                + "(e.g. InstallCastURL)",
                                "Error...",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Start checking for new version of application and display dialog to the user if update is available.
        /// </summary>
        /// <param name="updateCast">URL of the xml file that contains information about latest version of the application.</param>
        /// <param name="myAssembly">Assembly to use for version checking.</param>
        public static void Start(string installCast, Assembly myAssembly = null)
        {
            Start(installCast, Path.GetDirectoryName(Application.ExecutablePath), myAssembly);
        }

        /// <summary>
        /// Start checking for new version of application and display dialog to the user if update is available.
        /// </summary>
        /// <param name="updateCast">URL of the xml file that contains information about latest version of the application.</param>
        /// <param name="appCast">URL of the current application.</param>
        /// <param name="myAssembly">Assembly to use for version checking.</param>
        public static void Start(string installCast, string appCast, Assembly myAssembly = null)
        {
            try
            {
                // init
                if (!Directory.Exists(appCast))
                {
                    Directory.CreateDirectory(appCast);
                }
                
                TempInstallPath = Path.Combine(Path.Combine(appCast, RootSubPath), ".temp");
                AppCastURL = appCast;

                if (!Running)
                {
                    Running = true;

                    // check URLs
                    if (!Directory.Exists(appCast))
                    {
                        Directory.CreateDirectory(appCast);
                    }

                    if (Directory.Exists(TempInstallPath))
                    {
                        Directory.Delete(TempInstallPath, true);
                    }

                    CurrentCulture = (CurrentCulture == null) ? CultureInfo.CurrentCulture : CurrentCulture;
                    InstallCastURL = (InstallCastURL == null) ? installCast : InstallCastURL;

                    IsWinFormsApplication = Application.MessageLoop;
                    
                    InitializeInstaller(LoadXML(myAssembly ?? Assembly.GetEntryAssembly()));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:\n"
                                + ex.ToString(),
                                "Error...",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   EVENTARGS

        private static InstallInfoEventArgs LoadXML(Assembly myAssembly)
        {
            InstallInfoEventArgs args = new InstallInfoEventArgs();

            Regex rgx_URL = new Regex(@"^[.]{1,3}\\");

            Assembly mainAssembly = myAssembly;
            AssemblyCompanyAttribute companyAttribute = (AssemblyCompanyAttribute)Extension.GetAttribute(mainAssembly, typeof(AssemblyCompanyAttribute));

            if (string.IsNullOrEmpty(AppTitle))
            {
                AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)Extension.GetAttribute(mainAssembly, typeof(AssemblyTitleAttribute));
                AppTitle = (titleAttribute != null) ? titleAttribute.Title : mainAssembly.GetName().Name;
            }

            string appCompany = (companyAttribute != null) ? companyAttribute.Company : "";

            // check istalled version
            InstalledVersion = mainAssembly.GetName().Version;

            // open webrequest
            WebRequest webRequest  = WebRequest.Create(InstallCastURL);
            webRequest.CachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore);
            WebResponse webResponse;

            try
            {
                webResponse = webRequest.GetResponse();
            }
            catch (Exception)
            {
                return InstallInfoEventArgs.Empty;
            }

            // load xml file
            XmlDocument document;

            using (Stream appCastStream = webResponse.GetResponseStream())
            {
                document = new XmlDocument();

                if (appCastStream != null)
                {
                    document.Load(appCastStream);
                }
                else
                {
                    webResponse.Close();

                    return InstallInfoEventArgs.Empty;
                }
            }

            // check xml file items
            XmlNodeList install = document.SelectNodes("update");

            if (install != null)
            {
                foreach (XmlNode node in install)
                {
                    XmlNode appCastVersion   = node.SelectSingleNode("version");
                    XmlNode appCastUrl       = node.SelectSingleNode("url");
                    XmlNode appCastUrl64     = node.SelectSingleNode("url64");
                    XmlNode appCastChangeLog = node.SelectSingleNode("changelog");

                    if (appCastVersion != null)
                    {
                        CurrentVersion = new Version(appCastVersion.InnerText);

                        if (CurrentVersion == null)
                        {
                            webResponse.Close();

                            return InstallInfoEventArgs.Empty;
                        }
                    }
                    else
                    {
                        continue;
                    }

                    DownloadURL   = webResponse.ResponseUri.GetURL(appCastUrl);
                    DownloadURL64 = (IntPtr.Size.Equals(8)) ? webResponse.ResponseUri.GetURL(appCastUrl64) : string.Empty;
                    ChangeLogURL  = webResponse.ResponseUri.GetURL(appCastChangeLog);

                    DownloadURL   = (rgx_URL.IsMatch(DownloadURL)) ? Path.Combine(Path.GetDirectoryName(InstallCastURL), rgx_URL.Replace(DownloadURL, ""))
                                                                   : DownloadURL;
                    DownloadURL64 = (rgx_URL.IsMatch(DownloadURL64)) ? Path.Combine(Path.GetDirectoryName(InstallCastURL), rgx_URL.Replace(DownloadURL64, ""))
                                                                     : DownloadURL64;
                    ChangeLogURL  = (rgx_URL.IsMatch(ChangeLogURL)) ? Path.Combine(Path.GetDirectoryName(InstallCastURL), rgx_URL.Replace(ChangeLogURL, ""))
                                                                    : ChangeLogURL;
                }
            }

            // close webrequest
            webResponse.Close();

            // set event args
            args.DownloadURL        = (!string.IsNullOrEmpty(DownloadURL64)) ? DownloadURL64
                                                                             : DownloadURL;
            args.ChangelogURL       = ChangeLogURL;
            args.CurrentVersion     = CurrentVersion;
            args.InstalledVersion   = InstalledVersion;
            args.IsInstallAvailable = CurrentVersion > InstalledVersion;

            // only for test
            /*MessageBox.Show(args.DownloadURL + "\n"
                            + args.ChangelogURL + "\n"
                            + args.CurrentVersion + "\n"
                            + args.InstalledVersion);*/

            return args;
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   FUNCTION

        private static void InitializeInstaller(InstallInfoEventArgs args)
        {
            if (CheckForInstallEvent != null)
            {
                CheckForInstallEvent(args);
            }
            else
            {
                if (args != InstallInfoEventArgs.Empty)
                {
                    if (args.IsInstallAvailable)
                    {
                        if (!IsWinFormsApplication)
                        {
                            Application.EnableVisualStyles();
                        }
                        
                        if (ShowAtOwnThread)
                        {
                            Thread thread = new Thread(new ThreadStart(delegate
                            {
                                StartInstaller(DownloadURL, AppCastURL, TempInstallPath);
                            }));

                            thread.CurrentCulture = thread.CurrentUICulture = CurrentCulture;
                            thread.SetApartmentState(ApartmentState.STA);

                            thread.Start();
                        }
                        else
                        {
                            StartInstaller(DownloadURL, AppCastURL, TempInstallPath);
                        }
                    }
                    else
                    {
                        if (ReportErrors)
                        {
                            MessageBox.Show(Resources.install_UnavailableMessage,
                                            Resources.install_UnavailableCaption,
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Information);
                        }
                    }
                }
                else
                {
                    if (ReportErrors)
                    {
                        MessageBox.Show(Resources.install_CheckFailedMessage,
                                        Resources.install_CheckFailedCaption,
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                    }
                }
            }

            Running = false;
        }
        
        private static void StartInstaller(string updateCast, string appCast, string tempPath)
        {
            // init
            string installer_file_path = Path.Combine(TempInstallPath, "installer.exe");

            // extract installer
            if (!Directory.Exists(TempInstallPath))
            {
                DirectoryInfo di = Directory.CreateDirectory(TempInstallPath);
                di.Attributes    = FileAttributes.Directory | FileAttributes.Hidden;
            }
            else
            {
                DirectoryInfo di = new DirectoryInfo(TempInstallPath);

                if ((di.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
                {
                    di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
                }
            }

            File.WriteAllBytes(installer_file_path, Resources.installer);
            
            // generate new process
            var processStartInfo = new ProcessStartInfo();

            if (File.Exists(installer_file_path))
            {
                processStartInfo.UseShellExecute = true;
                processStartInfo.FileName        = installer_file_path;
                processStartInfo.Arguments       = $"\"{appCast}\" \"{updateCast}\" \"{tempPath}\"";
            }
            else return;

            // start installer
            try
            {
                Process installer = Process.Start(processStartInfo);

                installer.WaitForExit();
                appCast.DeleteDirectory();
            }
            catch (Win32Exception exception)
            {
                if (exception.NativeErrorCode != 1223)
                {
                    throw;
                }
            }
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    }
}
