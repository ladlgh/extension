﻿using System;

namespace Extension.AVM
{
    /// <summary>
    /// Enum representing the remind later time span.
    /// </summary>
    public enum TimeSpanFormat
    {
        Minutes,
        Hours,
        Days
    }
}
