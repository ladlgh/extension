﻿namespace Extension.AVM
{
    partial class frm_Changelog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        /*#region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Text = "frm_Main";
        }

        #endregion*/

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Changelog));
            this.btn_Close = new System.Windows.Forms.Button();
            this.tlp_BACKGROUND = new System.Windows.Forms.TableLayoutPanel();
            this.p_Action = new System.Windows.Forms.Panel();
            this.rtb_Changelog = new System.Windows.Forms.RichTextBox();
            this.tlp_BACKGROUND.SuspendLayout();
            this.p_Action.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Close
            // 
            resources.ApplyResources(this.btn_Close, "btn_Close");
            this.btn_Close.BackColor = System.Drawing.Color.Gainsboro;
            this.btn_Close.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_Close.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.UseVisualStyleBackColor = false;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // tlp_BACKGROUND
            // 
            this.tlp_BACKGROUND.BackColor = System.Drawing.Color.DarkGray;
            resources.ApplyResources(this.tlp_BACKGROUND, "tlp_BACKGROUND");
            this.tlp_BACKGROUND.Controls.Add(this.p_Action, 0, 1);
            this.tlp_BACKGROUND.Controls.Add(this.rtb_Changelog, 0, 0);
            this.tlp_BACKGROUND.Name = "tlp_BACKGROUND";
            // 
            // p_Action
            // 
            this.p_Action.BackColor = System.Drawing.Color.LightGray;
            this.p_Action.Controls.Add(this.btn_Close);
            resources.ApplyResources(this.p_Action, "p_Action");
            this.p_Action.Name = "p_Action";
            // 
            // rtb_Changelog
            // 
            this.rtb_Changelog.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rtb_Changelog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.rtb_Changelog, "rtb_Changelog");
            this.rtb_Changelog.Name = "rtb_Changelog";
            this.rtb_Changelog.ReadOnly = true;
            // 
            // frm_Changelog
            // 
            this.AcceptButton = this.btn_Close;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.Controls.Add(this.tlp_BACKGROUND);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_Changelog";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Closed);
            this.Load += new System.EventHandler(this.Form_Load);
            this.tlp_BACKGROUND.ResumeLayout(false);
            this.p_Action.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.TableLayoutPanel tlp_BACKGROUND;
        private System.Windows.Forms.Panel p_Action;
        private System.Windows.Forms.RichTextBox rtb_Changelog;
    }
}