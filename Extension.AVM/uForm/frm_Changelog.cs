﻿using System;
using System.Windows.Forms;

namespace Extension.AVM
{
    internal partial class frm_Changelog : Form
    {
        #region   INIT
        
        public frm_Changelog()
        {
            InitializeComponent();
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   FORM
        
        private void Form_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Changelog.ChangeLogURL))
            {
                rtb_Changelog.LoadFile(Changelog.ChangeLogURL, RichTextBoxStreamType.PlainText);
            }
        }

        private void Form_Closed(object sender, FormClosedEventArgs e)
        {
            Changelog.Running = false;
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   CONTROLS

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    }
}