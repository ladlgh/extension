﻿using System;
using System.Windows.Forms;
using Extension.Properties;

namespace Extension.AVM
{
    internal partial class frm_RemindLater : Form
    {
        #region   INIT

        public TimeSpanFormat RemindLaterFormat { get; private set; }
        public int RemindLaterAt { get; private set; }

        public frm_RemindLater()
        {
            InitializeComponent();
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   FORM

        private void Form_Load(object sender, EventArgs e)
        {
            DateTime remindLater = Settings.Default.RemindMeLater;
            int compare_DateTime = DateTime.Compare(DateTime.Now, remindLater);
            TimeSpan timespan = (remindLater).Subtract(DateTime.Now);

            cob_RemindLater.SelectedIndex = 0;

            if (timespan.TotalMinutes > 0.0)
            {
                rb_Yes.Checked = true;
                nud_RemindLater.Value = (timespan.Minutes > 0) ? timespan.Minutes : 1;
            }
            else
            {
                rb_No.Checked = true;
            }
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   CONTROLS

        private void rb_Yes_CheckedChanged(object sender, EventArgs e)
        {
            btn_OK.Text = "Remind later";
            nud_RemindLater.Enabled = rb_Yes.Checked;
            cob_RemindLater.Enabled = nud_RemindLater.Enabled;
        }

        private void rb_No_CheckedChanged(object sender, EventArgs e)
        {
            btn_OK.Text = "Update";
            nud_RemindLater.Enabled = rb_Yes.Checked;
            cob_RemindLater.Enabled = nud_RemindLater.Enabled;
        }

        private void btn_Ok_Click(object sender, EventArgs e)
        {
            if (rb_Yes.Checked)
            {
                RemindLaterFormat = (TimeSpanFormat)cob_RemindLater.SelectedIndex;
                RemindLaterAt = (int)nud_RemindLater.Value;

                DialogResult = DialogResult.Yes;
                this.Close();
            }
            else if (rb_No.Checked)
            {
                DialogResult = DialogResult.No;
                this.Close();
            }
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    }
}
