﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Extension.Properties;

namespace Extension.AVM
{
    internal partial class frm_Update : Form
    {
        #region   INIT

        private bool HideReleaseNotes { get; set; }

        public sealed override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
            }
        }

        public frm_Update()
        {
            InitializeComponent();

            btn_Skip.Visible        = AppUpdater.ShowSkipButton;
            btn_RemindLater.Visible = AppUpdater.ShowRemindLaterButton;
            lbl_Update.Text         = string.Format(lbl_Update.Text, AppUpdater.AppTitle);
            lbl_Description.Text    = string.Format(lbl_Description.Text,
                                                    AppUpdater.AppTitle,
                                                    AppUpdater.UpdateVersion,
                                                    AppUpdater.InstalledVersion);

            // only for test
            /*MessageBox.Show(Updater.AppTitle + "\n"
                            + Updater.CurrentVersion + "\n"
                            + Updater.InstalledVersion);*/

            if (string.IsNullOrEmpty(AppUpdater.ChangeLogURL))
            {
                HideReleaseNotes = true;
                var reduceHeight = lbl_ReleaseNotes.Height + rtb_ReleaseNotes.Height;

                lbl_ReleaseNotes.Hide();
                rtb_ReleaseNotes.Hide();

                Height -= reduceHeight;

                btn_Skip.Location        = new Point(btn_Skip.Location.X, btn_Skip.Location.Y - reduceHeight);
                btn_RemindLater.Location = new Point(btn_RemindLater.Location.X, btn_RemindLater.Location.Y - reduceHeight);
                btn_Update.Location      = new Point(btn_Update.Location.X, btn_Update.Location.Y - reduceHeight);
            }
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   FORM

        private void Form_Load(object sender, EventArgs e)
        {
            if (!HideReleaseNotes)
            {
                rtb_ReleaseNotes.LoadFile(AppUpdater.ChangeLogURL, RichTextBoxStreamType.PlainText);
            }
        }

        private void Form_Closed(object sender, FormClosedEventArgs e)
        {
            AppUpdater.Running = false;
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   CONTROLS

        private void Skip_Click(object sender, EventArgs e)
        {
            Settings.Default.AvailableVersion = AppUpdater.UpdateVersion.ToString();
            Settings.Default.SkipUpdate       = true;
            Settings.Default.Save();
        }

        private void RemindLater_Click(object sender, EventArgs e)
        {
            if (AppUpdater.LetUserSelectRemindLater)
            {
                frm_RemindLater remindLaterForm = new frm_RemindLater();
                remindLaterForm.Size            = new Size(this.Size.Width, remindLaterForm.Size.Height);

                DialogResult dialogResult = remindLaterForm.ShowDialog(this);

                if (dialogResult.Equals(DialogResult.Yes))
                {
                    AppUpdater.RemindLaterTimeSpan = remindLaterForm.RemindLaterFormat;
                    AppUpdater.RemindLaterAt       = remindLaterForm.RemindLaterAt;
                }
                else if (dialogResult.Equals(DialogResult.No))
                {
                    DialogResult = DialogResult.OK;
                    this.Close();

                    return;
                }
                else
                {
                    return;
                }
            }
            
            Settings.Default.AvailableVersion = AppUpdater.UpdateVersion.ToString();
            Settings.Default.SkipUpdate       = false;

            DateTime remindLaterDateTime = DateTime.Now;

            switch (AppUpdater.RemindLaterTimeSpan)
            {
                case TimeSpanFormat.Days:
                    {
                        remindLaterDateTime += TimeSpan.FromDays(AppUpdater.RemindLaterAt);
                    }
                    break;

                case TimeSpanFormat.Hours:
                    {
                        remindLaterDateTime += TimeSpan.FromHours(AppUpdater.RemindLaterAt);
                    }
                    break;

                case TimeSpanFormat.Minutes:
                    {
                        remindLaterDateTime += TimeSpan.FromMinutes(AppUpdater.RemindLaterAt);
                    }
                    break;
            }
            
            Settings.Default.RemindMeLater = remindLaterDateTime;
            Settings.Default.Save();

            AppUpdater.tmr_RemindLater.InitializeTimer(remindLaterDateTime).Start();
            
            DialogResult = DialogResult.None;
            this.Close();
        }

        private void Update_Click(object sender, EventArgs e)
        {
            if (AppUpdater.OpenDownloadPage) // open downloadpage in default webbrowser
            {
                Process.Start(new ProcessStartInfo(AppUpdater.DownloadURL));

                DialogResult = DialogResult.Abort;
                this.Close();
            }
            else
            {
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    }
}
