﻿namespace Extension.AVM
{
    partial class frm_Update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        /*#region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Text = "frm_Main";
        }

        #endregion*/

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Update));
            this.lbl_Update = new System.Windows.Forms.Label();
            this.lbl_Description = new System.Windows.Forms.Label();
            this.lbl_ReleaseNotes = new System.Windows.Forms.Label();
            this.btn_Update = new System.Windows.Forms.Button();
            this.btn_RemindLater = new System.Windows.Forms.Button();
            this.pb_Icon = new System.Windows.Forms.PictureBox();
            this.btn_Skip = new System.Windows.Forms.Button();
            this.tlp_BACKGROUNG = new System.Windows.Forms.TableLayoutPanel();
            this.rtb_ReleaseNotes = new System.Windows.Forms.RichTextBox();
            this.p_Action = new System.Windows.Forms.Panel();
            this.p_Icon = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Icon)).BeginInit();
            this.tlp_BACKGROUNG.SuspendLayout();
            this.p_Action.SuspendLayout();
            this.p_Icon.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_Update
            // 
            this.lbl_Update.AutoEllipsis = true;
            this.lbl_Update.BackColor = System.Drawing.Color.LightGray;
            resources.ApplyResources(this.lbl_Update, "lbl_Update");
            this.lbl_Update.Name = "lbl_Update";
            // 
            // lbl_Description
            // 
            this.lbl_Description.BackColor = System.Drawing.Color.LightGray;
            resources.ApplyResources(this.lbl_Description, "lbl_Description");
            this.lbl_Description.Name = "lbl_Description";
            // 
            // lbl_ReleaseNotes
            // 
            this.lbl_ReleaseNotes.BackColor = System.Drawing.Color.Transparent;
            this.tlp_BACKGROUNG.SetColumnSpan(this.lbl_ReleaseNotes, 2);
            resources.ApplyResources(this.lbl_ReleaseNotes, "lbl_ReleaseNotes");
            this.lbl_ReleaseNotes.Name = "lbl_ReleaseNotes";
            // 
            // btn_Update
            // 
            resources.ApplyResources(this.btn_Update, "btn_Update");
            this.btn_Update.BackColor = System.Drawing.Color.Gainsboro;
            this.btn_Update.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue;
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.UseVisualStyleBackColor = false;
            this.btn_Update.Click += new System.EventHandler(this.Update_Click);
            // 
            // btn_RemindLater
            // 
            resources.ApplyResources(this.btn_RemindLater, "btn_RemindLater");
            this.btn_RemindLater.BackColor = System.Drawing.Color.Gainsboro;
            this.btn_RemindLater.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_RemindLater.Name = "btn_RemindLater";
            this.btn_RemindLater.UseVisualStyleBackColor = false;
            this.btn_RemindLater.Click += new System.EventHandler(this.RemindLater_Click);
            // 
            // pb_Icon
            // 
            resources.ApplyResources(this.pb_Icon, "pb_Icon");
            this.pb_Icon.BackColor = System.Drawing.Color.LightGray;
            this.pb_Icon.Image = global::Extension.Properties.Resources.update_32;
            this.pb_Icon.Name = "pb_Icon";
            this.pb_Icon.TabStop = false;
            // 
            // btn_Skip
            // 
            resources.ApplyResources(this.btn_Skip, "btn_Skip");
            this.btn_Skip.BackColor = System.Drawing.Color.Gainsboro;
            this.btn_Skip.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btn_Skip.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Skip.Name = "btn_Skip";
            this.btn_Skip.UseVisualStyleBackColor = false;
            this.btn_Skip.Click += new System.EventHandler(this.Skip_Click);
            // 
            // tlp_BACKGROUNG
            // 
            this.tlp_BACKGROUNG.BackColor = System.Drawing.Color.DarkGray;
            resources.ApplyResources(this.tlp_BACKGROUNG, "tlp_BACKGROUNG");
            this.tlp_BACKGROUNG.Controls.Add(this.rtb_ReleaseNotes, 0, 3);
            this.tlp_BACKGROUNG.Controls.Add(this.lbl_Update, 0, 0);
            this.tlp_BACKGROUNG.Controls.Add(this.lbl_Description, 0, 1);
            this.tlp_BACKGROUNG.Controls.Add(this.lbl_ReleaseNotes, 0, 2);
            this.tlp_BACKGROUNG.Controls.Add(this.p_Action, 0, 4);
            this.tlp_BACKGROUNG.Controls.Add(this.p_Icon, 1, 0);
            this.tlp_BACKGROUNG.Name = "tlp_BACKGROUNG";
            // 
            // rtb_ReleaseNotes
            // 
            this.rtb_ReleaseNotes.BackColor = System.Drawing.Color.WhiteSmoke;
            this.rtb_ReleaseNotes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tlp_BACKGROUNG.SetColumnSpan(this.rtb_ReleaseNotes, 2);
            resources.ApplyResources(this.rtb_ReleaseNotes, "rtb_ReleaseNotes");
            this.rtb_ReleaseNotes.Name = "rtb_ReleaseNotes";
            this.rtb_ReleaseNotes.ReadOnly = true;
            // 
            // p_Action
            // 
            this.p_Action.BackColor = System.Drawing.Color.LightGray;
            this.tlp_BACKGROUNG.SetColumnSpan(this.p_Action, 2);
            this.p_Action.Controls.Add(this.btn_Update);
            this.p_Action.Controls.Add(this.btn_Skip);
            this.p_Action.Controls.Add(this.btn_RemindLater);
            resources.ApplyResources(this.p_Action, "p_Action");
            this.p_Action.Name = "p_Action";
            // 
            // p_Icon
            // 
            this.p_Icon.BackColor = System.Drawing.Color.LightGray;
            this.p_Icon.Controls.Add(this.pb_Icon);
            resources.ApplyResources(this.p_Icon, "p_Icon");
            this.p_Icon.Name = "p_Icon";
            this.tlp_BACKGROUNG.SetRowSpan(this.p_Icon, 2);
            // 
            // frm_Update
            // 
            this.AcceptButton = this.btn_Update;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.Controls.Add(this.tlp_BACKGROUNG);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_Update";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Closed);
            this.Load += new System.EventHandler(this.Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pb_Icon)).EndInit();
            this.tlp_BACKGROUNG.ResumeLayout(false);
            this.p_Action.ResumeLayout(false);
            this.p_Icon.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_RemindLater;
        private System.Windows.Forms.Button btn_Update;
        private System.Windows.Forms.Button btn_Skip;
        private System.Windows.Forms.Label lbl_Update;
        private System.Windows.Forms.Label lbl_Description;
        private System.Windows.Forms.Label lbl_ReleaseNotes;
        private System.Windows.Forms.PictureBox pb_Icon;
        private System.Windows.Forms.TableLayoutPanel tlp_BACKGROUNG;
        private System.Windows.Forms.Panel p_Action;
        private System.Windows.Forms.Panel p_Icon;
        private System.Windows.Forms.RichTextBox rtb_ReleaseNotes;
    }
}