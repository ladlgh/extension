﻿namespace Extension.AVM
{
    partial class frm_RemindLater
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_RemindLater));
            this.pictureBoxIcon = new System.Windows.Forms.PictureBox();
            this.lbl_Title = new System.Windows.Forms.Label();
            this.rb_No = new System.Windows.Forms.RadioButton();
            this.cob_RemindLater = new System.Windows.Forms.ComboBox();
            this.rb_Yes = new System.Windows.Forms.RadioButton();
            this.lbl_Description = new System.Windows.Forms.Label();
            this.btn_OK = new System.Windows.Forms.Button();
            this.nud_RemindLater = new System.Windows.Forms.NumericUpDown();
            this.tlp_BACKGROUND = new System.Windows.Forms.TableLayoutPanel();
            this.p_Action = new System.Windows.Forms.Panel();
            this.p_Icon = new System.Windows.Forms.Panel();
            this.p_Select = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_RemindLater)).BeginInit();
            this.tlp_BACKGROUND.SuspendLayout();
            this.p_Action.SuspendLayout();
            this.p_Icon.SuspendLayout();
            this.p_Select.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBoxIcon
            // 
            resources.ApplyResources(this.pictureBoxIcon, "pictureBoxIcon");
            this.pictureBoxIcon.Image = global::Extension.Properties.Resources.clock_32;
            this.pictureBoxIcon.Name = "pictureBoxIcon";
            this.pictureBoxIcon.TabStop = false;
            // 
            // lbl_Title
            // 
            resources.ApplyResources(this.lbl_Title, "lbl_Title");
            this.lbl_Title.BackColor = System.Drawing.Color.LightGray;
            this.lbl_Title.Name = "lbl_Title";
            // 
            // rb_No
            // 
            resources.ApplyResources(this.rb_No, "rb_No");
            this.rb_No.Checked = true;
            this.rb_No.Name = "rb_No";
            this.rb_No.TabStop = true;
            this.rb_No.UseVisualStyleBackColor = true;
            this.rb_No.CheckedChanged += new System.EventHandler(this.rb_No_CheckedChanged);
            // 
            // cob_RemindLater
            // 
            this.cob_RemindLater.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cob_RemindLater.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cob_RemindLater, "cob_RemindLater");
            this.cob_RemindLater.FormattingEnabled = true;
            this.cob_RemindLater.Items.AddRange(new object[] {
            resources.GetString("cob_RemindLater.Items"),
            resources.GetString("cob_RemindLater.Items1"),
            resources.GetString("cob_RemindLater.Items2")});
            this.cob_RemindLater.Name = "cob_RemindLater";
            // 
            // rb_Yes
            // 
            resources.ApplyResources(this.rb_Yes, "rb_Yes");
            this.rb_Yes.Name = "rb_Yes";
            this.rb_Yes.UseVisualStyleBackColor = true;
            this.rb_Yes.CheckedChanged += new System.EventHandler(this.rb_Yes_CheckedChanged);
            // 
            // lbl_Description
            // 
            resources.ApplyResources(this.lbl_Description, "lbl_Description");
            this.lbl_Description.BackColor = System.Drawing.Color.LightGray;
            this.lbl_Description.Name = "lbl_Description";
            // 
            // btn_OK
            // 
            resources.ApplyResources(this.btn_OK, "btn_OK");
            this.btn_OK.BackColor = System.Drawing.Color.Gainsboro;
            this.btn_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_OK.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.UseVisualStyleBackColor = false;
            this.btn_OK.Click += new System.EventHandler(this.btn_Ok_Click);
            // 
            // nud_RemindLater
            // 
            this.nud_RemindLater.BackColor = System.Drawing.Color.WhiteSmoke;
            this.nud_RemindLater.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.nud_RemindLater, "nud_RemindLater");
            this.nud_RemindLater.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.nud_RemindLater.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nud_RemindLater.Name = "nud_RemindLater";
            this.nud_RemindLater.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tlp_BACKGROUND
            // 
            this.tlp_BACKGROUND.BackColor = System.Drawing.Color.DarkGray;
            resources.ApplyResources(this.tlp_BACKGROUND, "tlp_BACKGROUND");
            this.tlp_BACKGROUND.Controls.Add(this.p_Action, 0, 3);
            this.tlp_BACKGROUND.Controls.Add(this.lbl_Title, 0, 0);
            this.tlp_BACKGROUND.Controls.Add(this.lbl_Description, 0, 1);
            this.tlp_BACKGROUND.Controls.Add(this.p_Icon, 1, 0);
            this.tlp_BACKGROUND.Controls.Add(this.p_Select, 0, 2);
            this.tlp_BACKGROUND.Name = "tlp_BACKGROUND";
            // 
            // p_Action
            // 
            this.p_Action.BackColor = System.Drawing.Color.LightGray;
            this.tlp_BACKGROUND.SetColumnSpan(this.p_Action, 2);
            this.p_Action.Controls.Add(this.btn_OK);
            resources.ApplyResources(this.p_Action, "p_Action");
            this.p_Action.Name = "p_Action";
            // 
            // p_Icon
            // 
            this.p_Icon.BackColor = System.Drawing.Color.LightGray;
            this.p_Icon.Controls.Add(this.pictureBoxIcon);
            resources.ApplyResources(this.p_Icon, "p_Icon");
            this.p_Icon.Name = "p_Icon";
            this.tlp_BACKGROUND.SetRowSpan(this.p_Icon, 2);
            // 
            // p_Select
            // 
            this.p_Select.BackColor = System.Drawing.Color.LightGray;
            this.tlp_BACKGROUND.SetColumnSpan(this.p_Select, 2);
            this.p_Select.Controls.Add(this.rb_Yes);
            this.p_Select.Controls.Add(this.nud_RemindLater);
            this.p_Select.Controls.Add(this.rb_No);
            this.p_Select.Controls.Add(this.cob_RemindLater);
            resources.ApplyResources(this.p_Select, "p_Select");
            this.p_Select.Name = "p_Select";
            // 
            // frm_RemindLater
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.Controls.Add(this.tlp_BACKGROUND);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_RemindLater";
            this.Load += new System.EventHandler(this.Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_RemindLater)).EndInit();
            this.tlp_BACKGROUND.ResumeLayout(false);
            this.tlp_BACKGROUND.PerformLayout();
            this.p_Action.ResumeLayout(false);
            this.p_Icon.ResumeLayout(false);
            this.p_Select.ResumeLayout(false);
            this.p_Select.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBoxIcon;
        private System.Windows.Forms.Label lbl_Title;
        private System.Windows.Forms.RadioButton rb_No;
        private System.Windows.Forms.ComboBox cob_RemindLater;
        private System.Windows.Forms.RadioButton rb_Yes;
        private System.Windows.Forms.Label lbl_Description;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.NumericUpDown nud_RemindLater;
        private System.Windows.Forms.TableLayoutPanel tlp_BACKGROUND;
        private System.Windows.Forms.Panel p_Action;
        private System.Windows.Forms.Panel p_Icon;
        private System.Windows.Forms.Panel p_Select;
    }
}