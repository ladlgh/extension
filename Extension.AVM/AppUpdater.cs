﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Cache;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Drawing;
using System.Text.RegularExpressions;
using sysTimer = System.Timers.Timer;
using Extension.Properties;

namespace Extension.AVM
{
    /// <summary>
    /// Main class that lets you auto update applications by setting some static fields and executing its Start method.
    /// </summary>
    public static class AppUpdater
    {
        #region   INIT

        public static string         RootSubPath              = ".avm";
        public static string         TempUpdatePath;
        public static string         AppTitle;
        public static string         AppCastURL;
        public static string         UpdateCastURL;
        public static bool           OpenDownloadPage;
        public static CultureInfo    CurrentCulture;
        public static bool           ShowAtOwnThread          = true;
        public static bool           ShowSkipButton           = false;
        public static bool           ShowRemindLaterButton    = true;
        public static bool           LetUserSelectRemindLater = true;
        public static int            RemindLaterAt            = 2;
        public static bool           ReportErrors             = true;
        public static sysTimer       tmr_RemindLater;
        public static TimeSpanFormat RemindLaterTimeSpan      = TimeSpanFormat.Days;
        public static int            DialogHeight             = int.MinValue;
        public static int            DialogWidth              = int.MinValue;
        
        public delegate void CheckForUpdateEventHandler(UpdateInfoEventArgs args);
        public static event  CheckForUpdateEventHandler CheckForUpdateEvent;

        private static frm_Update frm_update;

        internal static string  ChangeLogURL;
        internal static string  DownloadURL;
        internal static string  DownloadURL64;
        internal static bool    Mandatory;
        internal static bool    SilenceUpdate;
        internal static Version UpdateVersion;
        internal static Version InstalledVersion;
        internal static bool    IsWinFormsApplication;
        internal static bool    Running;

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   EXECUTION

        /// <summary>
        /// Start checking for new version of application and display dialog to the user if update is available.
        /// </summary>
        /// <param name="myAssembly">Assembly to use for version checking.</param>
        public static void Start(bool IsSilence = false, Assembly myAssembly = null)
        {
            if (UpdateCastURL != null)
            {
                if (AppCastURL != null)
                {
                    Start(UpdateCastURL, AppCastURL, IsSilence, myAssembly);
                }
                else
                {
                    Start(UpdateCastURL, IsSilence, myAssembly);
                }
            }
            else
            {
                MessageBox.Show("Please set all needed variables!\n"
                                + "(e.g. UpdateCastURL)",
                                "Warning...",
                                MessageBoxButtons.OK, 
                                MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Start checking for new version of application and display dialog to the user if update is available.
        /// </summary>
        /// <param name="updateCast">URL of the xml file that contains information about latest version of the application.</param>
        /// <param name="myAssembly">Assembly to use for version checking.</param>
        public static void Start(string updateCast, bool IsSilence = false, Assembly myAssembly = null)
        {
            Start(updateCast, Path.GetDirectoryName(Application.ExecutablePath), IsSilence, myAssembly);
        }

        /// <summary>
        /// Start checking for new version of application and display dialog to the user if update is available.
        /// </summary>
        /// <param name="updateCast">URL of the xml file that contains information about latest version of the application.</param>
        /// <param name="appCast">URL of the current application.</param>
        /// <param name="myAssembly">Assembly to use for version checking.</param>
        public static void Start(string updateCast, string appCast, bool IsSilence = false, Assembly myAssembly = null)
        {
            // init
            SilenceUpdate = IsSilence;
            TempUpdatePath = Path.Combine(Path.Combine(appCast, RootSubPath), ".temp");

            // check reminder timer
            if (tmr_RemindLater != null)
            {
                tmr_RemindLater.Stop();
                tmr_RemindLater = null;
            }

            if (!Running)
            {
                Running = true;

                // check URLs
                if (!Directory.Exists(appCast))
                {
                    Directory.CreateDirectory(appCast);
                }

                if (Directory.Exists(TempUpdatePath))
                {
                    Directory.Delete(TempUpdatePath, true);
                }

                CurrentCulture = CurrentCulture ?? CultureInfo.CurrentCulture;
                UpdateCastURL  = UpdateCastURL ?? updateCast;

                IsWinFormsApplication = Application.MessageLoop;

                InitializeUpdater(LoadXML(myAssembly ?? Assembly.GetEntryAssembly()));
            }
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   EVENTARGS

        private static UpdateInfoEventArgs LoadXML(Assembly myAssembly)
        {
            UpdateInfoEventArgs args = new UpdateInfoEventArgs();

            Regex rgx_URL = new Regex(@"^[.]{1,3}\\");

            Assembly mainAssembly = myAssembly;
            AssemblyCompanyAttribute companyAttribute = (AssemblyCompanyAttribute)mainAssembly.GetAttribute(typeof(AssemblyCompanyAttribute));

            if (string.IsNullOrEmpty(AppTitle))
            {
                AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)mainAssembly.GetAttribute(typeof(AssemblyTitleAttribute));
                AppTitle = (titleAttribute != null) ? titleAttribute.Title : mainAssembly.GetName().Name;
            }

            string appCompany = (companyAttribute != null) ? companyAttribute.Company : "";

            // check installed version
            InstalledVersion = mainAssembly.GetName().Version;

            // open webrequest
            WebRequest webRequest = WebRequest.Create(UpdateCastURL);
            webRequest.CachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore);
            WebResponse webResponse;

            try
            {
                webResponse = webRequest.GetResponse();
            }
            catch (Exception)
            {
                return UpdateInfoEventArgs.Empty;
            }

            // load xml file
            XmlDocument document;

            using (Stream appCastStream = webResponse.GetResponseStream())
            {
                document = new XmlDocument();

                if (appCastStream != null)
                {
                    document.Load(appCastStream);
                }
                else
                {
                    webResponse.Close();

                    return UpdateInfoEventArgs.Empty;
                }
            }

            // check xml file items
            XmlNodeList update = document.SelectNodes("update");

            if (update != null)
            {
                foreach (XmlNode node in update)
                {
                    XmlNode version     = node.SelectSingleNode("version");
                    XmlNode url         = node.SelectSingleNode("url");
                    XmlNode url64       = node.SelectSingleNode("url64");
                    XmlNode changelog   = node.SelectSingleNode("changelog");
                    XmlNode mandatory   = node.SelectSingleNode("mandatory");
                    XmlNode silence     = node.SelectSingleNode("silence");

                    if (version != null)
                    {
                        UpdateVersion = new Version(version.InnerText);

                        if (UpdateVersion == null)
                        {
                            webResponse.Close();

                            return UpdateInfoEventArgs.Empty;
                        }
                    }
                    else
                    {
                        continue;
                    }
                    
                    DownloadURL   = webResponse.ResponseUri.GetURL(url);
                    DownloadURL64 = (IntPtr.Size.Equals(8)) ? webResponse.ResponseUri.GetURL(url64) : string.Empty;
                    ChangeLogURL  = webResponse.ResponseUri.GetURL(changelog);

                    DownloadURL   = (rgx_URL.IsMatch(DownloadURL)) ? Path.Combine(Path.GetDirectoryName(UpdateCastURL), rgx_URL.Replace(DownloadURL, ""))
                                                                   : DownloadURL;
                    DownloadURL64 = (rgx_URL.IsMatch(DownloadURL64)) ? Path.Combine(Path.GetDirectoryName(UpdateCastURL), rgx_URL.Replace(DownloadURL64, ""))
                                                                     : DownloadURL64;
                    ChangeLogURL  = (rgx_URL.IsMatch(ChangeLogURL)) ? Path.Combine(Path.GetDirectoryName(UpdateCastURL), rgx_URL.Replace(ChangeLogURL, ""))
                                                                    : ChangeLogURL;
                    
                    Mandatory             = (mandatory != null) ? bool.Parse(mandatory.InnerText) : false;
                    ShowRemindLaterButton = (Mandatory) ? false : ShowRemindLaterButton;
                    ShowSkipButton        = (Mandatory) ? false : ShowSkipButton;

                    SilenceUpdate = (!SilenceUpdate && silence != null) ? bool.Parse(silence.InnerText) : SilenceUpdate;
                }
            }

            // close webrequest
            webResponse.Close();

            // check update priority / mandatory
            if (!Mandatory)
            {
                bool skip            = Settings.Default.SkipUpdate;
                Version skipVersion  = new Version(Settings.Default.AvailableVersion);
                DateTime remindLater = (tmr_RemindLater != null) ? Settings.Default.RemindMeLater : DateTime.Now;
                int compare_DateTime = DateTime.Compare(DateTime.Now, remindLater);

                if (skip
                    && UpdateVersion <= skipVersion)
                {
                    args.IsUpdateAvailable = false;

                    return args;
                }

                if (UpdateVersion > skipVersion)
                {
                    Settings.Default.AvailableVersion = UpdateVersion.ToString();
                    Settings.Default.SkipUpdate = false;
                    Settings.Default.Save();
                }

                if (compare_DateTime < 0)
                {
                    tmr_RemindLater.InitializeTimer(remindLater);

                    return UpdateInfoEventArgs.Empty;
                }
            }

            // set event args
            args.DownloadURL        = (!string.IsNullOrEmpty(DownloadURL64)) ? DownloadURL64
                                                                             : DownloadURL;
            args.ChangelogURL       = ChangeLogURL;
            args.CurrentVersion     = UpdateVersion;
            args.InstalledVersion   = InstalledVersion;
            args.IsUpdateAvailable  = UpdateVersion > InstalledVersion;
            args.IsSilenceUpdate    = SilenceUpdate;
            
            return args;
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   FUNCTION

        internal static sysTimer InitializeTimer(this sysTimer timer, DateTime remindLater)
        {
            TimeSpan timeSpan = remindLater - DateTime.Now;

            timer = new sysTimer();
            timer.Interval = (int)timeSpan.TotalMilliseconds;

            timer.Elapsed += delegate
            {
                timer.Stop();
                timer = null;
                Start();
            };

            return timer;
        }

        private static void InitializeUpdater(UpdateInfoEventArgs args)
        {
            if (CheckForUpdateEvent != null)
            {
                CheckForUpdateEvent(args);
            }
            else
            {
                if (args != UpdateInfoEventArgs.Empty)
                {
                    if (args.IsUpdateAvailable)
                    {
                        if (!IsWinFormsApplication)
                        {
                            Application.EnableVisualStyles();
                        }

                        if (args.IsSilenceUpdate)
                        {
                            if (ShowAtOwnThread)
                            {
                                Thread thread = new Thread(new ThreadStart(delegate
                                {
                                    StartUpdater();
                                }));
                                
                                thread.CurrentCulture = thread.CurrentUICulture = CurrentCulture;
                                thread.SetApartmentState(ApartmentState.STA);

                                thread.Start();
                                thread.Join();
                            }
                            else
                            {
                                StartUpdater();
                            }
                        }
                        else
                        {
                            DialogResult dialogresult;
                            
                            if (ShowAtOwnThread)
                            {
                                Thread thread = new Thread(new ThreadStart(delegate
                                {
                                    using (frm_update = new frm_Update())
                                    {
                                        DialogHeight = (DialogHeight == int.MinValue) ? frm_update.Size.Height : DialogHeight;
                                        DialogWidth  = (DialogWidth  == int.MinValue) ? frm_update.Size.Width  : DialogWidth;

                                        frm_update.Size = new Size(DialogWidth, DialogHeight);

                                        dialogresult = frm_update.ShowDialog();

                                        if (dialogresult == DialogResult.OK)
                                        {
                                            StartUpdater();
                                        }
                                        else if (dialogresult == DialogResult.Abort)
                                        {
                                            Extension.CloseApplication();
                                        }
                                    }
                                }));

                                thread.CurrentCulture = thread.CurrentUICulture = CurrentCulture;
                                thread.SetApartmentState(ApartmentState.STA);

                                thread.Start();
                                thread.Join();
                            }
                            else
                            {
                                using (frm_update = new frm_Update())
                                {
                                    DialogHeight = (DialogHeight == int.MinValue) ? frm_update.Size.Height : DialogHeight;
                                    DialogWidth  = (DialogWidth  == int.MinValue) ? frm_update.Size.Width  : DialogWidth;

                                    frm_update.Size = new Size(DialogWidth, DialogHeight);

                                    dialogresult = frm_update.ShowDialog();

                                    if (dialogresult == DialogResult.OK)
                                    {
                                        StartUpdater();
                                    }
                                    else if (dialogresult == DialogResult.Abort)
                                    {
                                        Extension.CloseApplication();
                                    }
                                }
                            }
                        }

                        return;
                    }
                    else
                    {
                        if (ReportErrors
                            && !SilenceUpdate)
                        {
                            MessageBox.Show(Resources.update_UnavailableMessage,
                                            Resources.update_UnavailableCaption,
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Information);
                        }
                    }
                }
                else
                {
                    if (ReportErrors
                        && !SilenceUpdate)
                    {
                        MessageBox.Show(Resources.update_CheckFailedMessage,
                                        Resources.update_CheckFailedCaption,
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                    }
                }
            }

            Running = false;
        }

        private static void StartUpdater()
        {
            // init
            string temp_update_path = TempUpdatePath;
            string updater_file_path = Path.Combine(temp_update_path, "updater.exe");

            // check path
            if (!Directory.Exists(temp_update_path))
            {
                DirectoryInfo di = Directory.CreateDirectory(temp_update_path);
                di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
            }
            else
            {
                DirectoryInfo di = new DirectoryInfo(temp_update_path);

                if ((di.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
                {
                    di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
                }
            }

            // extract updater
            File.WriteAllBytes(updater_file_path, Resources.updater);

            // generate new process
            ProcessStartInfo processStartInfo = new ProcessStartInfo();

            if (File.Exists(updater_file_path))
            {
                processStartInfo.UseShellExecute = true;
                processStartInfo.FileName = updater_file_path;
                processStartInfo.Arguments = $"\"{Application.ExecutablePath}\" \"{DownloadURL}\" \"{temp_update_path}\" \"{SilenceUpdate.ToString()}\"";
            }
            else
            {
                return;
            }

            // start updater process
            try
            {
                Process.Start(processStartInfo);
            }
            catch (Win32Exception exception)
            {
                if (exception.NativeErrorCode != 1223)
                {
                    throw;
                }
            }

            Extension.CloseApplication();
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    }
}
