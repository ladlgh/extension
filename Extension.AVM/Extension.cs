﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;

namespace Extension.AVM
{
    public static class Extension
    {
        public static Attribute GetAttribute(this Assembly assembly,
                                             Type attributeType)
        {
            object[] attributes = assembly.GetCustomAttributes(attributeType, false);

            if (attributes.Length == 0)
            {
                return null;
            }

            return (Attribute)attributes[0];
        }

        public static string GetURL(this Uri baseUri,
                                    XmlNode xmlNode)
        {
            string result = xmlNode?.InnerText ?? "";

            if (!string.IsNullOrEmpty(result)
                && Uri.IsWellFormedUriString(result, UriKind.Relative))
            {
                Uri uri = new Uri(baseUri, result);

                if (uri.IsAbsoluteUri)
                {
                    result = uri.AbsoluteUri;
                }
            }

            return result;
        }
        
        public static void DeleteDirectory(this string directory)
        {
            try
            {
                if (directory != null
                    && Directory.Exists(directory))
                {
                    Directory.Delete(directory, true);
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:\n"
                                + ex.ToString(),
                                "Error...",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        public static void CloseApplication()
        {
            /*Process currentProcess = Process.GetCurrentProcess();
            
            foreach (Process process in Process.GetProcessesByName(currentProcess.ProcessName))
            {
                if (process.Id != currentProcess.Id)
                {
                    process.Kill();
                }
            }*/

            if (Application.MessageLoop)
            {
                Application.ApplicationExit += new EventHandler((s, e) =>
                {
                    Process currentProcess = Process.GetCurrentProcess();

                    foreach (Process process in Process.GetProcessesByName(currentProcess.ProcessName))
                    {
                        if (process.Id != currentProcess.Id)
                        {
                            process.Kill();
                        }
                    }
                });

                Application.Exit();
            }
            else
            {
                Environment.Exit(0);
            }
        }
    }
    /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

    // Extension.AVM.psp_AVM
    public sealed class psp_AVM : PortableSettingsProvider
    {
        public override string  ClassName           { get { return "AVMSettingsProvider";   } }
        public override string  RootSubPath         { get { return AppUpdater.RootSubPath;  } }
        public override bool    RootSubPathIsHidden { get { return true;                    } }
        public override string  FileName            { get { return "avm";                   } }
        public override string  XmlRootNodeName     { get { return "AVMSettings";           } }
    }
    /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
}
