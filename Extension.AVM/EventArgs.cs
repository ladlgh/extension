﻿using System;

namespace Extension.AVM
{
    /// <summary>
    /// Object of this class gives you all the details about the update useful in handling the update logic yourself.
    /// </summary>
    public class UpdateInfoEventArgs : EventArgs
    {
        /// <summary>
        /// Value EMPTY
        /// </summary>
        public static readonly new UpdateInfoEventArgs Empty = new EmptyArgs();
        private class EmptyArgs : UpdateInfoEventArgs { }

        /// <summary>
        /// If new update is available then returns true otherwise false.
        /// </summary>
        public bool IsUpdateAvailable { get; set; }

        /// <summary>
        /// If silence update needed then returns true otherwise false.
        /// </summary>
        public bool IsSilenceUpdate { get; set; }

        /// <summary>
        ///     Download URL of the update file.
        /// </summary>
        public string DownloadURL { get; set; }

        /// <summary>
        /// URL of the webpage specifying changes in the new update.
        /// </summary>
        public string ChangelogURL { get; set; }

        /// <summary>
        /// Returns newest version of the application available to download.
        /// </summary>
        public Version CurrentVersion { get; set; }

        /// <summary>
        /// Returns version of the application currently installed on the user's PC.
        /// </summary>
        public Version InstalledVersion { get; set; }
    }

    /// <summary>
    /// Object of this class gives you all the details about the update useful in handling the update logic yourself.
    /// </summary>
    public class InstallInfoEventArgs : EventArgs
    {
        /// <summary>
        /// Value EMPTY
        /// </summary>
        public static readonly new InstallInfoEventArgs Empty = new EmptyArgs();
        private class EmptyArgs : InstallInfoEventArgs { }

        /// <summary>
        /// If new update is available then returns true otherwise false.
        /// </summary>
        public bool IsInstallAvailable { get; set; }

        /// <summary>
        ///     Download URL of the update file.
        /// </summary>
        public string DownloadURL { get; set; }

        /// <summary>
        /// URL of the webpage specifying changes in the new update.
        /// </summary>
        public string ChangelogURL { get; set; }

        /// <summary>
        /// Returns newest version of the application available to download.
        /// </summary>
        public Version CurrentVersion { get; set; }

        /// <summary>
        /// Returns version of the application currently installed on the user's PC.
        /// </summary>
        public Version InstalledVersion { get; set; }
    }

    /// <summary>
    /// Object of this class gives you all the details about the changelog useful in handling the changelog logic yourself.
    /// </summary>
    public class ChangelogInfoEventArgs : EventArgs
    {
        /// <summary>
        /// Value EMPTY
        /// </summary>
        public static readonly new ChangelogInfoEventArgs Empty = new EmptyArgs();
        private class EmptyArgs : ChangelogInfoEventArgs { }

        /// <summary>
        /// If new changelog file is available then returns true otherwise false.
        /// </summary>
        public bool IsChangelogAvailable { get; set; }

        /// <summary>
        ///     URL of the webpage specifying changes in the new update.
        /// </summary>
        public string ChangelogURL { get; set; }
    }
}
