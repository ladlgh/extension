﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Cache;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace Extension.AVM
{
    /// <summary>
    /// Class that lets you show changelog for applications by setting update url static fields and executing its Start method.
    /// </summary>
    public static class Changelog
    {
        #region   INIT

        public static string      AppCastURL;
        public static bool        ReportErrors    = true;
        public static CultureInfo CurrentCulture;
        public static bool        ShowAtOwnThread = true;
        public static int         DialogHeight    = int.MinValue;
        public static int         DialogWidth     = int.MinValue;

        internal static string  ChangeLogURL;
        internal static bool    IsWinFormsApplication;
        internal static bool    Running;

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   EXECUTION

        /// <summary>
        /// Start checking for new version of application and display dialog to the user if update is available.
        /// </summary>
        /// <param name="myAssembly">Assembly to use for version checking.</param>
        public static void Show(Assembly myAssembly = null)
        {
            Show(AppCastURL, myAssembly);
        }

        /// <summary>
        /// Start checking for new version of application and display dialog to the user if update is available.
        /// </summary>
        /// <param name="appCast">URL of the xml file that contains information about latest version of the application.</param>
        /// <param name="myAssembly">Assembly to use for version checking.</param>
        public static void Show(string appCast, Assembly myAssembly = null)
        {
            if (!Running)
            {
                Running = true;

                CurrentCulture        = (CurrentCulture == null) ? CultureInfo.CurrentCulture : CurrentCulture;
                AppCastURL            = appCast;
                IsWinFormsApplication = Application.MessageLoop;

                BackgroundWorker bgw_Worker   = new BackgroundWorker();
                bgw_Worker.DoWork             += bgw_Worker_DoWork;
                bgw_Worker.RunWorkerCompleted += bgw_Worker_OnRunWorkerCompleted;

                bgw_Worker.RunWorkerAsync(myAssembly ?? Assembly.GetEntryAssembly());
            }
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

        #region   EVENTARGS

        private static void bgw_Worker_OnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs runWorkerCompletedEventArgs)
        {
            if (!runWorkerCompletedEventArgs.Cancelled)
            {
                ChangelogInfoEventArgs args = (ChangelogInfoEventArgs)runWorkerCompletedEventArgs.Result;

                if (args != null)
                {
                    if (args.IsChangelogAvailable)
                    {
                        if (!IsWinFormsApplication)
                        {
                            Application.EnableVisualStyles();
                        }

                        frm_Changelog changelogForm;

                        if (ShowAtOwnThread)
                        {
                            Thread thread = new Thread(new ThreadStart(delegate
                            {
                                changelogForm      = new frm_Changelog();
                                DialogHeight       = (DialogHeight == int.MinValue) ? changelogForm.Size.Height : DialogHeight;
                                DialogWidth        = (DialogWidth == int.MinValue) ? changelogForm.Size.Width : DialogWidth;
                                changelogForm.Size = new Size(DialogWidth, DialogHeight);

                                changelogForm.ShowDialog();
                            }));

                            thread.CurrentCulture = thread.CurrentUICulture = CurrentCulture;
                            thread.SetApartmentState(ApartmentState.STA);

                            thread.Start();
                        }
                        else
                        {
                            changelogForm      = new frm_Changelog();
                            DialogHeight       = (DialogHeight == int.MinValue) ? changelogForm.Size.Height : DialogHeight;
                            DialogWidth        = (DialogWidth == int.MinValue) ? changelogForm.Size.Width : DialogWidth;
                            changelogForm.Size = new Size(DialogWidth, DialogHeight);

                            changelogForm.ShowDialog();
                        }

                        return;
                    }
                    else
                    {
                        if (ReportErrors)
                        {
                            MessageBox.Show("Changelog file is not exists!",
                                            "Warning...",
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Warning);
                        }
                    }
                }
            }

            Running = false;
        }

        private static void bgw_Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Cancel = true;

            Regex rgx_URL = new Regex(@"^[.]{1,3}\\");

            ChangelogInfoEventArgs args = new ChangelogInfoEventArgs();
            WebRequest webRequest       = WebRequest.Create(AppCastURL);
            webRequest.CachePolicy      = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore);

            WebResponse webResponse;

            try
            {
                webResponse = webRequest.GetResponse();
            }
            catch (Exception)
            {
                e.Cancel = false;

                return;
            }

            XmlDocument receivedAppCastDocument;

            using (Stream appCastStream = webResponse.GetResponseStream())
            {
                receivedAppCastDocument = new XmlDocument();

                if (appCastStream != null)
                {
                    receivedAppCastDocument.Load(appCastStream);
                }
                else
                {
                    e.Cancel = false;
                    webResponse.Close();

                    return;
                }
            }

            XmlNodeList update = receivedAppCastDocument.SelectNodes("update");

            if (update != null)
            {
                foreach (XmlNode node in update)
                {
                    ChangeLogURL = webResponse.ResponseUri.GetURL(node.SelectSingleNode("changelog"));
                }
                
                ChangeLogURL = (rgx_URL.IsMatch(ChangeLogURL)) ? Path.Combine(Path.GetDirectoryName(AppCastURL), rgx_URL.Replace(ChangeLogURL, ""))
                                                               : ChangeLogURL;
            }

            webResponse.Close();

            args.IsChangelogAvailable = File.Exists(ChangeLogURL);
            args.ChangelogURL         = ChangeLogURL;

            e.Cancel = false;
            e.Result = args;
        }

        #endregion
        /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    }
}
