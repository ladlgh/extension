﻿using System;

namespace Extension.BCG
{
    internal struct C40TextState
    {
        #region Properties

        internal int Shift { get; set; }

        internal bool UpperShift { get; set; }

        #endregion
    }

    internal struct DmtxBestLine
    {
        internal int Angle { get; set; }
        internal int HOffset { get; set; }
        internal int Mag { get; set; }
        internal int StepBeg { get; set; }
        internal int StepPos { get; set; }
        internal int StepNeg { get; set; }
        internal int DistSq { get; set; }
        internal double Devn { get; set; }
        internal DmtxPixelLoc LocBeg { get; set; }
        internal DmtxPixelLoc LocPos { get; set; }
        internal DmtxPixelLoc LocNeg { get; set; }
    }

    internal struct DmtxBresLine
    {
        #region Fields

        int _xStep;
        int _yStep;
        int _xDelta;
        int _yDelta;
        bool _steep;
        int _xOut;
        int _yOut;
        int _travel;
        int _outward;
        int _error;
        DmtxPixelLoc _loc;
        DmtxPixelLoc _loc0;
        DmtxPixelLoc _loc1;

        #endregion

        #region Constructors

        internal DmtxBresLine(DmtxBresLine orig)
        {
            this._error = orig._error;
            this._loc = new DmtxPixelLoc { X = orig._loc.X, Y = orig._loc.Y };
            this._loc0 = new DmtxPixelLoc { X = orig._loc0.X, Y = orig._loc0.Y };
            this._loc1 = new DmtxPixelLoc { X = orig._loc1.X, Y = orig._loc1.Y };
            this._outward = orig._outward;
            this._steep = orig._steep;
            this._travel = orig._travel;
            this._xDelta = orig._xDelta;
            this._xOut = orig._xOut;
            this._xStep = orig._xStep;
            this._yDelta = orig._yDelta;
            this._yOut = orig._yOut;
            this._yStep = orig._yStep;
        }

        internal DmtxBresLine(DmtxPixelLoc loc0, DmtxPixelLoc loc1, DmtxPixelLoc locInside)
        {
            int cp;
            DmtxPixelLoc locBeg, locEnd;


            /* Values that stay the same after initialization */
            this._loc0 = loc0;
            this._loc1 = loc1;
            this._xStep = (loc0.X < loc1.X) ? +1 : -1;
            this._yStep = (loc0.Y < loc1.Y) ? +1 : -1;
            this._xDelta = Math.Abs(loc1.X - loc0.X);
            this._yDelta = Math.Abs(loc1.Y - loc0.Y);
            this._steep = (this._yDelta > this._xDelta);

            /* Take cross product to determine outward step */
            if (this._steep)
            {
                /* Point first vector up to get correct sign */
                if (loc0.Y < loc1.Y)
                {
                    locBeg = loc0;
                    locEnd = loc1;
                }
                else
                {
                    locBeg = loc1;
                    locEnd = loc0;
                }
                cp = (((locEnd.X - locBeg.X) * (locInside.Y - locEnd.Y)) -
                      ((locEnd.Y - locBeg.Y) * (locInside.X - locEnd.X)));

                this._xOut = (cp > 0) ? +1 : -1;
                this._yOut = 0;
            }
            else
            {
                /* Point first vector left to get correct sign */
                if (loc0.X > loc1.X)
                {
                    locBeg = loc0;
                    locEnd = loc1;
                }
                else
                {
                    locBeg = loc1;
                    locEnd = loc0;
                }
                cp = (((locEnd.X - locBeg.X) * (locInside.Y - locEnd.Y)) -
                      ((locEnd.Y - locBeg.Y) * (locInside.X - locEnd.X)));

                this._xOut = 0;
                this._yOut = (cp > 0) ? +1 : -1;
            }

            /* Values that change while stepping through line */
            this._loc = loc0;
            this._travel = 0;
            this._outward = 0;
            this._error = (this._steep) ? this._yDelta / 2 : this._xDelta / 2;
        }

        #endregion

        #region Methods

        internal bool GetStep(DmtxPixelLoc target, ref int travel, ref int outward)
        {
            /* Determine necessary step along and outward from Bresenham line */
            if (this._steep)
            {
                travel = (this._yStep > 0) ? target.Y - this._loc.Y : this._loc.Y - target.Y;
                Step(travel, 0);
                outward = (this._xOut > 0) ? target.X - this._loc.X : this._loc.X - target.X;
                if (this._yOut != 0)
                {
                    throw new Exception("Invald yOut value for bresline step!");
                }
            }
            else
            {
                travel = (this._xStep > 0) ? target.X - this._loc.X : this._loc.X - target.X;
                Step(travel, 0);
                outward = (this._yOut > 0) ? target.Y - this._loc.Y : this._loc.Y - target.Y;
                if (this._xOut != 0)
                {
                    throw new Exception("Invald xOut value for bresline step!");
                }
            }

            return true;
        }

        internal bool Step(int travel, int outward)
        {
            int i;

            if (Math.Abs(travel) >= 2)
            {
                throw new ArgumentException("Invalid value for 'travel' in BaseLineStep!");
            }

            /* Perform forward step */
            if (travel > 0)
            {
                this._travel++;
                if (this._steep)
                {
                    this._loc = new DmtxPixelLoc() { X = this._loc.X, Y = this._loc.Y + this._yStep };
                    this._error -= this._xDelta;
                    if (this._error < 0)
                    {
                        this._loc = new DmtxPixelLoc() { X = this._loc.X + this._xStep, Y = this._loc.Y };
                        this._error += this._yDelta;
                    }
                }
                else
                {
                    this._loc = new DmtxPixelLoc() { X = this._loc.X + this._xStep, Y = this._loc.Y };
                    this._error -= this._yDelta;
                    if (this._error < 0)
                    {
                        this._loc = new DmtxPixelLoc() { X = this._loc.X, Y = this._loc.Y + this._yStep };
                        this._error += this._xDelta;
                    }
                }
            }
            else if (travel < 0)
            {
                this._travel--;
                if (this._steep)
                {
                    this._loc = new DmtxPixelLoc() { X = this._loc.X, Y = this._loc.Y - this._yStep };
                    this._error += this._xDelta;
                    if (this.Error >= this.YDelta)
                    {
                        this._loc = new DmtxPixelLoc() { X = this._loc.X - this._xStep, Y = this._loc.Y };
                        this._error -= this._yDelta;
                    }
                }
                else
                {
                    this._loc = new DmtxPixelLoc() { X = this._loc.X - this._xStep, Y = this._loc.Y };
                    this._error += this._yDelta;
                    if (this._error >= this._xDelta)
                    {
                        this._loc = new DmtxPixelLoc() { X = this._loc.X, Y = this._loc.Y - this._yStep };
                        this._error -= this._xDelta;
                    }
                }
            }

            for (i = 0; i < outward; i++)
            {
                /* Outward steps */
                this._outward++;
                this._loc = new DmtxPixelLoc() { X = this._loc.X + this._xOut, Y = this._loc.Y + this._yOut };
            }

            return true;
        }

        #endregion

        #region Properties

        internal int XStep
        {
            get { return _xStep; }
            set { _xStep = value; }
        }

        internal int YStep
        {
            get { return _yStep; }
            set { _yStep = value; }
        }

        internal int XDelta
        {
            get { return _xDelta; }
            set { _xDelta = value; }
        }

        internal int YDelta
        {
            get { return _yDelta; }
            set { _yDelta = value; }
        }

        internal bool Steep
        {
            get { return _steep; }
            set { _steep = value; }
        }

        internal int XOut
        {
            get { return _xOut; }
            set { _xOut = value; }
        }

        internal int YOut
        {
            get { return _yOut; }
            set { _yOut = value; }
        }

        internal int Travel
        {
            get { return _travel; }
            set { _travel = value; }
        }

        internal int Outward
        {
            get { return _outward; }
            set { _outward = value; }
        }

        internal int Error
        {
            get { return _error; }
            set { _error = value; }
        }

        internal DmtxPixelLoc Loc
        {
            get { return _loc; }
            set { _loc = value; }
        }

        internal DmtxPixelLoc Loc0
        {
            get { return _loc0; }
            set { _loc0 = value; }
        }

        internal DmtxPixelLoc Loc1
        {
            get { return _loc1; }
            set { _loc1 = value; }
        }

        #endregion
    }

    internal struct DmtxFollow
    {
        #region Fields

        int _ptrIndex;
        #endregion

        #region Properties
        internal int PtrIndex
        {
            set
            {
                _ptrIndex = value;
            }
        }

        internal byte CurrentPtr
        {
            get
            {
                return this.Ptr[_ptrIndex];
            }
            set
            {
                this.Ptr[_ptrIndex] = value;
            }
        }

        internal byte[] Ptr { get; set; }

        internal byte Neighbor
        {
            get
            {
                return this.Ptr[_ptrIndex];
            }
            set
            {
                this.Ptr[_ptrIndex] = value;
            }
        }

        internal int Step { get; set; }

        internal DmtxPixelLoc Loc { get; set; }

        #endregion
    }

    internal struct DmtxPixelLoc
    {
        #region Properties

        internal int X { get; set; }

        internal int Y { get; set; }

        #endregion
    }

    internal struct DmtxQuadruplet
    {
        byte[] _value;

        internal byte[] Value
        {
            get { return this._value ?? (this._value = new byte[4]); }
        }
    }

    internal struct DmtxScanGrid
    {
        #region Fields

        int _minExtent;
        int _maxExtent;
        int _xOffset;
        int _yOffset;
        int _xMin;
        int _xMax;
        int _yMin;
        int _yMax;

        int _total;
        int _extent;
        int _jumpSize;
        int _pixelTotal;
        int _startPos;

        int _pixelCount;
        int _xCenter;
        int _yCenter;

        #endregion

        #region Constructors

        internal DmtxScanGrid(DmtxDecode dec)
        {
            int smallestFeature = dec.ScanGap;
            this._xMin = dec.XMin;
            this._xMax = dec.XMax;
            this._yMin = dec.YMin;
            this._yMax = dec.YMax;

            /* Values that get set once */
            int xExtent = this._xMax - this._xMin;
            int yExtent = this._yMax - this._yMin;
            int maxExtent = (xExtent > yExtent) ? xExtent : yExtent;

            if (maxExtent < 1)
            {
                throw new ArgumentException("Invalid max extent for Scan Grid: Must be greater than 0");
            }

            int extent = 1;
            this._minExtent = extent;
            for (; extent < maxExtent; extent = ((extent + 1) * 2) - 1)
            {
                if (extent <= smallestFeature)
                {
                    _minExtent = extent;
                }
            }

            this._maxExtent = extent;

            this._xOffset = (this._xMin + this._xMax - this._maxExtent) / 2;
            this._yOffset = (this._yMin + this._yMax - this._maxExtent) / 2;

            /* Values that get reset for every level */
            this._total = 1;
            this._extent = this._maxExtent;

            this._jumpSize = this._extent + 1;
            this._pixelTotal = 2 * this._extent - 1;
            this._startPos = this._extent / 2;
            this._pixelCount = 0;
            this._xCenter = this._yCenter = this._startPos;

            SetDerivedFields();
        }

        #endregion

        #region Methods

        internal DmtxRange PopGridLocation(ref DmtxPixelLoc loc)
        {
            DmtxRange locStatus;

            do
            {
                locStatus = GetGridCoordinates(ref loc);

                /* Always leave grid pointing at next available location */
                this._pixelCount++;

            } while (locStatus == DmtxRange.DmtxRangeBad);

            return locStatus;
        }

        private DmtxRange GetGridCoordinates(ref DmtxPixelLoc locRef)
        {

            /* Initially pixelCount may fall beyond acceptable limits. Update grid
             * state before testing coordinates */

            /* Jump to next cross pattern horizontally if current column is done */
            if (this._pixelCount >= this._pixelTotal)
            {
                this._pixelCount = 0;
                this._xCenter += this._jumpSize;
            }

            /* Jump to next cross pattern vertically if current row is done */
            if (this._xCenter > this._maxExtent)
            {
                this._xCenter = this._startPos;
                this._yCenter += this._jumpSize;
            }

            /* Increment level when vertical step goes too far */
            if (this._yCenter > this._maxExtent)
            {
                this._total *= 4;
                this._extent /= 2;
                SetDerivedFields();
            }

            if (this._extent == 0 || this._extent < this._minExtent)
            {
                locRef.X = locRef.Y = -1;
                return DmtxRange.DmtxRangeEnd;
            }

            int count = this._pixelCount;

            if (count >= this._pixelTotal)
            {
                throw new Exception("Scangrid is beyong image limits!");
            }

            DmtxPixelLoc loc = new DmtxPixelLoc();
            if (count == this._pixelTotal - 1)
            {
                /* center pixel */
                loc.X = this._xCenter;
                loc.Y = this._yCenter;
            }
            else
            {
                int half = this._pixelTotal / 2;
                int quarter = half / 2;

                /* horizontal portion */
                if (count < half)
                {
                    loc.X = this._xCenter + ((count < quarter) ? (count - quarter) : (half - count));
                    loc.Y = this._yCenter;
                }
                /* vertical portion */
                else
                {
                    count -= half;
                    loc.X = this._xCenter;
                    loc.Y = this._yCenter + ((count < quarter) ? (count - quarter) : (half - count));
                }
            }

            loc.X += this._xOffset;
            loc.Y += this._yOffset;

            locRef.X = loc.X;
            locRef.Y = loc.Y;

            if (loc.X < this._xMin || loc.X > this._xMax ||
                  loc.Y < this._yMin || loc.Y > this._yMax)
            {
                return DmtxRange.DmtxRangeBad;
            }

            return DmtxRange.DmtxRangeGood;
        }

        /// <summary>
        /// Update derived fields based on current state
        /// </summary>
        private void SetDerivedFields()
        {
            this._jumpSize = this._extent + 1;
            this._pixelTotal = 2 * this._extent - 1;
            this._startPos = this._extent / 2;
            this._pixelCount = 0;
            this._xCenter = this._yCenter = this._startPos;
        }

        #endregion

        #region Properties

        /// <summary>
        ///  Smallest cross size used in scan
        /// </summary>
        internal int MinExtent
        {
            get { return _minExtent; }
            set { _minExtent = value; }
        }

        /// <summary>
        /// Size of bounding grid region (2^N - 1)
        /// </summary>
        internal int MaxExtent
        {
            get { return _maxExtent; }
            set { _maxExtent = value; }
        }

        /// <summary>
        /// Offset to obtain image X coordinate
        /// </summary>
        internal int XOffset
        {
            get { return _xOffset; }
            set { _xOffset = value; }
        }

        /// <summary>
        /// Offset to obtain image Y coordinate
        /// </summary>
        internal int YOffset
        {
            get { return _yOffset; }
            set { _yOffset = value; }
        }

        /// <summary>
        ///  Minimum X in image coordinate system
        /// </summary>
        internal int XMin
        {
            get { return _xMin; }
            set { _xMin = value; }
        }

        /// <summary>
        /// Maximum X in image coordinate system
        /// </summary>
        internal int XMax
        {
            get { return _xMax; }
            set { _xMax = value; }
        }

        /// <summary>
        ///  Minimum Y in image coordinate system
        /// </summary>
        internal int YMin
        {
            get { return _yMin; }
            set { _yMin = value; }
        }

        /// <summary>
        /// Maximum Y in image coordinate system
        /// </summary>
        internal int YMax
        {
            get { return _yMax; }
            set { _yMax = value; }
        }

        /// <summary>
        ///  Total number of crosses at this size
        /// </summary>
        internal int Total
        {
            get { return _total; }
            set { _total = value; }
        }

        /// <summary>
        ///  Length/width of cross in pixels
        /// </summary>
        internal int Extent
        {
            get { return _extent; }
            set { _extent = value; }
        }

        /// <summary>
        /// Distance in pixels between cross centers
        /// </summary>
        internal int JumpSize
        {
            get { return _jumpSize; }
            set { _jumpSize = value; }
        }

        /// <summary>
        ///  Total pixel count within an individual cross path
        /// </summary>
        internal int PixelTotal
        {
            get { return _pixelTotal; }
            set { _pixelTotal = value; }
        }

        /// <summary>
        /// X and Y coordinate of first cross center in pattern
        /// </summary>
        internal int StartPos
        {
            get { return _startPos; }
            set { _startPos = value; }
        }

        /// <summary>
        /// Progress (pixel count) within current cross pattern
        /// </summary>
        internal int PixelCount
        {
            get { return _pixelCount; }
            set { _pixelCount = value; }
        }

        /// <summary>
        /// X center of current cross pattern
        /// </summary>
        internal int XCenter
        {
            get { return _xCenter; }
            set { _xCenter = value; }
        }

        /// <summary>
        /// Y center of current cross pattern
        /// </summary>
        internal int YCenter
        {
            get { return _yCenter; }
            set { _yCenter = value; }
        }

        #endregion
    }

    internal struct DmtxTriplet
    {
        byte[] _value;

        internal byte[] Value
        {
            get { return this._value ?? (this._value = new byte[3]); }
        }
    }
}
