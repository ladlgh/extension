﻿using System;

namespace Extension.BCG
{
    public enum BarCodeType : int
    {
        UNSPECIFIED,
        CODE128,
        CODE128A,
        CODE128B,
        CODE128C,
        DATAMATRIX
    }
    
    public enum SaveTypes : int
    {
        JPG,
        BMP,
        PNG,
        GIF,
        TIFF,
        UNSPECIFIED
    }

    public enum AlignmentPositions : int
    {
        CENTER,
        LEFT,
        RIGHT
    }

    public enum LabelPositions : int
    {
        TOPLEFT,
        TOPCENTER,
        TOPRIGHT,
        BOTTOMLEFT,
        BOTTOMCENTER,
        BOTTOMRIGHT
    }

    internal enum DmtxFormat
    {
        Matrix,
        Mosaic,
    }

    internal enum DmtxSymAttribute
    {
        DmtxSymAttribSymbolRows,
        DmtxSymAttribSymbolCols,
        DmtxSymAttribDataRegionRows,
        DmtxSymAttribDataRegionCols,
        DmtxSymAttribHorizDataRegions,
        DmtxSymAttribVertDataRegions,
        DmtxSymAttribMappingMatrixRows,
        DmtxSymAttribMappingMatrixCols,
        DmtxSymAttribInterleavedBlocks,
        DmtxSymAttribBlockErrorWords,
        DmtxSymAttribBlockMaxCorrectable,
        DmtxSymAttribSymbolDataWords,
        DmtxSymAttribSymbolErrorWords,
        DmtxSymAttribSymbolMaxCorrectable
    }

    public enum DmtxSymbolSize
    {
        DmtxSymbolRectAuto = -3,
        DmtxSymbolSquareAuto = -2,
        DmtxSymbolShapeAuto = -1,
        DmtxSymbol10x10 = 0,
        DmtxSymbol12x12,
        DmtxSymbol14x14,
        DmtxSymbol16x16,
        DmtxSymbol18x18,
        DmtxSymbol20x20,
        DmtxSymbol22x22,
        DmtxSymbol24x24,
        DmtxSymbol26x26,
        DmtxSymbol32x32,
        DmtxSymbol36x36,
        DmtxSymbol40x40,
        DmtxSymbol44x44,
        DmtxSymbol48x48,
        DmtxSymbol52x52,
        DmtxSymbol64x64,
        DmtxSymbol72x72,
        DmtxSymbol80x80,
        DmtxSymbol88x88,
        DmtxSymbol96x96,
        DmtxSymbol104x104,
        DmtxSymbol120x120,
        DmtxSymbol132x132,
        DmtxSymbol144x144,
        DmtxSymbol8x18,
        DmtxSymbol8x32,
        DmtxSymbol12x26,
        DmtxSymbol12x36,
        DmtxSymbol16x36,
        DmtxSymbol16x48
    }

    internal enum DmtxFlip
    {
        DmtxFlipNone = 0x00,
        DmtxFlipX = 0x01 << 0,
        DmtxFlipY = 0x01 << 1
    }

    internal enum DmtxPackOrder
    {
        /* Custom format */
        DmtxPackCustom = 100,
        /* 1 bpp */
        DmtxPack1bppK = 200,
        /* 8 bpp grayscale */
        DmtxPack8bppK = 300,
        /* 16 bpp formats */
        DmtxPack16bppRGB = 400,
        DmtxPack16bppRGBX,
        DmtxPack16bppXRGB,
        DmtxPack16bppBGR,
        DmtxPack16bppBGRX,
        DmtxPack16bppXBGR,
        DmtxPack16bppYCbCr,
        /* 24 bpp formats */
        DmtxPack24bppRGB = 500,
        DmtxPack24bppBGR,
        DmtxPack24bppYCbCr,
        /* 32 bpp formats */
        DmtxPack32bppRGBX = 600,
        DmtxPack32bppXRGB,
        DmtxPack32bppBGRX,
        DmtxPack32bppXBGR,
        DmtxPack32bppCMYK
    }

    internal enum DmtxRange
    {
        DmtxRangeGood,
        DmtxRangeBad,
        DmtxRangeEnd
    }

    internal enum DmtxDirection
    {
        DmtxDirNone = 0x00,
        DmtxDirUp = 0x01 << 0,
        DmtxDirLeft = 0x01 << 1,
        DmtxDirDown = 0x01 << 2,
        DmtxDirRight = 0x01 << 3,
        DmtxDirHorizontal = DmtxDirLeft | DmtxDirRight,
        DmtxDirVertical = DmtxDirUp | DmtxDirDown,
        DmtxDirRightUp = DmtxDirRight | DmtxDirUp,
        DmtxDirLeftDown = DmtxDirLeft | DmtxDirDown
    }

    public enum DmtxScheme
    {
        DmtxSchemeAutoFast = -2,
        DmtxSchemeAutoBest = -1,
        DmtxSchemeAscii = 0,
        DmtxSchemeC40,
        DmtxSchemeText,
        DmtxSchemeX12,
        DmtxSchemeEdifact,
        DmtxSchemeBase256,
        DmtxSchemeAsciiGS1
    }

    internal enum DmtxMaskBit
    {
        DmtxMaskBit8 = 0x01 << 0,
        DmtxMaskBit7 = 0x01 << 1,
        DmtxMaskBit6 = 0x01 << 2,
        DmtxMaskBit5 = 0x01 << 3,
        DmtxMaskBit4 = 0x01 << 4,
        DmtxMaskBit3 = 0x01 << 5,
        DmtxMaskBit2 = 0x01 << 6,
        DmtxMaskBit1 = 0x01 << 7
    }

    internal enum DmtxEdge
    {
        DmtxEdgeTop = 0x01 << 0,
        DmtxEdgeBottom = 0x01 << 1,
        DmtxEdgeLeft = 0x01 << 2,
        DmtxEdgeRight = 0x01 << 3
    }

    enum DmtxChannelStatus
    {
        DmtxChannelValid = 0x00,
        DmtxChannelUnsupportedChar = 0x01 << 0,
        DmtxChannelCannotUnlatch = 0x01 << 1
    }

    enum DmtxUnlatch
    {
        Explicit,
        Implicit
    }
}
