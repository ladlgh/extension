﻿using System.Data;
using System.IO;
using System.Security.Cryptography;

namespace Extension.DTS
{
    public static class extDataTable
    {
        // Load File To DataTable
        public static DataTable DeSerialize(this DataTable datatable,
                                            string file_path,
                                            bool crypto = false)
        {
            byte[] storage;
            string tableName = datatable.TableName;

            if (crypto)
            {
                using (CryptoCore cryptor = new CryptoCore())
                {
                    storage = cryptor.DecryptBuffer(File.ReadAllBytes(file_path));
                }
            }
            else
            {
                storage = File.ReadAllBytes(file_path);
            }

            datatable = storage.DeSerialize();

            return datatable.DefaultView.ToTable(tableName);
        }

        // Write DataTable To File
        public static void Serialize(this DataTable datatable,
                                     string file_path,
                                     bool crypto = false)
        {
            byte[] storage = datatable.Serialize();

            if (crypto)
            {
                using (CryptoCore cryptor = new CryptoCore())
                {
                    File.WriteAllBytes(file_path, cryptor.EncryptBuffer(storage));
                }
            }
            else
            {
                File.WriteAllBytes(file_path, storage);
            }
        }
    }
}
